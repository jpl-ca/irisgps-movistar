<?php

class DeviceService {

	public static function allDevices()
	{

		$data = Device::all();
		return Response::success($data, false, false, ': la información de todos los dispositivos se ha recuperado con éxito'); //retorna un response correcto y genera un Token nuevo
	}

	public static function createDevice($input)
	{
		$validator = Validator::make(
		    $input,
		    Device::$rules
		);
		if ($validator->fails())
		{
			$messages = $validator->errors()->toArray();
			return Response::invalid(false, false, ": los datos tienen errores", $messages);
		}

		$data =new Device;
		$data->mobile = $input['imei'];
		$data->activated = is_null($input['activated']) ? 0 : 1;
		$data->allowed = is_null($input['allowed']) ? 0 : 1;

		if($data->save())
		{
			return Response::success($data, false, false, ': se ha creado el registro de manera exitosa'); //retorna un response correcto y genera un Token nuevo
		}

		return Response::invalid(false, false, ": no se pudo procesar la solicitud", 'Hubo un error al procesar la solicitud');
	}

	public static function pairDevice($mobile, $plate, $dni)
	{
		$vehicles = Vehicle::where('plate', $plate);

		$vehicle = $vehicles->first();

        $responsor= new Responsor();

		if(is_null($vehicle)){
            $responsor->isInvalid();
            $responsor->message = "la placa del vehículo no es correcta"; //retorna un response invalido pero no actualiza el Token
            $responsor->isEmptyToken();
            return $responsor->response();
		}

		$vehicle = $vehicles->whereNull('device_id')->first();

		if(is_null($vehicle)){
            $responsor->isInvalid();
            $responsor->message = "el vehículo ya ha sido pareado"; //retorna un response invalido pero no actualiza el Token
            $responsor->isEmptyToken();
            return $responsor->response();
		}

		$employee = Employee::where('dni', $dni)->first();

		if(is_null($employee)){
            $responsor->isInvalid();
            $responsor->message = "el conductor no existe"; //retorna un response invalido pero no actualiza el Token
            $responsor->isEmptyToken();
            return $responsor->response();
		}

		$device = Device::where('mobile', $mobile)->first();

		$tracking_route = TrackingRoute::leftJoin('route_tasks', 'tracking_routes.id', '=', 'route_tasks.tracking_route_id')
            ->where('tracking_routes.vehicle_id', $vehicle->id)
			->where('tracking_routes.date', '>=', Carbon::now()->startOfDay()->toDateTimeString())
			->where('tracking_routes.date', '<=', Carbon::now()->endOfDay()->toDateTimeString())
            ->whereIn('route_tasks.task_state_id', array(1,3))
			->orderBy('tracking_routes.date', 'DESC')
            ->get(array('tracking_routes.*'))->first();

        if(is_null($tracking_route)){
            //$responsor->isInvalid();
            $vehicle->device_id = $device->id;
            $vehicle->save();
            $responsor->message = "no hay rutas programadas para este vehículo para el día de hoy"; //retorna un response invalido pero no actualiza el Token
            $responsor->isEmptyToken();
            $responsor->data = [
                'id' => null,
                'vehicle_id' => $vehicle->id
            ];
            return $responsor->response();
        }

		$passenger = $tracking_route->passengers()
			->where('tracking_route_id', $tracking_route->id)
			->where('employee_id', $employee->id)
			->first();

		if(is_null($passenger)){
            $responsor->isNoAuth();
            $responsor->message = "el conductor no está autorizado"; //retorna un response invalido pero no actualiza el Token
            $responsor->isEmptyToken();
            return $responsor->response();
		}
		
		$vehicle->device_id = $device->id;
		$vehicle->save();
		
		$data = TrackingRoute::with(
			['tasks.state', 'tasks.customer',
			'tasks.stateHistory',
			'passengers.employee.job',
			'passengers.type',
			'comments.user']
			)->find($tracking_route->id);

        $responsor->message = "dispositivo pareado correctamente"; //retorna un response invalido pero no actualiza el Token
        $responsor->data = $data;
        return $responsor->response();

	}

	public static function unpairDevice($mobile)
	{		

		$device = Device::where('mobile', $mobile)->first();

		$vehicle = Vehicle::where('device_id', $device->id)->first();

        $responsor = new Responsor();

		if(is_null($vehicle) || is_null($mobile)){
            $responsor->isInvalid();
            $responsor->message = "el dispositivo no está pareado"; //retorna un response invalido pero no actualiza el Token
            $responsor->isEmptyToken();
            return $responsor->response();
		}

		$vehicle->device_id = null;
		$device->token = null;
		$vehicle->save();
		$device->save();

        $responsor->message = "el dispositivo se ha despareado con éxito"; //retorna un response invalido pero no actualiza el Token
        $responsor->isEmptyToken();
        return $responsor->response();

	}

	public static function getPairedVehiclePlateByToken($token)
	{
		$device = Device::where('token', $token)->first();

		$vehicle = Vehicle::where('device_id',$device->id)->first();

		if(is_null($vehicle) || is_null($device)){
			return null; // retorna vacío si no pudo encontrar el vehiculo buscando el dispositivo por el token
		}

		return $vehicle->plate;
	}

    public static function getDeviceByIMEI() {

        $device = Device::where('mobile', Request::header('IMEI'))->first();

        if(is_null($device)) {
            return Response::view('errors.404', array(), 404);
        }

        return $device;
    }

}