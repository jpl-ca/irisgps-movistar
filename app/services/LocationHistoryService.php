<?php

class LocationHistoryService {

	public static function storeLocations($plate, $locations)
	{
        $responsor = new Responsor();

		if(is_null($plate) || is_null($locations))
		{
            $responsor->isInvalid();
            $responsor->message = "los datos no están completos"; //retorna un response invalido pero no actualiza el Token
            return $responsor->response();
		}
		
		$locations = json_decode($locations, true);
		$locationHistories = LocationHistory::toSaveModel('LocationHistory', $locations);

        $responsor->message = "las posiciones se han registrado con éxito"; //retorna un response invalido pero no actualiza el Token
        return $responsor->response();
	}

	public static function storeLocationsx($locations)
	{
        $responsor = new Responsor();

		$locations = json_decode($locations, true);

		$path = public_path("data/locations.json");

		$file = fopen($path, "w");

		$stored = json_decode(file_get_contents($path), true);

		$stored = is_null($stored) ? [] : $stored;

		foreach ($locations as $loc) {
			array_push($stored, [
				"lat" => $loc['lat'],
				"lng" => $loc['lng'],
				"speed_device" => $loc['speed_device'],
				"speed_custom" => $loc['speed_custom'],
				"created_at" => $loc['created_at']
			]);
		}

		fwrite($file, json_encode($stored));
		fclose($file);

        $responsor->message = "las posiciones se han registrado con éxito"; //retorna un response invalido pero no actualiza el Token
        return $responsor->response();
	}

}