<?php

class CustomerService {

	public static function allCustomers()
	{

		$data = Customer::all();
		return Response::success($data, false, false, ': la información de todos los clientes se ha recuperado con éxito'); //retorna un response correcto y genera un Token nuevo
	}

	public static function createCustomer($input)
	{
		$validator = Validator::make(
		    $input,
		    Customer::$rules
		);
		if ($validator->fails())
		{
			$messages = $validator->errors()->toArray();
			return Response::invalid(false, false, ": los datos tienen errores", $messages);
		}

		$data =new Customer;
		$data->customer_code = $input['customer_code'];
		$data->name = $input['name'];
		$data->phone = $input['phone'];
		$data->mobile = (is_null($input['mobile'])) ? null : $input['mobile'];
		$data->lat = $input['lat'];
		$data->lng = $input['lng'];
		$data->address = $input['address'];
		
		$data->district = $input['district'];
		$data->province = $input['province'];
		$data->region = $input['region'];

		if($data->save())
		{
			return Response::success($data, false, false, ': se ha creado el registro de manera exitosa'); //retorna un response correcto y genera un Token nuevo
		}

		return Response::invalid(false, false, ": no se pudo procesar la solicitud", 'Hubo un error al procesar la solicitud');
	}

}