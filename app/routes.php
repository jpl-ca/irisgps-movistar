<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


if($_ENV['DOMAIN_NAME'] == 'irisgps.com')
{
	Route::group(array('domain' => 'movistar.irisgps.com'), function() {
		include 'routes/api.php';
		include 'routes/site.php';
		include 'routes/web_services.php';
	});

	Route::get('/', array('before' => '', function()
	{

		return View::make('landing.landing1');

	}));
}
else{

	Route::get('/test-notification/{gcm}', array('before' => '', function($gcm)
	{
		// $devices = Device::wherenotNull('gcm')->select('gcm')->lists('gcm');

		$pusher = new Pusher();

		$pusher->addRecipent($gcm);

		return dd($pusher->send());
	}));

	Route::get('/testx', array('before' => '', function()
	{
		$geofence = Geofence::find(1);

		$assigned_vehicles = $geofence->vehiclesIds();
		//$assigned_vehicles = array_flip($assigned_vehicles);

		//return dd($assigned_vehicles);
		$data_to_evaluate[1] = [
						'vehicle_id' => 1,
						'lat' => 123,
						'lng' => 321,
						'date' => 123,
						'plate' => 1123
					];
		$data_to_evaluate[3] = [
						'vehicle_id' => 2,
						'lat' => 123,
						'lng' => 321,
						'date' => 123,
						'plate' => 1123
					];

		$assigned_vehicles_with_last_position = array_only($data_to_evaluate, array_intersect(array_keys($data_to_evaluate), $assigned_vehicles));
		//return dd(array_keys($data_to_evaluate));
		//return dd($assigned_vehicles);
		// $assigned_vehicles_with_last_position = array_intersect(array_keys($data_to_evaluate), $assigned_vehicles);

		return dd($assigned_vehicles_with_last_position);
	}));

	Route::get('/test', array('before' => '', function()
	{
		$vehicle = Vehicle::wherePlate('XOE-035')->first();
		$date = Carbon::now();
		$path = LocationHistory::whereVehicleId($vehicle->id)
		->where('created_at', '>=', $date->copy()->startOfDay()->toDateTimeString())
		->where('created_at', '<=', $date->copy()->endOfDay()->toDateTimeString())
		->select('lat','lng')
		->get()->toArray();

		return GeoPos::distance($path);
		exit();
	}));

	Route::get('/test-speed', array('before' => '', function()
	{
		$vehicle = Vehicle::wherePlate('BDE-774')->first();
		$date = Carbon::now()->subDays(7);
		$path = LocationHistory::whereVehicleId($vehicle->id)
		->where('created_at', '>=', $date->copy()->startOfDay()->toDateTimeString())
		->where('created_at', '<=', $date->copy()->endOfDay()->toDateTimeString())
		->select('lat','lng', 'created_at')
		->get()->toArray();

		$results = GeoPos::speed($path, 0.01, 'km');
		return dd($results->current(true));
		exit();
	}));

	Route::get('/test-address', array('before' => '', function()
	{
		$locations = [
			[
				'lat' => -9.9435461,
				'lng' => -76.2458899
			],
			[
				'lat' => -9.9282015,
				'lng' => -76.2438944
			],
			[
				'lat' => -12.0874108,
				'lng' => -76.9887115
			],
			[
				'lat' => -12.0905513,
				'lng' => -76.9850203
			]
		];

		$result = array();

		foreach ($locations as $location) {
			array_push($result, GeoPos::getAddressData2($location['lat'], $location['lng']));
		}

		return dd($result);

		exit();
	}));

	include 'routes/api.php';
	include 'routes/site.php';
	include 'routes/web_services.php';
}