<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"geofences_notification_targets" => "Objetivos de notificacion de Geo-Cercas",
	"geofences_validation_frecuency" => "Validacion de Geo-Cercas en los últimos minutos",
	"speed_limits_green" => "Limites de velocidad Verde",
	"speed_limits_yellow" => "Limites de velocidad Amarillo",
	"speed_limits_orange" => "Limite de velocidad Naranja",

);
