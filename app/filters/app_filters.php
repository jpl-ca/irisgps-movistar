<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/


Route::filter('ws-singleAuth', function()
{
    $responsor = new Responsor();
	if (!is_null(Request::header('IMEI')) && !is_null(Request::header('gcm')))
	{
		$device = Device::findByMobile(Request::header('IMEI'));


		if(is_null($device))
		{
            $responsor->isNoAuth();
            $responsor->isEmptyToken();
            $responsor->message = 'el dispositivo no está registrado';
            return $responsor->response();
		}

		if($device->isActivated())
		{
			if(!$device->isAllowed())
			{
                $responsor->isNoAuth();
                $responsor->isEmptyToken();
                $responsor->message = 'el dispositivo no está permitido';
                return $responsor->response();
			}

			Device::registerGCM(Request::header('IMEI'));

			//->ejecuta la acción correspondiente

		}else{
            $responsor->isNoAuth();
            $responsor->isEmptyToken();
            $responsor->message = 'el dispositivo no está activado';
            return $responsor->response();
		}
	}
	else
	{
        $responsor->isInvalid();
        $responsor->isEmptyToken();
        $responsor->message = 'el número de teléfono o id gcm no han sido encontrado en la solicitud';
        return $responsor->response();
	}
});

Route::filter('ws-tokenAuth', function()
{
    $responsor = new Responsor();

	if(!is_null(Request::header('token')))
	{
		if (!Tokenizer::validate(Request::header('token'), Request::header('IMEI')))
		{
            $responsor->isNoAuth();
            $responsor->isEmptyToken();
            $responsor->message = 'no hay un token válido';
            return $responsor->response();
		}
	}else
	{
        $responsor->isInvalid();
        $responsor->isEmptyToken();
        $responsor->message = 'no se ha encontrado un token en la solicitud';
        return $responsor->response();
	}
});

///////////////////////////////////

Route::filter('site', function()
{
	if (Input::has('token') && Input::has('mobile'))
	{
		return Response::noAuth();
	}
});