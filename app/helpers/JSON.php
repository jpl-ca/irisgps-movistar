<?php

class JSON {
	
	public static function isJson($string) {
		$decoded = json_decode($string);
		if (is_array($decoded) || is_object($decoded)) 
	    {
	    	return true;
	    }else{
	    	return false;
	    }
	}

	public static function ToObject($json) {
		if(!self::isJson($json)){
			return null;
		}
		return json_decode($json);
	}

	public static function ToArray($json) {
		if(!self::isJson($json)){
			return null;
		}
		return json_decode($json, true);
	}
}