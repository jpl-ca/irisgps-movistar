<?php

class GeoPos {

	const EARTH_RADIUS = 6378137; // Earth’s mean radius in meter
	const UNIT_M = "m";
	const UNIT_KM = "km";
	const VALUE_M = 1;
	const VALUE_KM = 1000;
	const KILOMETERS_PER_HOUR_TO_METERS_PER_SECOND = 0.27777777777777777777777777777778;
	const METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR = 3.6;
	const BAD_SPEED_CACULATION = "bad speed calculation ='(";

	public static function getAddressData2($orig_lat, $orig_lon, $formatted = false)
	{		
		$url = "http://maps.google.com/maps/api/geocode/json?latlng=$orig_lat,$orig_lon&sensor=false";
	    //$data = json_decode(file_get_contents($url), true);
	    $data = json_decode(file_get_contents($url));

	    // $address = $data->results[0]->address_components[0]->types;

	    $ai = 0;

	    $address_components = $data->results[$ai]->address_components;

	    $hasStreetNumber = false;
	    $hasRoute = false;
	    $hasDistrict = false;
	    $hasProvince = false;
	    $hasRegion = false;

	    for ($i = 0; $i < count($address_components); $i++) {
	    	$ac = $address_components[$i];

	    	if(in_array('street_number', $ac->types))
	    	{
	    		if(!$hasStreetNumber){
	    			$street_number = $ac->long_name;
	    			$hasStreetNumber = true;
	    		}
	    	}

	    	if(in_array('route', $ac->types))
	    	{
	    		if(!$hasRoute){
	    			$route = $ac->long_name;
	    			$hasRoute = true;
	    		}
	    	}

	    	if ($i == (count($address_components)-1)) {
				if($hasStreetNumber && $hasRoute)
		    	{
		    		break;
		    	}else{
		    		if($ai >= 1)
		    		{
		    			$ai--;
		    			$address_components = $data->results[$ai]->address_components;
		    			$i = -1;
		    		}else{
		    			break;
		    		}
		    	}
		    }
	    }

	    $ai = 2;

	    $address_components = $data->results[$ai]->address_components;

	    for ($i = 0; $i < count($address_components); $i++) {
	    	$ac = $address_components[$i];

	    	if(in_array('locality', $ac->types))
	    	{
	    		if(!$hasDistrict){
	    			$district = $ac->long_name;
	    			$hasDistrict = true;
	    		}
	    	}

	    	if(in_array('administrative_area_level_2', $ac->types))
	    	{
	    		if(!$hasProvince){
	    			$province = $ac->long_name;
	    			$hasProvince = true;
	    		}
	    	}

	    	if(in_array('administrative_area_level_1', $ac->types))
	    	{
	    		if(!$hasRegion){
	    			$region = $ac->long_name;
	    			$hasRegion = true;
	    		}
	    	}

	    	if ($i == (count($address_components)-1)) {
				if($hasDistrict && $hasProvince && $hasRegion)
		    	{
		    		break;
		    	}else{
		    		if($ai >= 1)
		    		{
		    			$ai--;
		    			$address_components = $data->results[$ai]->address_components;
		    			$i = -1;
		    		}else{
		    			break;
		    		}
		    	}
		    }
	    }

	    $return_array['address'] = ((isset($route)) ? $route : '').' '.((isset($street_number)) ? $street_number : '');
	    $return_array['district'] = (isset($district)) ? $district : '';
	    $return_array['province'] = (isset($province)) ? $province : '';
	    $return_array['region'] = (isset($region)) ? $region : '';

	    $return_array['address'] = trim($return_array['address']);

	    $return_array['district'] = trim( preg_replace('/\d/', '', $return_array['district']) );
	    $return_array['district'] = trim( str_replace('District', '', $return_array['district']) );

	    $return_array['province'] = trim( preg_replace('/\d/', '', $return_array['province']) );
	    $return_array['region'] = trim( preg_replace('/\d/', '', $return_array['region']) );

	    $return_array['formatted_address'] = $data->results[0]->formatted_address;

	    
	    return $formatted ? ($return_array['district'].', '.$return_array['address']) : $return_array;
	}

	public static function getAddressData($orig_lat, $orig_lon, $formatted = false)
	{		
		$url = "http://maps.google.com/maps/api/geocode/json?latlng=$orig_lat,$orig_lon&sensor=false";
	    //$data = json_decode(file_get_contents($url), true);
	    $data = json_decode(file_get_contents($url));

	    $address = $data->results[0]->formatted_address;

	    $ai = 2;

	    $address_components = $data->results[$ai]->address_components;

	    $hasDistrict = false;
	    $hasProvince = false;
	    $hasRegion = false;

	    for ($i = 0; $i < count($address_components); $i++) {
	    	$ac = $address_components[$i];

	    	if(in_array('locality', $ac->types))
	    	{
	    		if(!$hasDistrict){
	    			$district = $ac->long_name;
	    			$hasDistrict = true;
	    		}
	    	}

	    	if(in_array('administrative_area_level_2', $ac->types))
	    	{
	    		if(!$hasProvince){
	    			$province = $ac->long_name;
	    			$hasProvince = true;
	    		}
	    	}

	    	if(in_array('administrative_area_level_1', $ac->types))
	    	{
	    		if(!$hasRegion){
	    			$region = $ac->long_name;
	    			$hasRegion = true;
	    		}
	    	}

	    	if ($i == (count($address_components)-1)) {
				if($hasDistrict && $hasProvince && $hasRegion)
		    	{
		    		break;
		    	}else{
		    		if($ai >= 1)
		    		{
		    			$ai--;
		    			$address_components = $data->results[$ai]->address_components;
		    			$i = -1;
		    		}else{
		    			break;
		    		}
		    	}
		    }
	    }

	    $return_array['address']= (isset($address)) ? $address : '';
	    $return_array['district']= (isset($district)) ? $district : '';
	    $return_array['province']= (isset($province)) ? $province : '';
	    $return_array['region']= (isset($region)) ? $region : '';
	    
	    return $formatted ? $return_array['district'] : $return_array;
	}

	public static function is_in_polygon($points_polygon, $poligon_lng, $poligon_lat, $longitude_x, $latitude_y)
	{
	  $i = $j = $c = 0;
	  for ($i = 0, $j = $points_polygon ; $i < $points_polygon; $j = $i++) {
	    if ( (($poligon_lat[$i]  >  $latitude_y != ($poligon_lat[$j] > $latitude_y)) &&
	     ($longitude_x < ($poligon_lng[$j] - $poligon_lng[$i]) * ($latitude_y - $poligon_lat[$i]) / ($poligon_lat[$j] - $poligon_lat[$i]) + $poligon_lng[$i]) ) )
	       $c = !$c;
	  }
	  return $c;
	}

	public static function address($orig_lat, $orig_lon, $formatted = false){
		return self::getAddressData2($orig_lat, $orig_lon, $formatted);
	}

	public static function distance(array $arrayOfPoints, $unit = 'km')
	{
		self::validateArrayOfPoint($arrayOfPoints, "distance");

		$totalPoints = count($arrayOfPoints);

		$distance = 0;

		for($i = 0; $i < $totalPoints; $i++) {
			if(isset($arrayOfPoints[$i+1])){
				$distance += self::calculateDistance($arrayOfPoints[$i]['lat'], $arrayOfPoints[$i]['lng'], $arrayOfPoints[$i+1]['lat'], $arrayOfPoints[$i+1]['lng']);
			}else{
				break;
			}
		}
		switch ($unit) {
			case self::UNIT_M:
				$distance_convertion_unit = self::VALUE_M;
				$rounder = 0;
				break;

			case self::UNIT_KM:
				$distance_convertion_unit = self::VALUE_KM;
				$rounder = 3;
				break;
		}
		return round($distance / $distance_convertion_unit, $rounder);
	}

	public static function speed(array $arrayOfPointsAndTime, $lastUnits = 1, $unit = 'km')
	{
		self::validateArrayOfPoint($arrayOfPointsAndTime, "speed");

		$arrayOfPointsAndTime = array_reverse($arrayOfPointsAndTime);

		$speeds = new Speeds;

		switch ($unit) {
			case self::UNIT_M:
				$distance_convertion_unit = self::VALUE_M;
				$speed_convertion_unit = 1;
				$time_convertion_unit = 60;
				break;

			case self::UNIT_KM:
				$distance_convertion_unit = self::VALUE_KM;
				$speed_convertion_unit = self::METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR;
				$time_convertion_unit = 60;
				break;
		}

		$lastUnits = $lastUnits * $distance_convertion_unit;

		$totalPoints = count($arrayOfPointsAndTime);

		$timeA = null;

		$distance = 0;
		$i = 0;

		while($i < $totalPoints) {
			$sumCounter = true;

			if(isset($arrayOfPointsAndTime[$i+1])){

				$pointA = $arrayOfPointsAndTime[$i];
				$pointB = $arrayOfPointsAndTime[$i+1];
				$calculateDistance = self::calculateDistance($pointA['lat'], $pointA['lng'], $pointB['lat'], $pointB['lng']);
				$distance += $calculateDistance;

				if(is_null($timeA)){
					$timeA = $pointA['created_at'];
				}

				$timeB = $pointB['created_at'];

				if($distance >= $lastUnits){

					$time = self::calculateTime($timeA, $timeB);
					$speed = self::calculateSpeed($distance, $time);

					if($speed != self::BAD_SPEED_CACULATION)
					{
						$speeds->append([
							'distance' => $distance / $distance_convertion_unit,
							'time' => $time / $time_convertion_unit,
							'speed' => $speed * 3.6,
							'from' => $timeA,
							'until' => $timeB,
							'pointA' => $pointA['lat'].','.$pointA['lng'],
							'pointB' => $pointB['lat'].','.$pointB['lng'],
							'pointA_id' => $pointA['id'],
							'pointB_id' => $pointB['id'],
						]);
						$timeA = null;
						$distance = 0;
						//debe de sumar el contador

					}else{
						unset($arrayOfPointsAndTime[$i+1]);
   						$arrayOfPointsAndTime = array_values($arrayOfPointsAndTime);
   						$totalPoints = count($arrayOfPointsAndTime);
						$distance -= $calculateDistance;
						//debe de mantenerse igual el contador
						$sumCounter = false;
					}

				}

				if($sumCounter){
					$i++;
				}
				//debe de sumar el contador

			}else{
				break;
			}
		}

		if($distance != 0) {
			$time = self::calculateTime($timeA, $timeB);
			$speed = self::calculateSpeed($distance, $time);
			$speeds->append([
				'distance' => $distance / $distance_convertion_unit,
				'time' => $time / $time_convertion_unit,
				'speed' => $speed * 3.6,
				'from' => $timeA,
				'until' => $timeB,
				'pointA' => $pointA['lat'].','.$pointA['lng'],
				'pointB' => $pointB['lat'].','.$pointB['lng']
			]);
		}

		return $speeds;
	}

	public static function speed2(array $arrayOfPointsAndTime, $lastUnits = 1, $unit = 'km')
	{
		self::validateArrayOfPoint($arrayOfPointsAndTime, "speed");

		$arrayOfPointsAndTime = array_reverse($arrayOfPointsAndTime);

		$speeds = new Speeds;

		switch ($unit) {
			case self::UNIT_M:
				$distance_convertion_unit = self::VALUE_M;
				$speed_convertion_unit = self::METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR;
				$time_convertion_unit = 60;
				break;

			case self::UNIT_KM:
				$distance_convertion_unit = self::VALUE_KM;
				$speed_convertion_unit = 1;
				$time_convertion_unit = 60;
				break;
		}

		$lastUnits = $lastUnits * $distance_convertion_unit;

		$totalPoints = count($arrayOfPointsAndTime);

		$timeA = null;

		$distance = 0;
		$i = 0;

		while($i < $totalPoints) {
			$sumCounter = true;

			if(isset($arrayOfPointsAndTime[$i+1])){

				$pointA = $arrayOfPointsAndTime[$i];
				$pointB = $arrayOfPointsAndTime[$i+1];
				$calculateDistance = self::calculateDistance($pointA['lat'], $pointA['lng'], $pointB['lat'], $pointB['lng']);
				$distance += $calculateDistance;

				if(is_null($timeA)){
					$timeA = $pointA['created_at'];
				}

				$timeB = $pointB['created_at'];

				if($distance >= $lastUnits){

					$time = self::calculateTime($timeA, $timeB);
					$speed = self::calculateSpeed($calculateDistance, $time) * 3.6;

					if($speed != self::BAD_SPEED_CACULATION)
					{
						$speeds->append([
							'distance' => $calculateDistance / $distance_convertion_unit,
							'time' => self::secondsToHuman($time),
							'speed' => $speed,
							'from' => $timeA,
							'until' => $timeB,
							'pointA' => $pointA['lat'].','.$pointA['lng'],
							'pointB' => $pointB['lat'].','.$pointB['lng'],
							'pointA_id' => $pointA['id'],
							'pointB_id' => $pointB['id'],
						]);
						$timeA = null;
						$distance = 0;
						//debe de sumar el contador

					}else{
						unset($arrayOfPointsAndTime[$i+1]);
   						$arrayOfPointsAndTime = array_values($arrayOfPointsAndTime);
   						$totalPoints = count($arrayOfPointsAndTime);
						$distance -= $calculateDistance;
						//debe de mantenerse igual el contador
						$sumCounter = false;
					}

				}

				if($sumCounter){
					$i++;
				}
				//debe de sumar el contador

			}else{
				break;
			}
		}

		if($distance != 0) {
			$time = self::calculateTime($timeA, $timeB);
			$speed = self::calculateSpeed($distance, $time);
			$speeds->append([
				'distance' => $distance / $distance_convertion_unit,
				'time' => self::secondsToHuman($time),
				'speed' => $speed * 3.6,
				'from' => $timeA,
				'until' => $timeB,
				'pointA' => $pointA['lat'].','.$pointA['lng'],
				'pointB' => $pointB['lat'].','.$pointB['lng']
			]);
		}

		return $speeds;
	}

	public static function speedExceed(array $arrayOfPointsAndTime, $speedLimit = 100, $unit = 'km')
	{
		self::validateArrayOfPoint($arrayOfPointsAndTime, "speed");

		$arrayOfPointsAndTime = array_reverse($arrayOfPointsAndTime);

		$speeds = new Speeds;

		switch ($unit) {
			case self::UNIT_M:
				$distance_convertion_unit = self::VALUE_M;
				$speed_convertion_unit = self::KILOMETERS_PER_HOUR_TO_METERS_PER_SECOND;
				$time_convertion_unit = 60;
				break;

			case self::UNIT_KM:
				$distance_convertion_unit = self::VALUE_KM;
				$speed_convertion_unit = 1;
				$time_convertion_unit = 60;
				break;
		}

		$speedLimit = $speedLimit * $speed_convertion_unit;

		$totalPoints = count($arrayOfPointsAndTime);
		
		for($i = 0; $i < $totalPoints; $i++) {
			if(isset($arrayOfPointsAndTime[$i+1])){
				$pointA = $arrayOfPointsAndTime[$i];
				$pointB = $arrayOfPointsAndTime[$i+1];

				if($pointA['speed_custom'] >= $speedLimit)
				{

					$distance = self::calculateDistance($pointA['lat'], $pointA['lng'], $pointB['lat'], $pointB['lng']);

					$timeA = $pointA['created_at'];

					$timeB = $pointB['created_at'];

					$time = self::calculateTime($timeA, $timeB);

					$speed = $pointA['speed_custom'];

					$speeds->append([
						'distance' => $distance / $distance_convertion_unit,
						'time' => $time / $time_convertion_unit,
						'speed' => $speed * $speed_convertion_unit,
						'from' => $timeA,
						'until' => $timeB,
						'pointA' => $pointA['lat'].','.$pointA['lng'],
						'pointB' => $pointB['lat'].','.$pointB['lng']
					]);
				}

			}else{
				break;
			}
		}

		return $speeds;
	}

	public static function speedExceedX(array $arrayOfPointsAndTime, $speedLimit = 100, $lastUnits = 1, $unit = 'km')
	{
		self::validateArrayOfPoint($arrayOfPointsAndTime, "speed");

		$arrayOfPointsAndTime = array_reverse($arrayOfPointsAndTime);

		$speeds = new Speeds;

		switch ($unit) {
			case self::UNIT_M:
				$distance_convertion_unit = self::VALUE_M;
				$speed_convertion_unit = self::METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR;
				$time_convertion_unit = 60;
				break;

			case self::UNIT_KM:
				$distance_convertion_unit = self::VALUE_KM;
				$speed_convertion_unit = 1;
				$time_convertion_unit = 60;
				break;
		}

		$lastUnits = $lastUnits * $distance_convertion_unit;
		$speedLimit = $speedLimit * $speed_convertion_unit;

		$totalPoints = count($arrayOfPointsAndTime);

		$timeA = null;

		$distance = 0;
		$i = 0;

		while($i < $totalPoints) {
			$sumCounter = true;

			if(isset($arrayOfPointsAndTime[$i+1])){

				$pointA = $arrayOfPointsAndTime[$i];
				$pointB = $arrayOfPointsAndTime[$i+1];
				$calculateDistance = self::calculateDistance($pointA['lat'], $pointA['lng'], $pointB['lat'], $pointB['lng']);
				$distance += $calculateDistance;

				if(is_null($timeA)){
					$timeA = $pointA['created_at'];
				}

				$timeB = $pointB['created_at'];

				if($distance >= $lastUnits){

					$time = self::calculateTime($timeA, $timeB);
					$speed = self::calculateSpeed($calculateDistance, $time) * 3.6;

					if($speed != self::BAD_SPEED_CACULATION)
					{
						if($speed >= $speedLimit)
						{
							$speeds->append([
								'distance' => $calculateDistance / $distance_convertion_unit,
								'time' => self::secondsToHuman($time),
								'speed' => $speed,
								'from' => $timeA,
								'until' => $timeB,
								'pointA' => $pointA['lat'].','.$pointA['lng'],
								'pointB' => $pointB['lat'].','.$pointB['lng'],
								'pointA_id' => $pointA['id'],
								'pointB_id' => $pointB['id'],
							]);
						}
						$timeA = null;
						$distance = 0;
						//debe de sumar el contador

					}else{
						unset($arrayOfPointsAndTime[$i+1]);
   						$arrayOfPointsAndTime = array_values($arrayOfPointsAndTime);
   						$totalPoints = count($arrayOfPointsAndTime);
						$distance -= $calculateDistance;
						//debe de mantenerse igual el contador
						$sumCounter = false;
					}

				}

				if($sumCounter){
					$i++;
				}
				//debe de sumar el contador

			}else{
				break;
			}
		}

		if($distance != 0) {
			$time = self::calculateTime($timeA, $timeB);
			$speed = self::calculateSpeed($distance, $time) * 3.6;

			if($speed >= $speedLimit)
			{
				$speeds->append([
					'distance' => $distance / $distance_convertion_unit,
					'time' => self::secondsToHuman($time),
					'speed' => $speed,
					'from' => $timeA,
					'until' => $timeB,
					'pointA' => $pointA['lat'].','.$pointA['lng'],
					'pointB' => $pointB['lat'].','.$pointB['lng']
				]);
			}
		}
		return $speeds;
	}

	public static function speedExceed1(array $arrayOfPointsAndTime, $speedLimit = 100, $unit = 'km')
	{
		self::validateArrayOfPoint($arrayOfPointsAndTime, "speed");

		$arrayOfPointsAndTime = array_reverse($arrayOfPointsAndTime);

		$speeds = new Speeds;

		switch ($unit) {
			case self::UNIT_M:
				$distance_convertion_unit = self::VALUE_M;
				$speed_convertion_unit = self::KILOMETERS_PER_HOUR_TO_METERS_PER_SECOND;
				$time_convertion_unit = 60;
				break;

			case self::UNIT_KM:
				$distance_convertion_unit = self::VALUE_KM;
				$speed_convertion_unit = 1;
				$time_convertion_unit = 3600;
				break;
		}

		$speedLimit = $speedLimit * $speed_convertion_unit;

		$totalPoints = count($arrayOfPointsAndTime);
		
		for($i = 0; $i < $totalPoints; $i++) {

			if(isset($arrayOfPointsAndTime[$i+1])){
				$pointA = $arrayOfPointsAndTime[$i];
				$pointB = $arrayOfPointsAndTime[$i+1];

				$distance = self::calculateDistance($pointA['lat'], $pointA['lng'], $pointB['lat'], $pointB['lng']);

				$timeA = $pointA['created_at'];

				$timeB = $pointB['created_at'];

				$time = self::calculateTime($timeA, $timeB);

				$distance = $distance / $distance_convertion_unit;

				$time = $time / $time_convertion_unit;

				$speed = $distance / $time;

				//if($pointA['speed_custom'] >= $speedLimit)

				if($speed >= $speedLimit)
				{

					$speeds->append([
						'distance' => $distance,
						'time' => $time,
						'speed' => $speed,
						'from' => $timeA,
						'until' => $timeB,
						'pointA' => $pointA['lat'].','.$pointA['lng'],
						'pointB' => $pointB['lat'].','.$pointB['lng']
					]);
				}

			}else{
				break;
			}
		}

		return $speeds;
	}

	public static function speedExceed2(array $arrayOfPointsAndTime, $speedLimit = 100, $unit = 'km')
	{
		self::validateArrayOfPoint($arrayOfPointsAndTime, "speed");

		$arrayOfPointsAndTime = array_reverse($arrayOfPointsAndTime);

		$speeds = array();

		switch ($unit) {
			case self::UNIT_M:
				$distance_convertion_unit = self::VALUE_M;
				$speed_convertion_unit = self::KILOMETERS_PER_HOUR_TO_METERS_PER_SECOND;
				$time_convertion_unit = 60;
				break;

			case self::UNIT_KM:
				$distance_convertion_unit = self::VALUE_KM;
				$speed_convertion_unit = 1;
				$time_convertion_unit = 60;
				break;
		}

		$speedLimit = $speedLimit * $speed_convertion_unit;

		$totalPoints = count($arrayOfPointsAndTime);
		
		for($i = 0; $i < $totalPoints; $i++) {
			if(isset($arrayOfPointsAndTime[$i+1])){
				$pointA = $arrayOfPointsAndTime[$i];
				$pointB = $arrayOfPointsAndTime[$i+1];

				if($pointA['speed_custom'] >= $speedLimit)
				{

					$distance = self::calculateDistance($pointA['lat'], $pointA['lng'], $pointB['lat'], $pointB['lng']);

					$timeA = $pointA['created_at'];

					$timeB = $pointB['created_at'];

					$time = self::calculateTime($timeA, $timeB);

					$speed = $pointA['speed_custom'];

					array_push($speeds, [
						'distance' => $distance / $distance_convertion_unit,
						'time' => $time / $time_convertion_unit,
						'speed' => $speed * $speed_convertion_unit,
						'from' => $timeA,
						'until' => $timeB,
						'pointA' => $pointA['lat'].','.$pointA['lng'],
						'pointB' => $pointB['lat'].','.$pointB['lng']
					]);
				}

			}else{
				break;
			}
		}

		return $speeds;
	}

	public static function speedExceed3(array $arrayOfPointsAndTime, $speedLimit = 100, $unit = 'km')
	{
		self::validateArrayOfPoint($arrayOfPointsAndTime, "speed");

		$arrayOfPointsAndTime = array_reverse($arrayOfPointsAndTime);

		$speeds = array();

		switch ($unit) {
			case self::UNIT_M:
				$distance_convertion_unit = self::VALUE_M;
				$speed_convertion_unit = self::KILOMETERS_PER_HOUR_TO_METERS_PER_SECOND;
				$time_convertion_unit = 60;
				break;

			case self::UNIT_KM:
				$distance_convertion_unit = self::VALUE_KM;
				$speed_convertion_unit = 1;
				$time_convertion_unit = 60;
				break;
		}

		$speedLimit = $speedLimit * $speed_convertion_unit;

		$totalPoints = count($arrayOfPointsAndTime);
		
		for($i = 0; $i < $totalPoints; $i++) {
			if(isset($arrayOfPointsAndTime[$i+1])){
				$pointA = $arrayOfPointsAndTime[$i];
				$pointB = $arrayOfPointsAndTime[$i+1];

				if($pointA['speed_custom'] >= $speedLimit)
				{

					$distance = self::calculateDistance($pointA['lat'], $pointA['lng'], $pointB['lat'], $pointB['lng']);

					$timeA = $pointA['created_at'];

					$timeB = $pointB['created_at'];

					$time = self::calculateTime($timeA, $timeB);

					$speed = $pointA['speed_custom'];

					$speeds[0] = [
						'distance' => $distance / $distance_convertion_unit,
						'time' => $time / $time_convertion_unit,
						'speed' => $speed * $speed_convertion_unit,
						'from' => $timeA,
						'until' => $timeB,
						'pointA' => $pointA['lat'].','.$pointA['lng'],
						'pointB' => $pointB['lat'].','.$pointB['lng']
					];
				}

			}else{
				break;
			}
		}

		return $speeds;
	}

	public static function speedExceed4(array $arrayOfPointsAndTime, $speedLimit = 100, $unit = 'km')
	{
		self::validateArrayOfPoint($arrayOfPointsAndTime, "speed");

		$arrayOfPointsAndTime = array_reverse($arrayOfPointsAndTime);

		$speeds = array();

		switch ($unit) {
			case self::UNIT_M:
				$distance_convertion_unit = self::VALUE_M;
				$speed_convertion_unit = self::KILOMETERS_PER_HOUR_TO_METERS_PER_SECOND;
				$time_convertion_unit = 60;
				break;

			case self::UNIT_KM:
				$distance_convertion_unit = self::VALUE_KM;
				$speed_convertion_unit = 1;
				$time_convertion_unit = 3600;
				break;
		}

		$speedLimit = $speedLimit * $speed_convertion_unit;

		$totalPoints = count($arrayOfPointsAndTime);
		
		for($i = 0; $i < $totalPoints; $i++) {
			if(isset($arrayOfPointsAndTime[$i+1])){
				$pointA = $arrayOfPointsAndTime[$i];
				$pointB = $arrayOfPointsAndTime[$i+1];

				$distance = self::calculateDistance($pointA['lat'], $pointA['lng'], $pointB['lat'], $pointB['lng']);

				$timeA = $pointA['created_at'];

				$timeB = $pointB['created_at'];

				$time = self::calculateTime($timeA, $timeB);

				$distance = $distance / $distance_convertion_unit;
				$time = $time / $time_convertion_unit;

				$speed = $distance / $time;

				//if($pointA['speed_custom'] >= $speedLimit)
				if($speed >= $speedLimit)
				{

					$speeds[0] = [
						'distance' => $distance,
						'time' => $time,
						'speed' => $speed,
						'from' => $timeA,
						'until' => $timeB,
						'pointA' => $pointA['lat'].','.$pointA['lng'],
						'pointB' => $pointB['lat'].','.$pointB['lng']
					];
				}

			}else{
				break;
			}
		}

		return $speeds;
	}

	public static function speedExceedW(array $arrayOfPointsAndTime, $speedLimit = 100, $lastUnits = 1, $unit = 'km')
	{
		self::validateArrayOfPoint($arrayOfPointsAndTime, "speed");

		$arrayOfPointsAndTime = array_reverse($arrayOfPointsAndTime);

		$speeds = array();

		switch ($unit) {
			case self::UNIT_M:
				$distance_convertion_unit = self::VALUE_M;
				$speed_convertion_unit = self::METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR;
				$time_convertion_unit = 60;
				break;

			case self::UNIT_KM:
				$distance_convertion_unit = self::VALUE_KM;
				$speed_convertion_unit = 1;
				$time_convertion_unit = 60;
				break;
		}

		$lastUnits = $lastUnits * $distance_convertion_unit;
		$speedLimit = $speedLimit * $speed_convertion_unit;

		$totalPoints = count($arrayOfPointsAndTime);

		$timeA = null;

		$distance = 0;
		$i = 0;

		while($i < $totalPoints) {
			$sumCounter = true;

			if(isset($arrayOfPointsAndTime[$i+1])){

				$pointA = $arrayOfPointsAndTime[$i];
				$pointB = $arrayOfPointsAndTime[$i+1];
				$calculateDistance = self::calculateDistance($pointA['lat'], $pointA['lng'], $pointB['lat'], $pointB['lng']);
				$distance += $calculateDistance;

				if(is_null($timeA)){
					$timeA = $pointA['created_at'];
				}

				$timeB = $pointB['created_at'];

				if($distance >= $lastUnits){

					$time = self::calculateTime($timeA, $timeB);
					$speed = self::calculateSpeed($calculateDistance, $time) * 3.6;
					if($speed != self::BAD_SPEED_CACULATION)
					{
						if($speed >= $speedLimit)
						{
							$$speeds[0] = [
								'distance' => $calculateDistance / $distance_convertion_unit,
								'time' => self::secondsToHuman($time),
								'speed' => $speed,
								'from' => $timeA,
								'until' => $timeB,
								'pointA' => $pointA['lat'].','.$pointA['lng'],
								'pointB' => $pointB['lat'].','.$pointB['lng'],
								'pointA_id' => $pointA['id'],
								'pointB_id' => $pointB['id'],
							];
						}
						$timeA = null;
						$distance = 0;
						//debe de sumar el contador

					}else{
						unset($arrayOfPointsAndTime[$i+1]);
   						$arrayOfPointsAndTime = array_values($arrayOfPointsAndTime);
   						$totalPoints = count($arrayOfPointsAndTime);
						$distance -= $calculateDistance;
						//debe de mantenerse igual el contador
						$sumCounter = false;
					}

				}

				if($sumCounter){
					$i++;
				}
				//debe de sumar el contador

			}else{
				break;
			}
		}

		if($distance != 0) {
			$time = self::calculateTime($timeA, $timeB);
			$speed = self::calculateSpeed($distance, $time) * 3.6;

			if($speed >= $speedLimit)
			{
				$speeds[0] = [
					'distance' => $distance / $distance_convertion_unit,
					'time' => self::secondsToHuman($time),
					'speed' => $speed,
					'from' => $timeA,
					'until' => $timeB,
					'pointA' => $pointA['lat'].','.$pointA['lng'],
					'pointB' => $pointB['lat'].','.$pointB['lng']
				];
			}
		}
		return $speeds;
	}

	private static function calculateDistance($latA, $lngA, $latB, $lngB)
	{
		$dLat = deg2rad($latB - $latA);
		$dLong = deg2rad($lngB - $lngA);
		$a = sin($dLat / 2) * sin($dLat / 2) + cos( deg2rad($latA) ) * cos( deg2rad($latB) ) * sin($dLong / 2) * sin($dLong / 2);
		$c = 2 * atan2( sqrt($a), sqrt(1 - $a) );
		return self::EARTH_RADIUS * $c; // returns the distance in meter
	}

	private static function calculateTime($timeA, $timeB)
	{
		$carbonTimeA = Carbon::createFromFormat('Y-m-d H:i:s', $timeA);
		$carbonTimeB = Carbon::createFromFormat('Y-m-d H:i:s', $timeB);
		return $carbonTimeA->diffInSeconds($carbonTimeB);
	}

	private static function calculateSpeed($distance, $time)
	{
		if($time == 0){
			return self::BAD_SPEED_CACULATION;
		}

		return $distance / $time; // returns the speed in m/s
	}

	private static function validateArrayOfPoint(array $arrayOfPoints, $type = "distance")
	{
		if(is_array($arrayOfPoints)){
			$totalPoints = count($arrayOfPoints);
			if($totalPoints < 2){
				throw new Exception("There should at least 2 points on array", 1);
			}
			if($type === "distance"){
				if(!isset($arrayOfPoints[0]['lat']) || !isset($arrayOfPoints[0]['lng'])){
					throw new Exception("Given array is not a LatLng array", 1);
				}
			}elseif ($type === "speed") {
				if(!isset($arrayOfPoints[0]['lat']) || !isset($arrayOfPoints[0]['lng']) || !isset($arrayOfPoints[0]['created_at'])){
					throw new Exception("Given array is not a LatLngTime array", 1);
				}
			}else{
				throw new Exception("Validator must specify is own type: 'distance' or 'speed'", 1);
			}
			return true;
		}else{
			throw new Exception("There should be an array of points", 1);
		}
	}

	public static function secondsToHuman($secs)
	{
        $units = array(
                "sem"	=> 7*24*3600,
                "dia"	=>   24*3600,
                "hr"	=>      3600,
                "min"	=>        60,
                "seg"	=>         1,
        );
		// specifically handle zero
        if ( $secs == 0 ) return "0 seconds";
        $s = "";
        foreach ( $units as $name => $divisor ) {
                if ( $quot = intval($secs / $divisor) ) {
                        $s .= "$quot $name";
                        $s .= (abs($quot) > 1 ? "s" : "") . ", ";
                        $secs -= $quot * $divisor;
                }
        }
        
        return substr($s, 0, -2);
	}
}
