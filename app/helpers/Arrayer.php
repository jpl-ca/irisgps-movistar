<?php

class Arrayer {

	public static function trimExplode($delimiter, $string)
	{
		return array_map('trim', explode($delimiter, $string));
	}

	public static function trimImplode($glue, array $array)
	{
		return array_map('trim', implode($glue, $array));
	}

}