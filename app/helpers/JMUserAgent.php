<?php

class JMUserAgent {

	public static function isAndroidRequest()
	{
		return (Request::header('User-Agent') == 'androidjmhttp') ? true : false;
	}

	public static function isWSRequest()
	{
		return ( (Request::header('User-Agent') == 'androidjmhttp') && (Request::header('Device-Type') == 'ws') ) ? true : false;
	}

	public static function isWebRequest()
	{
		return ((Request::header('User-Agent') == 'androidjmhttp') || Request::ajax()) ? false : true;
	}

}