<?php

class FakeSpeeds extends ArrayObject
{

	const GREEN_STATE = "green";
	const YELLOW_STATE = "yellow";
	const ORANGE_STATE = "orange";
	const RED_STATE = "red";
	
	function __construct($array = null)
	{
		if(is_array($array)){
			parent::__construct($array);
		}
	}

	public function __call($func, $argv)
    {
        if (!is_callable($func) || substr($func, 0, 6) !== 'array_')
        {
            throw new BadMethodCallException(__CLASS__.'->'.$func);
        }
        return call_user_func_array($func, array_merge(array($this->getArrayCopy()), $argv));
    }

	public function current($rounded = false){
		return '-';
	}

	public function max($rounded = false){
		return '-';
	}

	public function min($rounded = false){
		return '-';
	}

	public function average($rounded = false){
		return '-';
	}

	public function statistics($rounded = false){
		return [
			'max_speed' => $this->max($rounded),
			'min_speed' => $this->min($rounded),
			'average_speed' => $this->average($rounded)
		];
	}

	public function timesOverSpeed($speedLimit)
	{		
		return '-';
	}

	public static function state($speed, $top_speed)
	{
		return self::GREEN_STATE;
	}
}