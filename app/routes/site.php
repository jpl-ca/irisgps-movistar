<?php

/*
|--------------------------------------------------------------------------
| Web app Routes
|--------------------------------------------------------------------------
*/

//Route::get('/path', array('as'=>'', 'uses'=>''));

//Route::get('/', array('as'=>'getHome', 'uses'=>'SiteController@getHome'));

Route::group(array('before' => 'guest'), function()
{	
	Route::get('login', array('as'=>'getLogin', 'uses'=>'SiteController@getLogin'));

	Route::post('login', array('as'=>'postLogin', 'uses'=>'AuthController@postLogin'));

	Route::post('recupear-password', array('as'=>'postRemind', 'uses'=>'RemindersController@postRemind'));

	Route::get('cambiar-password/{token?}', array('as'=>'getReset', 'uses'=>'RemindersController@getReset'));

	Route::post('cambiar-password', array('as'=>'postReset', 'uses'=>'RemindersController@postReset'));

    Route::get('test-notification-speed', function(){
        Artisan::call('speed:alerts', array('--fake' => null));
        return 'notifications sent!';            
    });

    Route::get('test-notification-geofences', function(){
        Artisan::call('geofence:validate', array('--fake' => null));
        return 'notifications sent!';
    });

    Route::get('test-notification-new-vehicle', function(){
        $pusher = new Pusher();     
        $devices = Device::wherenotNull('gcm')->select('gcm')->lists('gcm');
        $pusher->push_type = 'NEW_VEHICLE';
        $pusher->addRecipent($devices);
        $pusher->send();
        return 'notifications sent!';
    });
});


Route::group(array('before' => 'auth'), function()
{
    Route::get('perfil', array('as'=>'getEditProfile', 'uses'=>'UsersController@getEditUser'));

    Route::post('perfil', array('as'=>'postEditProfile', 'uses'=>'UsersController@postEditUser'));

    Route::get('logout', array('as'=>'getLogout', 'uses'=>'AuthController@getLogout'));


    Route::group(array('before' => 'admin'), function() {

        Route::get('/', array('as'=>'getHome', 'uses'=>'SiteController@getHome'));

        Route::get('eliminar/{class}/{id}', array('as'=>'delete', 'uses'=>'AuthController@delete'));

        /** MANUAL DE USUARIO **/
        foreach ((new UserManualController)->pages as $key => $value) {
            Route::get($value['uri'], array('as'=>$key, 'uses'=>'UserManualController@'.$key));
        }

        Route::group(array('prefix' => 'vehiculos', 'before' => ''), function()
        {
            Route::get('posiciones', array('as'=>'getVehiclesPositions', 'uses'=>'VehiclesController@getVehiclesPositions'));

            Route::get('ruta-de-vehiculo/{id}', array('as'=>'getTrackingRouteById', 'uses'=>'TrackingRoutesController@getTrackingRouteById'));

            Route::get('recorrido-de-vehiculo/{plate}/{date?}', array('as'=>'getVehiclePathByDate', 'uses'=>'VehiclesController@getVehiclePathByDate'));

            Route::get('todo', array('as'=>'getAllVehicles', 'uses'=>'VehiclesController@getAllVehicles'));

            Route::get('agregar', array('as'=>'getCreateVehicle', 'uses'=>'VehiclesController@getCreateVehicle'));

            Route::post('agregar/{route?}/{withInput?}', array('as'=>'postCreateVehicle', 'uses'=>'VehiclesController@postCreateVehicle'));

            Route::get('editar/{vehicle_id}', array('as'=>'getEditVehicle', 'uses'=>'VehiclesController@getEditVehicle'));

            Route::post('editar/{vehicle_id}', array('as'=>'postEditVehicle', 'uses'=>'VehiclesController@postEditVehicle'));
            
            Route::get('buscar-recorrido', array('as'=>'getSearchVehiclePath', 'uses'=>'TrackingRoutesController@getSearchVehiclePath'));

        });

        Route::group(array('prefix' => 'usuarios', 'before' => ''), function()
        {
            Route::get('todo', array('as'=>'getAllUsers', 'uses'=>'UsersController@getAllUsers'));

            Route::get('agregar', array('as'=>'getCreateUser', 'uses'=>'UsersController@getCreateUser'));

            Route::post('agregar', array('as'=>'postCreateUser', 'uses'=>'UsersController@postCreateUser'));

            Route::get('editar/{id?}', array('as'=>'getEditUser', 'uses'=>'UsersController@getEditUser'));

            Route::post('editar/{id?}', array('as'=>'postEditUser', 'uses'=>'UsersController@postEditUser'));

        });

        Route::group(array('prefix' => 'clientes', 'before' => ''), function()
        {
            Route::get('/todo', array('as'=>'getAllCustomers', 'uses'=>'CustomersController@getAllCustomers'));

            Route::get('agregar', array('as'=>'getCreateCustomer', 'uses'=>'CustomersController@getCreateCustomer'));
            
            Route::post('agregar', array('as'=>'postCreateCustomer', 'uses'=>'CustomersController@postCreateCustomer'));

            Route::get('editar/{id}', array('as'=>'getEditCustomer', 'uses'=>'CustomersController@getEditCustomer'));
            
            Route::post('editar/{id}', array('as'=>'postEditCustomer', 'uses'=>'CustomersController@postEditCustomer'));

            Route::get('importar-data', array('as' => 'getImportDataCustomer', 'uses' => 'CustomersController@getImportData'));

            Route::post('importar-data', array('as' => 'postImportDataCustomer', 'uses' => 'CustomersController@postImportData'));

        });

        Route::group(array('prefix' => 'geo-cercas', 'before' => ''), function()
        {
            Route::get('/todo', array('as'=>'getAllGeoFences', 'uses'=>'GeoFencesController@getAllGeoFences'));

            Route::get('agregar', array('as'=>'getCreateGeoFence', 'uses'=>'GeoFencesController@getCreateGeoFence'));
            
            Route::post('agregar', array('as'=>'postCreateGeoFence', 'uses'=>'GeoFencesController@postCreateGeoFence'));

            Route::get('editar/{id}', array('as'=>'getEditGeoFence', 'uses'=>'GeoFencesController@getEditGeoFence'));

            Route::post('editar/{id}', array('as'=>'postEditGeoFence', 'uses'=>'GeoFencesController@postEditGeoFence'));

            Route::get('asignar-vehiculos/{id}', array('as'=>'getAssignVehicles', 'uses'=>'GeoFencesController@getAssignVehicles'));

            Route::post('asignar-vehiculos/{id}', array('as'=>'postAssignVehicles', 'uses'=>'GeoFencesController@postAssignVehicles'));


        });

        Route::group(array('prefix' => 'dispositivos', 'before' => ''), function()
        {
            Route::get('todo', array('as'=>'getAllDevices', 'uses'=>'DevicesController@getAllDevices'));

            Route::get('agregar', array('as'=>'getCreateDevice', 'uses'=>'DevicesController@getCreateDevice'));

            Route::post('agregar', array('as'=>'postCreateDevice', 'uses'=>'DevicesController@postCreateDevice'));

            Route::get('editar/{id}', array('as'=>'getEditDevice', 'uses'=>'DevicesController@getEditDevice'));

            Route::post('editar/{id}', array('as'=>'postEditDevice', 'uses'=>'DevicesController@postEditDevice'));
        });

        Route::group(array('prefix' => 'empleados', 'before' => ''), function()
        {
            Route::get('todo', array('as'=>'getAllEmployees', 'uses'=>'EmployeesController@getAllEmployees'));

            Route::get('agregar', array('as'=>'getCreateEmployee', 'uses'=>'EmployeesController@getCreateEmployee'));

            Route::post('agregar', array('as'=>'postCreateEmployee', 'uses'=>'EmployeesController@postCreateEmployee'));

            Route::get('editar/{employee_id}', array('as'=>'getEditEmployee', 'uses'=>'EmployeesController@getEditEmployee'));

            Route::post('editar/{employee_id}', array('as'=>'postEditEmployee', 'uses'=>'EmployeesController@postEditEmployee'));
        });

        Route::group(array('prefix' => 'rutas', 'before' => ''), function()
        {
            Route::get('todo', array('as'=>'getAllTrackingRoutes', 'uses'=>'TrackingRoutesController@getAllTrackingRoutes'));

            Route::get('agregar', array('as'=>'getCreateTrackingRoute', 'uses'=>'TrackingRoutesController@getCreateTrackingRoute'));

            Route::post('agregar', array('as'=>'postCreateTrackingRoute', 'uses'=>'TrackingRoutesController@postCreateTrackingRoute'));

            Route::get('confirmar/{tracking_route_id}', array('as'=>'getConfirmCreation', 'uses'=>'TrackingRoutesController@getConfirmCreation'));

            Route::post('confirmar/{tracking_route_id}', array('as'=>'postConfirmCreation', 'uses'=>'TrackingRoutesController@postConfirmCreation'));

            Route::post('agregar-comentario', array('as'=>'postCreateRouteComment', 'uses'=>'TrackingRoutesController@postCreateRouteComment'));

            Route::get('buscar', array('as'=>'getSearchTrackingRoute', 'uses'=>'TrackingRoutesController@getSearchTrackingRoute'));

            Route::post('agregar-pasajero/{tracking_route_id}', array('as'=>'addPassenger', 'uses'=>'TrackingRoutesController@addPassenger'));

            Route::post('agregar-cliente/{tracking_route_id}', array('as'=>'addVisitToCustomer', 'uses'=>'TrackingRoutesController@addVisitToCustomer'));

            Route::get('borrar-pasajero/{passenger_id}', array('as'=>'deletePassenger', 'uses'=>'TrackingRoutesController@deletePassenger'));

        });

        Route::group(array('prefix' => 'reportes', 'before' => ''), function()
        {
            Route::get('tareas-por-semana', array('as'=>'getTasksPerWeek', 'uses'=>'ReportsController@getTasksPerWeek'));
            Route::get('incidentes-por-semana', array('as'=>'getIncidentsPerWeek', 'uses'=>'ReportsController@getIncidentsPerWeek'));
            Route::get('excesos-de-velocidad', array('as'=>'getVehicleSpeeds', 'uses'=>'ReportsController@getVehicleSpeeds'));
            Route::get('excesos-de-velocidad-exportar', array('as'=>'getExportVehicleSpeeds', 'uses'=>'ReportsController@getExportVehicleSpeeds'));
            Route::get('salidas-de-geocercas', array('as'=>'getGeofencesAccess', 'uses'=>'ReportsController@getGeofencesAccess'));
            Route::get('salidas-de-geocercas-exportar', array('as'=>'getExportGeofencesAccess', 'uses'=>'ReportsController@getExportGeofencesAccess'));
            Route::get('consumo-de-combustible', array('as'=>'getGasConsumption', 'uses'=>'ReportsController@getGasConsumption'));
            Route::get('consumo-de-combustible-exportar', array('as'=>'getGasConsumptionExport', 'uses'=>'ReportsController@getGasConsumptionExport'));

        });
            
        Route::get('configuraciones', array('as'=>'getConfigurations', 'uses'=>'ConfigurationsController@getConfigurations'));
        Route::post('configuraciones', array('as'=>'postConfigurations', 'uses'=>'ConfigurationsController@postConfigurations'));

    });

});
