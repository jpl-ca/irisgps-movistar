<?php

/*
|--------------------------------------------------------------------------
| API Services Routes
|--------------------------------------------------------------------------
*/

Route::group(array('prefix' => 'api', 'before' => ''), function()
{

    //Route::get('/path', array( 'as'=>'', 'uses'=>''));

	Route::get('vehicles-last-positions', array('as'=>'vehicles-last-positions', 'uses'=>'VehicleService@vehiclesLastPositions'));
	
	Route::post('store-locations', array('as'=>'api-mo-store-locations', 'uses'=>'LocationHistoriesController@postStoreLocations'));

	Route::get('store-locations', function(){

		$responsor = new Responsor();

		$vehicle_id = Input::get('vehicle_id', null);
		$lat = Input::get('lat', null);
		$lng = Input::get('lng', null);
		$speed_custom = Input::get('speed_custom', null);
		$movement = Input::get('movement', null);
		$contact_open = Input::get('contact_open', null);
		$created_at = Input::get('created_at', null);

		if( !is_null($vehicle_id) && !is_null($lat) && !is_null($lng) && !is_null($speed_custom) && !is_null($movement) && !is_null($contact_open) && !is_null($created_at) )
		{
			$lh = new LocationHistory;

			// $created_at = Carbon::createFromFormat('Y-n-j G:i:s', $created_at, 'America/Lima')->toDateTimeString();
			$created_at = Carbon::now()->toDateTimeString();

			$lh->vehicle_id = $vehicle_id;
			$lh->lat = $lat;
			$lh->lng = $lng;
			$lh->speed_device = $speed_custom / 3.6;
			$lh->speed_custom = $speed_custom;
			$lh->movement = $movement;
			$lh->contact_open = $contact_open;
			$lh->created_at = $created_at;
			$lh->updated_at = $created_at;

			$lh->save();

			$responsor->message = "las posiciones se han registrado con éxito"; //retorna un response invalido pero no actualiza el Token
        	return $responsor->response();

		}else{
			$responsor->isInvalid();
            $responsor->message = "los datos no están completos"; //retorna un response invalido pero no actualiza el Token
            return $responsor->response();
		}
	});

	Route::get('get-current-system-date', function(){
		return Carbon::now()->toDateTimeString();
	});

	Route::get('register-gcm/{imei}/{gcm}', array('as'=>'api-mo-register-gcm', 'uses'=>'DevicesController@updateGCMx'));

	Route::get('delete-file', function(){
		$handle = fopen (public_path("data/locations.json"), "w+");
		fclose($handle);
	});

	Route::get('view-file', function(){
		$path = public_path("data/locations.json");
		$stored = json_decode(file_get_contents($path));
		return View::make('test')->with(array("stored" => $stored));
	});


});