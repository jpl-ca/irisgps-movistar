<?php

class GeofenceRecord extends IrisModel {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['id', 'geofence_id', 'vehicle_id', 'entrance', 'departure', 'entrance_latlng', 'departure_latlng'];

	public function vehicle()
    {
        return $this->belongsTo('Vehicle', 'vehicle_id','id');
    }

	public function geofence()
    {
        return $this->belongsTo('Geofence', 'geofence_id','id');
    }

}