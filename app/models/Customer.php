<?php

class Customer extends IrisModel {

	// Add your validation rules here
	public static $rules = [
		'name' => 'required',
		'customer_code' => 'required|unique:customers,customer_code',
		'address' => '',
		'phone' => 'required|numeric',
		'mobile' => 'numeric',
		'lat' => 'required',
		'lng' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['customer_code','name', 'phone', 'mobile'];

	public function getUpdateRules()
	{
		return $rules = [
			'name' => 'required',
			'customer_code' => 'required|unique:customers,customer_code'.( is_null($this->id) ? '' : ','.$this->id ),
			'address' => '',
			'phone' => 'required|numeric',
			'mobile' => 'numeric',
			'lat' => 'required',
			'lng' => 'required'
		];
	}

}