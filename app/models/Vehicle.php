<?php

class Vehicle extends IrisModel {

	// Add your validation rules here
	public static $rules = [
		'plate' => array('required', 'regex:/[A-Z]{1}[0-9,A-Z]{1}[A-Z]{1}-[0-9]{3}/'),
		'brand' => 'required',
		'model' => 'required',
		'color' => 'required',
        'fuel_performance' => 'required',
        'speed_limit' => 'required|integer'
	];

	// Don't forget to fill this array
	protected $fillable = [];
/*
	public function unpair()
	{
		$this->device_id = null;
	}*/

    public function getUpdateRules()
    {
        return $rules = [
            'plate' => array('required', 'regex:/[A-Z]{1}[0-9,A-Z]{1}[A-Z]{1}-[0-9]{3}/'),
            'brand' => 'required',
            'model' => 'required',
            'color' => 'required',
            'fuel_performance' => 'required',
            'speed_limit' => 'required|integer'
        ];
    }

	public function vehicleHistory()
    {
        return $this->hasMany('RouteVehicleHistory','vehicle_id','id');
    }

	public function trackingRoutes()
    {
        return $this->hasMany('TrackingRoute','vehicle_id','id');
    }

	public function todayTrackingRoute()
    {
        return $this->hasMany('TrackingRoute','vehicle_id','id');
    }

    public function locations()
    {
        return $this->hasMany('LocationHistory', 'vehicle_id', 'id');
    }

    public function lastLocation()
    {
        return $this->hasMany('LocationHistory', 'vehicle_id', 'id');
    }

    public function todayPath()
    {
        return $this->hasMany('LocationHistory', 'vehicle_id', 'id');
    }

    public function locationHistories()
    {
        return $this->hasMany('LocationHistory','vehicle_id','id');
    }

	public function device()
    {
		return $this->belongsTo('Device','device_id','id');
    }

    public function hasTrackingRoute($date = null)
    {
    	$date = is_null($date) ? Carbon::now() : Carbon::createFromFormat('d-m-Y', $date);
    	$tracking_route = TrackingRoute::where('date', '>=', $date->startOfDay()->toDateTimeString())
		    ->where('date', '<=', $date->endOfDay()->toDateTimeString())
		    ->where('vehicle_id', $this->id)
		    ->first();

		return (!is_null($tracking_route)) ? $this->getTrackingRouteById($tracking_route->id) : null;
    }

    public function getTrackingRouteById($tracking_route_id)
    {
    	return TrackingRoute::with([
			'tasks.state',
			'tasks.customer',
			'tasks.stateHistory.state',
			'passengers.employee.job',
			'passengers.type',
			'comments.user'
		])->find($tracking_route_id);
    }

    public function hasTrackingRouteLocationHistory($date = null)
    {
    	$date = is_null($date) ? Carbon::now() : Carbon::createFromFormat('d-m-Y', $date);
    	$tracking_route = TrackingRoute::where('date', '>=', $date->startOfDay()->toDateTimeString())
		    ->where('date', '<=', $date->endOfDay()->toDateTimeString())
		    ->where('vehicle_id', $this->id)
		    ->first();

		return (!is_null($tracking_route)) ? $this->getLocationHistoryByTrackingRouteId($tracking_route->id) : null;
    }

    public function getLocationHistoryByTrackingRouteId($tracking_route_id)
    {
    	return LocationHistory::leftJoin('tracking_routes', 'tracking_routes.id', '=', 'location_histories.tracking_route_id')
        ->with([
			'incidentType'
		])
		->where('tracking_route_id', $tracking_route_id)
		->where('vehicle_id', $this->id)
		->orderBy('created_at', 'DESC')
		->get();
    }

    public function hasLocationHistory($date = null)
    {
    	$location_history = $this->getLocationHistoryByDate($date);

		return (!is_null($location_history->first())) ? $location_history : null;
    }

    public function getLocationHistoryByDate($date = null)
    {
    	$date = is_null($date) ? Carbon::now() : Carbon::createFromFormat('d-m-Y', $date);

        return LocationHistory::with([
                'incidentType'
            ])
            ->where('vehicle_id', $this->id)
            ->where('created_at', '>=', $date->startOfDay()->toDateTimeString())
            ->where('created_at', '<=', $date->endOfDay()->toDateTimeString())
            ->orderBy('id', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function lastPositions($quantity = -1, $when = 'last_date')
    {
        if($when == 'last_date'){
            $when = LocationHistory::where('vehicle_id', $this->id)
            ->orderBy('created_at', 'DESC')->first()->created_at->format('Y-m-d');
        }

        $validator = Validator::make(
            ['when' => $when],
            ['when' => 'required|date_format:Y-m-d']
        );

        if ($validator->fails())
        {
            $from = Carbon::now()->startOfDay();
        }else{
            $from = Carbon::createFromFormat('Y-m-d', $when)->startOfDay();
        }

        $until = $from->copy()->endOfDay();

        $last_position = LocationHistory::with([
                'incidentType'
            ])
            ->where('vehicle_id', $this->id)
            ->where('created_at', '>=', $from->toDateTimeString())
            ->where('created_at', '<=', $until->toDateTimeString())
            ->orderBy('created_at', 'DESC');
        if($quantity < 1){
            $last_position = $last_position->get();
        }else{
            $last_position = $last_position->take($quantity)->get();
        }

        return (!is_null($last_position)) ? $last_position : null;
    }

    public function lastPositionInRange($from, $until)
    {
        $last_position = LocationHistory::where('vehicle_id', $this->id)
            ->where('created_at', '>=', $from)
            ->where('created_at', '<=', $until)
            ->orderBy('created_at', 'DESC')->first();

        return (!is_null($last_position)) ? $last_position : null;
    }

    public function hasLastPosition($tracking_route_id = null)
    {   
        if(is_null($tracking_route_id))
        {
            $last_position = LocationHistory::with([
                    'incidentType'
                ])
                ->where('vehicle_id', $this->id)
                ->orderBy('created_at', 'DESC')
                ->first();
        }else{
            $last_position = LocationHistory::where('tracking_route_id', $tracking_route_id)
                ->with([
                    'incidentType'
                ])
                ->where('vehicle_id', $this->id)
                ->orderBy('created_at', 'DESC')
                ->first();
        }

		return (!is_null($last_position)) ? $last_position : null;
    }

}