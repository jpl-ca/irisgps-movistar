<?php

class Geofence extends IrisModel {

	// Add your validation rules here
	public static $rules = [
    	'name' => 'required',
    	'polygon' => 'required'
    ];

	// Don't forget to fill this array
	protected $fillable = [];

	public function vehiclesIds()
    {
        return Vehicle::whereIn('id', GeofenceVehicle::whereGeofenceId($this->id)->lists('vehicle_id'))->lists('id');
    }

    public function records()
    {
        return $this->hasMany('GeofenceRecord','geofence_id','id');
    }

}