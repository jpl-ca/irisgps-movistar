<?php

class Configuration extends IrisModel {
	
	public static $rules = [
		'geofences_notification_targets' => 'required|email',
		'geofences_validation_frecuency' => 'required|integer'
	];

	// Don't forget to fill this array
	protected $fillable = ['id', 'key', 'value'];

}