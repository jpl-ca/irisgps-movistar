<?php

class GeofenceVehicle extends IrisModel {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['id', 'geofence_id', 'vehicle_id'];

}