@extends('layouts.base')

@section('body')

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default login-box-shadow">
                    <div class="iris-background2 text-center">
                        <h3 class="panel-title"><img src="{{asset('assets/images/irisgps-logo-largo_inv.png')}}" alt="" style="height:55px"></h3>
                    </div>
                    <div class="panel-body">

                        <!--ALERT ZONE -->
                        @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{Session::get('success')}}
                            </div>
                        @endif
                        @if(Session::has('info'))
                            <div class="alert alert-info alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{Session::get('info')}}
                            </div>
                        @endif
                        @if(Session::has('warning'))
                            <div class="alert alert-warning alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{Session::get('warning')}}
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('validation_errors'))
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Se han encontrado los siguientes errores:
                                <ul>
                                    @foreach (Session::get('validation_errors') as $validation_error)
                                        @foreach ($validation_error as $error_message)
                                            <li>{{$error_message}}</li>
                                        @endforeach
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                                    <!--END ALERT ZONE -->

                            <form role="form" action="{{ route('postLogin') }}" method="POST">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="login form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="login form-control" placeholder="Contraseña" name="password" type="password" value="">
                                    </div>
                                    {{--
                                    <div class="checkbox">
                                        <label>
                                            <input name="remember" type="checkbox" value="Remember Me">Recordarme
                                        </label>
                                    </div>
                                    --}}
                                    <!-- Change this to a button or input when using this as a form -->
                                    <button type="submit" class="btn btn-lg btn-login btn-block">Iniciar Sesión</button>
                                </fieldset>
                                <p class="password-reminder-link"><a href="javascript:void(0)" class="iris-link pull-right" data-toggle="modal" data-target="#passwordReminder">¿Has olvidado tu contraseña?</a></p>
                            </form>
                    </div>
                    {{--
                    <div class="panel-footer text-center">
                        <p><img src="{{asset('assets/images/logoSGTEL_color-H40.png')}}" alt=""></p>
                    </div>
                    --}}
                </div>
                {{--
                <a class="pull-right" href="https://play.google.com/store/apps/details?id=jmt.irisgps.cobratrack" target="_blank">
                    <img alt="IrisGPS para Cobra en Google Play" src="https://developer.android.com/images/brand/es-419_app_rgb_wo_45.png" />
                </a>
                --}}
                <!-- Modal -->
                <div class="modal fade" id="passwordReminder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Recuperar contraseña</h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal" action="{{ action('RemindersController@postRemind') }}" method="POST">
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">E-mail</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" name="email" id="email" placeholder="" autocomplete="off">
                                        </div>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <input type="submit" class="btn btn-primary" value="Recuperar contraseña">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop