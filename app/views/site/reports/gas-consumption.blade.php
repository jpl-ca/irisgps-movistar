@extends('layouts.dashboard-base')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="hidden" id="current_date" date="{{Carbon::now()}}"></div>
            {{$templator->getForm()}}
        </div>
    </div>
    @if(!empty($data))
    	<br>
		<div class="row">
        	<div class="col-lg-12 text-center">
                @if( $vehicle === 0 )
                <h3>Consumo de combustible del todos los vehículos</h3>
                    <a href='{{ route("getGasConsumptionExport")."?vehicle_id=$vehicle&from=$from_date&until=$until_date" }}' class="btn btn-success pull-right"><i class="fa fa-file-excel-o"></i> Exportar</a>
                @else
        		<h3>Consumo de combustible del vehículo: {{ $vehicle->plate }}
                    <a href='{{ route("getGasConsumptionExport")."?vehicle_id=$vehicle->id&from=$from_date&until=$until_date" }}' class="btn btn-success pull-right"><i class="fa fa-file-excel-o"></i> Exportar</a>
                @endif
                    <br>
        			<small>del {{ $from_date }} al {{ $until_date }}</small>
        		</h3>
        	</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				@include("site.reports.export.gas-consumption")                     
			</div>
		</div>
	@else
		<br>
		<br>
		<div class="row">
			<div class="col-lg-12 text-center">
				<h5>No hay datos</h5>
			</div>
		</div>
    @endif
@stop

@section('js')
    {{$templator->getScripts()}}
    <script>
        $(function () {
            var now = $('#current_date').attr('date');

            $('#from').datetimepicker({
                locale: 'es',
                format: 'DD/MM/YYYY',
                /*minDate: "2015-06-12",*/
                sideBySide: true,
                defaultDate: now
            });

            $('#until').datetimepicker({
                locale: 'es',
                format: 'DD/MM/YYYY',
                /*minDate: "2015-06-12",*/
                sideBySide: true,
                defaultDate: now
            });
        });
    </script>
@stop