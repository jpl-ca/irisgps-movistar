<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Geo-Cerca</th>
            <th>Vehículo</th>
            <th>Fecha de salida</th>
            <th>Fecha de entrada</th>
            <th>Tiempo fuera de la geocerca</th>
        </tr>
    </thead>
    <tbody>
    	{{---*/

			$i = 1;

    	/*--}}
    	@foreach($data as $d)
        <tr>
        	<td>{{ $i }}</td>
            <td>{{ $d->geofence->name }}</td>
            <td>{{ $d->vehicle->plate }}</td>
            <td>{{ $d->departure }} </td>
            <td>{{ $d->entrance }} </td>
            <td>{{ (new Carbon($d->departure))->diffInMinutes((new Carbon($d->entrance)), true) }} min </td>
        </tr>
    	{{---*/
			$i++;
    	/*--}}
    	@endforeach
    </tbody>
</table>