<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>distancia</th>
            <th>tiempo</th>
            <th>velocidad</th>
            <th>desde</th>
            <th>hasta</th>
            <th>conductor</th>
        </tr>
    </thead>
    <tbody>
    	{{---*/

			$i = 1;
			$tr = $vehicle->hasTrackingRoute(Carbon::createFromFormat('d/m/Y', $from_date)->format('d-m-Y'));
			$driver = '-';
			if(!is_null($tr))
			{
				$emp = Employee::find( $tr->getDriverId() );
				$driver = $emp->first_name.' '.$emp->last_name;
			}

    	/*--}}
    	@foreach($data as $d)
        <tr>
        	<td>{{ $i }}</td>
            <td>{{ round($d['distance'], 4) }} km</td>
            <td>{{ $d['time'] }}</td>
            <td>{{ $d['speed'] }} km/hr</td>
            <td>{{ $d['from'] }}</td>
            <td>{{ $d['until'] }}</td>
            <td>{{ $driver }}</td>
        </tr>
    	{{---*/
			$i++;
    	/*--}}
    	@endforeach
    </tbody>
</table>