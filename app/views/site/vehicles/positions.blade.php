@extends('layouts.dashboard-base-fwc')

@section('content')
	<div class="row">
        <div class="hidden">
            <div id="modal_button" style="padding: 15px;">
                <a href="javascript:void(0)" style="background: none" data-toggle="modal" data-target="#dataModal">
                    <i class="fa fa-car fa-fw icon-material-share btn btn-primary" style="font-size: 30px; padding-left: 14px; padding-top: 14px; height: 60px;">
                    <div class="share-ripple ripple-wrapper"></div>
                   </i>
                </a>  
            </div>
        </div>        

        <div class="full-width-div" id="map-canvas"></div>

        <!-- Modal -->
        <div class="modal fade" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

            		<div class="modal-body">
            			<h2 class="page-subtitle">Vehículos <small><a href="javascript:void(0)" class="displayAllMarkers">ver todos</a></small></h2>

                        <table class="table table-striped table-bordered table-hover" id="all-elements-datatable">
                            <thead>
                                <tr>
                                    <th>Placa</th>
                                    <th>Últ. Pos</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $vehicle)
                                <tr class="">
                                    <td>{{$vehicle->plate}}</td>
                                    <td>{{(is_null($vehicle->hasLastPosition())) ? '-' : $vehicle->hasLastPosition()->getCreatedAtForHumans()}}</td>
                                    <td class="center">
                                        @if(!is_null($vehicle->hasLastPosition()))
                                        {{--*/
                                            $markerImage = null;
                                            $vehicleState = null;
                                            $lastPosition = $vehicle->hasLastPosition();
                                            $date = $lastPosition->created_at;
                                            // $path = $lastPosition->toArray();
                                            $path = $vehicle->lastPositions(2, $date->copy()->format('Y-m-d'))->toArray();

                                            if(count($path) < 2)
                                            {
                                                $speeds = 0;
                                            }else
                                            {

                                                $speeds = GeoPos::speed($path, 0.01, 'km');
                                                $speeds = $speeds->current(true);
                                            }
                                            // $speeds = $lastPosition->speed_custom;
                                            $address = GeoPos::address($lastPosition->lat, $lastPosition->lng, true);

                                            $carDate = Carbon::createFromFormat('Y-m-d H:i:s', $date);

                                            $hasTrackingRouteOnLastPositionDate = !is_null($vehicle->hasTrackingRoute($carDate->copy()->format('d-m-Y')));

                                            $hasTrackingRouteToday = !is_null($vehicle->hasTrackingRoute());

                                            $markerImage = null;
                                            
                                            if(!$lastPosition->movement && !$lastPosition->contact_open)
                                            {
                                                if( $hasTrackingRouteToday )
                                                {
                                                    $markerImage = "parked";
                                                    $vehicleState = "Parado";
                                                    $speeds = 0;
                                                }else{
                                                    $markerImage = "parked_no_active";
                                                    $vehicleState = "Parado, no activo";
                                                    $speeds = 0;
                                                }
                                            }
                                            elseif(!$lastPosition->movement && $lastPosition->contact_open)
                                            {
                                                $markerImage = "stoped";
                                                $vehicleState = "Detenido";
                                                $speeds = 0;
                                            }

                                            if( $hasTrackingRouteOnLastPositionDate && !is_null($markerImage)){
						if($vehicle->hasTrackingRoute()){
                                                $driver = $vehicle->hasTrackingRoute()->passengers()->where('passenger_type_id', 1)->first()->employee;
                                                $driver = $driver->last_name.', '.$driver->first_name;
						}else{
						   $driver = "";
						}
                                                if($carDate->isSameDay(Carbon::now()))
                                                {       
                                                    $markerImage = is_null($markerImage) ? "moving_" : $markerImage;
                                                    $vehicleState = is_null($markerImage) ? "Activo, en movimiento" : $vehicleState;
                                                }else{
                                                    $markerImage = is_null($markerImage) ? "no_active_" : $markerImage;
                                                    $vehicleState = is_null($markerImage) ? "No activo, en movimiento" : $vehicleState;
                                                }
                                            }else{
                                                $driver = '-';
                                                $markerImage = is_null($markerImage) ? "no_active_" : $markerImage;
                                                $vehicleState = is_null($markerImage) ? "No activo, en movimiento" : $vehicleState;
                                            }

                                            $markerImage = in_array($markerImage, ["moving_", "no_active_"]) ? $markerImage . Speeds::state($speeds, $vehicle->speed_limit) : $markerImage;

                                        /*--}}
                                        <a class="iris-link vehicleMarker" href="javascript:void(0)" title="ver la última posicion registrada" lat="{{$vehicle->hasLastPosition()->lat}}" lng="{{$vehicle->hasLastPosition()->lng}}" plate="{{$vehicle->plate}}" icon="{{URL::route('getHome')}}/assets/images/markers/{{$markerImage}}.png" id="{{$vehicle->id}}" path_link="{{ (!is_null($vehicle->hasLocationHistory())) ? route('getVehiclePathByDate', $vehicle->plate) : ''  }}" route_link="{{ (!is_null($vehicle->hasTrackingRoute())) ? route('getTrackingRouteById', $vehicle->hasTrackingRoute()->id) : '' }}" marker_data="<div><h4 style='text-decoration:underline'>{{ $vehicle->plate }}</h4>
                                        <p>
                                        <strong>Estado: </strong>{{ $vehicleState }}<br>
                                        <strong>Dirección: </strong>{{ $address }}<br>
                                        @if(!is_null($speeds))
                                        <strong>Velocidad: </strong>{{ $speeds }} km/h<br>
                                        @else
                                        <strong>Velocidad: </strong>-<br>
                                        @endif
                                        <strong>Fecha: </strong>{{ $date }}<br>
                                        <strong>Conductor: </strong>{{ $driver }}</p>

                                        {{--

                                        <p style='text-align:center'>posición registrada:{{$vehicle->hasLastPosition()->getCreatedAtForHumans()}}</p>
                                        @if(!is_null($vehicle->hasLocationHistory()))
                                        <p style='text-align:center'>
                                        <a class='btn btn-default' href='{{route('getVehiclePathByDate', $vehicle->plate)}}'>ver recorrido</a>
                                        </p>
                                        @endif
                                        @if(!is_null($vehicle->hasTrackingRoute()))
                                        <p style='text-align:center'>
                                        <a class='btn btn-default' href='{{route('getTrackingRouteById', $vehicle->hasTrackingRoute()->id)}}'>ver ruta de hoy</a>
                                        </p>
                                        @endif

                                        --}}

                                        ">

                                            <i class="fa fa-eye fa-fw"></i>
                                        </a>
                                        @endif
                                        @if(!is_null($vehicle->hasTrackingRoute()))
                                        <a class="iris-link" href="{{ route('getTrackingRouteById', $vehicle->hasTrackingRoute()->id) }}" title="ver la ruta de hoy">
                                            <i class="fa fa-map-marker fa-fw"></i>
                                        </a>
                                        @endif
                                        @if(!is_null($vehicle->hasLocationHistory()))
                                        <a class="iris-link" href="{{ route('getVehiclePathByDate', $vehicle->plate) }}" title="ver el recorrido de hoy">
                                            <i class="fa fa-road fa-fw"></i>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

            		</div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

	</div>
@stop

@section('js')
    <script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=drawing,places,geometry"></script>

    @include('javascript.datatable-init')

    <script>
        var map;
        var markers = [];
        var allMarkers = [];
        var center = new google.maps.LatLng(-12.0879227, -77.0159834);

        function initialize() {
            var styles = [
              {
                stylers: [
                  { hue: "#607D8B" },
                  { saturation: -20 }
                ]
              },{
                featureType: "road",
                elementType: "geometry",
                stylers: [
                  { lightness: 100 },
                  { visibility: "simplified" }
                ]
              },{
                featureType: "road",
                elementType: "labels",
                stylers: [
                  { visibility: "on" }
                ]
              }
            ];

            var mapOptions = {
                zoom: 10,
                center: center,
                styles: styles,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('modal_button'));

            // Adds a Places search box. Searching for a place will center the map on that
            // location.

            setListeners(); //incializa los listeners
            getAllVehicleMarkers();
            existsPreviusPolygon();
        }



        function existsPreviusPolygon()
        {
            @forelse($geofences as $geofence)

                var poly = {{ $geofence->points }};

                if(poly != '')
                {
                    info = "<h4>{{ $geofence->name }}</h4>";
                    drawPolygon( poly, info );
                }

            @empty
            @endforelse
        }

        function drawPolygon(points, info)
        {
            var polygono = new google.maps.Polygon({
              paths: points,
              strokeColor: "#00527A",
              strokeOpacity: 1,
              strokeWeight: 2,
              fillColor: "#00527A",
              fillOpacity: 0.5
            });


            var infowindow = new google.maps.InfoWindow({
                content: info
            });

            google.maps.event.addListener(polygono, 'click', function(event) {
                infowindow.setPosition(event.latLng);
                infowindow.open(map, polygono);
            });

            polygono.setMap(map);
        }

        function setListeners() //listeners
        {
            $('.vehicleMarker').click(function(){
                $('#dataModal').modal('hide');
                id = $(this).attr('id');
                vlat = $(this).attr('lat');
                vlng = $(this).attr('lng');
                plate = $(this).attr('plate');
                icon = $(this).attr('icon');
                route_link = $(this).attr('route_link');
                path_link = $(this).attr('path_link');
                marker_data = $(this).attr('path_link');


                lat = parseFloat(vlat);
                lng = parseFloat(vlng);

                console.log(plate);
                console.log(lat);
                console.log(lng);
                console.log('========================');

                data = [plate, marker_data]; // title, body
                addMarker(lat, lng, data, icon);
            });

            $('.displayAllMarkers').click(function(){
                displayAll();
            });
        }

        function createMarker(lat, lng, data, icon) {

            var coordinate = new google.maps.LatLng(lat, lng);

            var marker = new google.maps.Marker({
                position: coordinate,
                map: map,
                icon:  icon,
                animation: google.maps.Animation.DROP,
                title: data[0]
            });

            var infowindow = new google.maps.InfoWindow({
                content: data[1]
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });

            return marker;
        }

        function addMarker(lat, lng, data, icon) {
            deleteMarkers();

            var coordinate = new google.maps.LatLng(lat, lng);

            var marker = new google.maps.Marker({
                position: coordinate,
                map: map,
                icon:  icon,
                animation: google.maps.Animation.DROP,
                title: data[0]
            });

            markers.push(marker);

            var infowindow = new google.maps.InfoWindow({
                content: data[1]
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });

            map.setCenter(new google.maps.LatLng(lat, lng));
            map.setZoom(10);
        }

        // Sets the map on all markers in the array.
        function setAllMap(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
            for (var i = 0; i < allMarkers.length; i++) {
                allMarkers[i].setMap(null);
            }
        }

        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
            clearMarkers();
            markers = [];
        }        

        function deleteAllMarkers() {
            clearMarkers();
            markers = [];
            allMarkers = [];
        }

        function getAllVehicleMarkers()
        {
            deleteAllMarkers();

            $(".vehicleMarker").each(function(index, obj){

                vid = $(obj).attr('id');
                vlat = $(obj).attr('lat');
                vlng = $(obj).attr('lng');
                plate = $(obj).attr('plate');
                icon = $(obj).attr('icon');
                route_link = $(this).attr('route_link');
                path_link = $(this).attr('path_link');
                marker_data = $(this).attr('marker_data');


                lat = parseFloat(vlat);
                lng = parseFloat(vlng);

                console.log(plate);
                console.log(lat);
                console.log(lng);
                console.log('========================');

                data = [plate, marker_data]; // title, body
                marker = createMarker(lat, lng, data, icon);
                allMarkers.push(marker);
            });
        }

        function displayAll()
        {
            $('#dataModal').modal('hide');
            clearMarkers();
            markers = allMarkers;
            setAllMap(map);
            map.setCenter(center);
            map.setZoom(10);
        }

        function bodyMakerPosition(plate, route_link, path_link)
        {
            if (typeof(route_link)==='undefined') route_link = '';
            if (typeof(path_link)==='undefined') path_link = '';
            var cad = "<div><h4>" + plate + "</h2>";

            if(route_link != '')
            {
                cad += "<a href='" + route_link + "' target=_blank>ver ruta</a><br>";
            }

            if(path_link != '')
            {
                cad += "<a href='" + path_link + "' target=_blank>ver recorrido</a>";
            }

            cad += "</div>";
            return cad;
        }

        $(window).load(initialize);
    </script>
@stop
