<p>Elija un empleado para agregarlo como pasajero</p>
<form class="form-inline" action="{{ route('addPassenger', $tracking_route_id)  }}" method="POST" role="form">
    {{ Templator::rawSelectBasic($id = 'employee_id', $label = 'Empleado' , $name = 'employee_id', $employees , $haveEmptyOption = false, $selected = null, $disabled=false)  }}
    <button type="submit" class="btn btn-success">Agregar</button>
</form>