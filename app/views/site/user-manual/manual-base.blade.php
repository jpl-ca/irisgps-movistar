@extends('layouts.dashboard-base')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <legend>{{ $legend }}</legend>
        </div>
    </div>
    @if(is_array($detail_view))
    @for ($i = 0; $i < count($detail_view); $i++)
        {{-- expr --}}
	<div class="row">
        <div class="col-sm-4">

            @include("site.user-manual.pages.$detail_view[$i]")

        </div>
		<div class="col-sm-8">

            <img class="img-responsive" src="{{ asset("assets/images/manual/$image[$i].png") }}" alt="imagen detalle">

		</div>
	</div>
    <hr>
    @endfor
    @else
    <div class="row">
        <div class="col-sm-4">

            @include("site.user-manual.pages.$detail_view")

        </div>
        <div class="col-sm-8">

            <img class="img-responsive" src="{{ asset("assets/images/manual/$image.png") }}" alt="imagen detalle">

        </div>
    </div>
    @endif
@stop