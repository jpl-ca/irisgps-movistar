<h3 class="sub-legend">Agregar registro</h3>
<ol class="iris-ol">
	<li>Escoger el tipo de usuario.</li>
	<li>Colocar los nombres</li>
	<li>Y apellidos</li>
	<li>escribir una dirección de correo electronico válida y que no haya sido registrada con anterioridad.</li>
	<li>Escribir una contraseña. El usuario luego podrá cambiarla desde su panel de "Perfil"</li>
	<li>Cuando todo este listo haga click en el botón "Agregar".</li>
	<li>Si quiere volver a la lista de usuarios haga click en el botón "Regresar".</li>
</ol>