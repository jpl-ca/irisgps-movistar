<ol class="iris-ol">
	<li>Indicar la fecha "desde" donde se quiere realizar la búsqueda.</li>
	<li>Citar la fecha "hasta" donde se deseen los resultados. Ambas fechas tienen que tener el formato dd/mm/yyyy.</li>
	<li>Puede escoger un vehículo de la lista y si quiere ver los resultados de todos los vehículos deje seleccionada la opción "Cualquiera".</li>
	<li>Cuando sus opciones estén listas haga click en el boton Buscar.</li>
	<li>Los resultados se mostrarán en una tabla en la parte inferior, de no haber resultados se mostrará un mensaje.</li>
	<li>Cada uno de los resultados tendrá la opcion "ver recorrido" representada por el ícono de una "pista", al hacer click en ella podremos ver el recorrido de dicho resultado.</li>
</ol>