<h3 class="sub-legend">Editar registro</h3>
<ol class="iris-ol">
    <li>Escribir el nuevo número de IMEI del dispositivo si es que se desar cambiar este campo, este campo deberá de ser único para cada dispositivo.</li>
    <li>Deseleccione la opción "Activado" para deshabilitar el uso del dispositivo.</li>
    <li>Desmarque la opción "Permitido" si desea revocar el acceso del dispositivo a las diferentes funcionalidades de la aplicación movil.</li>
    <li>Cuando todo este listo, haga click en el botón "Guardar".</li>
    <li>Caso contrario haga click en el botón "Regresar" para volver a la lista de dispositivos.</li>
</ol>