<h3 class="sub-legend">Editar registro</h3>
<ol class="iris-ol">
	<li>Si desea cambiar la placa, deberá esta de tener un formato válido y adicionalmente no deberá de existir en ningún otro registro ya que el campo Placa es un registro único.</li>
	<li>Podrá Cambiar la marca.</li>
	<li>Editar el modelo.</li>
	<li>Y el color si lo desea.</li>
	<li>Cuando todo este listo, haga click en el botón "Guardar"</li>
	<li>Caso contrario haga click en el botón "Regresar" para volver a la navegación anterior.</li>
</ol>