<h3 class="sub-legend">Agregar registro</h3>
<ol class="iris-ol">
	<li>Escribir el número de IMEI del dispositivo a registrar, este registro deberá de ser único para cada dispositivo.</li>
    <p><smal><strong>Para obtener el número IMEI de un dispositivo debera de marcar el código *#06#</strong></smal></p>
	<li>El campo "Activado" sirve para habilitar el uso del dispositivo. Si desea registrar el dispositivo pero no activarlo desmarque esta casilla.</li>
	<li>La opción "Permitido" consederá acceso al dispositivo a las diferentes funcionalidades de la aplicación movil.</li>
	<li>Cuando todo este listo, haga click en el botón "Agregar".</li>
	<li>Caso contrario haga click en el botón "Regresar" para volver a la lista de dispositivos.</li>
</ol>