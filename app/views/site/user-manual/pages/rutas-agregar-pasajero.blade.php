<h3 class="sub-legend">Agergar pasajero a ruta</h3>
<ol class="iris-ol">
	<li>Al hacer click en el botón "+ Agregar Pasajero" se desplegará una ventana.</li>
	<li>Donde deberá de elegir el nombre del pasajero.</li>
	<li>Y hacer click en el botón "Agregar" para confirmar.</li>
	<li>O en el botón "Cerrar" para cancelar.</li>
</ol>