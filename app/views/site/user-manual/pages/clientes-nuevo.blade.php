<h3 class="sub-legend">Agregar Registro</h3>
<ol class="iris-ol">
	<li>Citar el nombre del cliente, puede ser persona natural o jurídica.</li>
	<li>Colocar un código para dicho cliente.</li>
	<li>Establecer una dirección.</li>
	<li>Colocar el teléfono fijo del cliente.</li>
	<li>Y un celular de haber uno.</li>
	<li>En el mapa se mostrará la ciudad</li>
    <li>Se deberá de hacer click en cualquier parte para que aparezca un marcardo, deberá de indicar el punto de la dirección.</li>
    <li>Cuanto todo este listo hacer click en el botón "Agregar"</li>
    <li>Caso contrario haca click en el botón "Regresar" para volver a la lista de clientes.</li>
</ol>