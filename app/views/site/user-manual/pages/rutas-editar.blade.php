<h3 class="sub-legend">Editar registro</h3>
<p>Despues de crear una ruta nueva con éxito o escoga la opción editar verá esta vista.</p>
<ol class="iris-ol">
	<li>La fecha en la que se creo la ruta.</li>
	<li>El vehículo seleccionado.</li>
	<li>En este panel podrá escoger cual de los pasajeros es el conductor.</li>
	<li>Al hacer click en el ícono del "tacho" rojo que esta al costado de cada pasajero podrá eliminar ese registro.</li>
	<li>Si desea agregar un pasajero haga click en el botón "+ Agregar pasajero"</li>
	<li>vera una lista con los clientes a visitar y la descripción de la tarea que se realizará en cada una de las esas visitas.</li>
    <li>El ícono del "tacho" rojo al costado del nombre de cada cliente sirve para eliminar dicho registro.</li>
    <li>Si quiere agregar una nueva visita a un cliente haga click en el botón "+ Agregar visita a cliente".</li>
    <li>Para guardar los cambios haga click en el botón "Guardar Cambios"</li>
</ol>