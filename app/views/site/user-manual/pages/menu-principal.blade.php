<h3 class="sub-legend">Elementos</h3>
<p class="text-justify">Los elementos del menu principal son los siguientes:</p>
<ol class="iris-ol">
	<li>Menú de navegación, desde donde se podrá navegar entre los distintos módulos de la aplicación.</li>
	<li>Boton de usuario y panel de acción, el usuario podrá editar datos de su cuenta en su perfil, así mismo podrá cerrar sesión.</li>
	<li>Zona de alertas, donde se mostrarán mensajes de acuerdo a las diferentes acciones que se realicen.</li>
	<li>Enlaces de acceso rápido, te llevarán a las acciones principales de la aplicación.</li>
</ol>