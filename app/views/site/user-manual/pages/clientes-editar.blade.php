<h3 class="sub-legend">Editar Registro</h3>
<ol class="iris-ol">
    <li>Citar el nombre del cliente, puede ser persona natural o jurídica.</li>
    <li>Colocar un código para dicho cliente o dejar el anterior.</li>
    <li>Establecer una dirección.</li>
    <li>Colocar el teléfono fijo del cliente.</li>
    <li>Y un celular de haber uno.</li>
    <li>En el mapa se mostrará la el punto previamente seleccionado para moverlo, hacer click en otra parte del mapa.</li>
    <li>Cuanto todo este listo hacer click en el botón "Agregar"</li>
    <li>Caso contrario haca click en el botón "Regresar" para volver a la lista de clientes.</li>
</ol>