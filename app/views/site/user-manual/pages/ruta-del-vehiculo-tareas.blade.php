<h3 class="sub-legend">Tareas</h3>
<ol class="iris-ol">
	<li>Lista desplegable con la lista de clientes a visitar.</li>
	<li>Detalle de la tarea a realizar en la visita asi como también la información del cliente.</li>
	<li>Historial de estados de la tarea, donde se muestra el estado, la fecha en que se realizó el cambio y el detalle del cambio de estado.</li>
</ol>