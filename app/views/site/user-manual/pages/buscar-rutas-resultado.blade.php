<h3 class="sub-legend">Resultados</h3>
<ol class="iris-ol">
	<li>Los resultados se mostrarán en una tabla debajo del panel de búsqueda.</li>
    <li>El ícono del "ojo" sirve para ver el detalle de la ruta correspondiente.</li>
    <li>El ícono del "lápiz" sirve para editar el registro seleccionado.</li>
    <li>El ícono del "tacho" sirve para eliminar el registro seleccionado.</li>
</ol>