<h3 class="sub-legend">Agregar registro</h3>
<ol class="iris-ol">
	<li>Se debe de colocar una placa en el formato válido "?#?-###".</li>
	<li>Elegir una marca.</li>
	<li>Escoger un modelo.</li>
	<li>Y seleccionar un color.</li>
	<li>Cuando todo este listo, hacer click en el botón "Agregar"</li>
	<li>Si desea regresar a la lista de todos los vehículos haga click en el botón regresar.</li>
</ol>