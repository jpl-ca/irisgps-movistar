<ol class="iris-ol">
	<li>Deberá de escoger una semana como se muestra en la figura.</li>
	<li>Posteriormente haga click en el botón "Ver" para mostrar los resultados.</li>
	<li>Los resultados se mostrarán en una gráfica estadística de barras con la leyenda en la parte inferior.</li>
</ol>