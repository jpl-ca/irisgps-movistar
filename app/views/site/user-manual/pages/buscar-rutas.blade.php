<ol class="iris-ol">
	<li>Deberá de escoger una fecha de inicio para la búsqueda.</li>
	<li>Y tambíen una fecha límite.</li>
	<li>Si desea filtrar la búsqueda a un solo vehículo podrá elegirlo de la lista, caso contrario dejar seleccionada la opción "Cualquiera"</li>
	<li>Haga click en el botón "Buscar" para ejecutar la búsqueda.</li>
</ol>