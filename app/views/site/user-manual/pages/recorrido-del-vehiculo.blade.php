<ol class="iris-ol">
	<li>Pestaña de detalle de recorrido</li>
	<li>Pestaña de incidencias reportadas en ese recorrido.</li>
	<li>Se muestran los detalles del recorrido actual, fecha del día en que se realizó el recorrido y el último momento en el que se registró la última posición de vehículo en el sistema.</li>
	<li>En la pesataña detalles del vehículo se muestra la información del vehículo, tales como placa, marca, etc.</li>
	<li>Se muestra en el mapa la última posición registrada del vehículo.</li>
</ol>