<h3 class="sub-legend">Agergar pasajero a ruta</h3>
<ol class="iris-ol">
    <li>Al hacer click en el botón "+ Agregar visita a cliente" se desplegará una ventana.</li>
    <li>Donde deberá de elegir el nombre del cliente.</li>
    <li>Llenar la descripción de la tarea al realizar la visita.</li>
    <li>Y hacer click en el botón "Agregar" para confirmar.</li>
    <li>O en el botón "Cerrar" para cancelar.</li>
</ol>