<h3 class="sub-legend">Comentarios</h3>
<ol class="iris-ol">
	<li>Panel donde se muestra el historial de los comentarios, de no haber ninguno se mostrará un mensaje.</li>
	<li>Espacio para detallar un comentario nuevo.</li>
	<li>Botón de acción para agregar un nuevo comentario.</li>
</ol>