<h3 class="sub-legend">Login</h3>
<p class="text-justify">Para iniciar sesión deberá de seguir los siguientes pasos:</p>
<ol class="iris-ol">
	<li>Campo e-mail, deberá de escribir una dirección válida de correo electrónico</li>
	<li>Campo contraseña, deberá de escribir su contraseña.</li>
	<li>Botón de inicio de sesión, pulse este boton cuando haya completado los campos anteriores.</li>
	<li>Link de recuperación de contraseña, servirá para recupear su contraseña en caso de haberla olvidado</li>
</ol>