<h3 class="sub-legend">Todos los registros</h3>
<ol class="iris-ol">
    <li>Para agregar un nuevo registro, hacer click en el botón "Agregar Nuevo"</li>
    <li>El ícono del "ojo" sirve para ver el detalle de la ruta correspondiente.</li>
    <li>El ícono del "lápiz" sirve para editar el registro seleccionado.</li>
    <li>El ícono del "tacho" sirve para eliminar el registro seleccionado.</li>
</ol>