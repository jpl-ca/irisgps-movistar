<h3 class="sub-legend">Eliminar registro</h3>
<ol class="iris-ol">
    <li>Para eliminar un registro se debe de hacer click en el ícono del "tacho" del registro correspondiente.</li>
    <li>Esta acción desplegará una ventana de confirmación, de querer continuar con la eliminación elegir la opción "Si, estoy seguro"</li>
    <li>Caso contrario escoger la opción "No"</li>
</ol>