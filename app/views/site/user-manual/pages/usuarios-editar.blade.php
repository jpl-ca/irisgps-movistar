<h3 class="sub-legend">Editar registro</h3>
<ol class="iris-ol">
    <li>Escoger el tipo de usuario a cambiar</li>
    <li>Editar los nombres</li>
    <li>Y apellidos</li>
    <li>escribir una dirección de correo electronico válida y que no haya sido registrada con anterioridad o dejar la actual.</li>
    <li>Escribir una contraseña si desea cambiarla, si la deja en blanco la contraseña anterior no cambiará.</li>
    <li>Cuando todo este listo haga click en el botón "Guardar".</li>
    <li>Si quiere volver a la lista de usuarios haga click en el botón "Regresar".</li>
</ol>