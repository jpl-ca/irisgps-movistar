<h3 class="sub-legend">Editar cuenta</h3>
<p class="text-justify">En esta sección ud. podrá editar los detalles de su cuenta con excepción del tipo de cuenta.</p>
<ol class="iris-ol">
	<li>Campo de nombres</li>
	<li>Campo de apellidos</li>
	<li>Campo de e-mail, si desea cambiar de correo con el que inicia sesión basta con editar este campo.</li>
	<li>Campo de contraseña, para cambiar su contraseña debe de llenar este campo con la nueva contraseña deseada, caso contrario deberá de dejarlo en blanco.</li>
	<li>Boton guardar, al darle click toda la información modificada se procesará y actualizará.</li>
</ol>