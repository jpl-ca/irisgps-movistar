<ol class="iris-ol">
	<li>Tabla con todos los vehículos registrados y los datos de su última posición registrada.</li>
	<li>Al hacer click en "ver todos" en el mapa se mostrarán todos los vehículos representados en íconos en sus respectivas últimas posiciones</li>
	<li>El ícono del "ojo" nos mostrará en el mapa solo el vehículo seleccionado, ocultando el resto de vehículos. Este ícono se mostrará solo si el vehículo tiene alguna posición registrada en el sistema.</li>
	<li>El ícono del "marcador de mapa", nos indica que para el día de hoy (día actual) existe una "Ruta" asignada a ese vehículo, al hacer click sobre el marcador no redireccionará a los detalles de dicha ruta. Este ícono solo se mostrará si el vehículo tiene una ruta programada el día de hoy.</li>
	<li>El ícono de la "pista" nos indica que el vehículo ha tenido recorrido el día de hoy (día actual). Al hacer click sobre el ícono nos redireccionará a los detalles de ese recorrido. Este ícono solo se mostrará si el vehículo tiene algun recorrido el día de hoy.</li>
	<li>En el mapa se mostrarán las posiciones de los vehículos representadas por los íconos de un automóvil rojo.</li>
</ol>