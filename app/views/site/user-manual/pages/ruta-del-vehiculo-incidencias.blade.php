<h3 class="sub-legend">Incidencias</h3>
<ol class="iris-ol">
	<li>Cada una de las incidencias se mostrará en orden inverso al que sucedieron, lo nuevos primero. Tipo de la incidencia.</li>
	<li>Fecha y hora del suceso.</li>
	<li>Descripción introducida por el conductor del vehículo al suceder la incidencia.</li>
</ol>