<h3 class="sub-legend">Agregar registro</h3>
<ol class="iris-ol">
	<li>Para agregar una ruta, deberá de escoger una fecha.</li>
	<li>Y un vehículo, recuerde que la combinación de estos debe de ser única para evitar que un vehículo realice una ruta dos veces en un día.</li>
	<li>Selecione los clientes a visitar en esta ruta del panel de la izquierda y haga click en la flecha para pasarlos a la lista de clientes a visitar en esta ruta.</li>
    <li>Si desea registrar en ese momento a un cliente nuevo, haga click en el boton "Agergar Cliente +"</li>
	<li>Escoga los pasajeros que irán en el vehículo del panel de la izquierda y haga click en la flecha para pasarlos a la lista de pasajeros elegidos para esta ruta.</li>
    <li>Si desea agregar un nuevo registro de empleado haga click en el botón "Agregar Empleado +"</li>
	<li>Cuando estén todos sus datos completos haga click en el botón "Agregar" para proceder</li>
	<li>Si desea limpar todos los campos del formulario haga click en el botón "Reset"</li>
	<li>Si lo qu quiere es cancelar esta operación y volver a la lista de ruta haga click en el botón "Regresar"</li>
</ol>