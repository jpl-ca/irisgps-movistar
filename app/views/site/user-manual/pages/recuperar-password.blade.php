<h3 class="sub-legend">Recuperar contraseña</h3>
<ol class="iris-ol">
	<li>Campo de e-mail, escriba aquí la dirección válida de correo electrónico registrada con anterioridad en el sistema.</li>
	<li>Botón de recuperación, cuando haya escrito su e-mail, de click en este boton para continuar. Recibirá pronto un correo electrónico con un link para poder cambiar su contraseña</li>
</ol>