<h3 class="sub-legend">Todos los registros</h3>
<ol class="iris-ol">
    <li>Para agregar un nuevo registro, hacer click en el botón "Agregar Nuevo"</li>
    <li>El registro correspondiente al usuario con el cual ha iniciado sesión no puede ser eliminado por usted mismo.</li>
    <li>El ícono del "lápiz" sirve para editar el registro seleccionado.</li>
    <li>El ícono del "tacho" sirve para eliminar el registro seleccionado.</li>
</ol>