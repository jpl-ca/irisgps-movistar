<h3 class="sub-legend">Agregar registro</h3>
<ol class="iris-ol">
	<li>Se deberá de indicar un número de DNI único para el empleado.</li>
	<li>Sus nombres.</li>
	<li>Y apellidos.</li>
	<li>Número de teléfono celular.</li>
	<li>Y puesto que ocupa en la empresa.</li>
	<li>Cuando todo este listo hacer click en el botón "Agregar"</li>
    <li>Caso contrario click en el botón "Regresar" para volver a la lista de empleados.</li>
</ol>