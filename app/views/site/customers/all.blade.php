@extends('layouts.dashboard-base')

@section('content')
	<div class="row">
		<div class="col-lg-6">
		    <table class="table table-striped table-bordered table-hover" id="all-elements-datatable">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Nombre</th>
                        <th>Código</th>
                        <th>Telf.</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $customer)
                    <tr class="">
                        <td>{{$customer->id}}</td>
                        <td>{{$customer->name}}</td>
                        <td>{{$customer->customer_code}}</td>
                        <td>{{$customer->phone}}</td>
                        <td class="center">
                            <a class="iris-link" href="{{ route('getEditCustomer', $customer->id) }}" title="editar">
                                <i class="fa fa-pencil fa-fw"></i>
                            </a>     
                            <a class="iris-link confirm-action" href="{{ route('delete', array(get_class($customer),$customer->id)) }}" title="eliminar">
                                <i class="fa fa-trash fa-fw"></i>
                            </a>          
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
		</div>
        <div class="col-lg-6">
            <div id="map-canvas" style="height: 500px;"></div>            
        </div>
	</div>
@stop

@section('js')
    @include('javascript.datatable-init')

    <script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=drawing,places,geometry"></script>

    <script>
        var map;
        var markers = [];
        var center = new google.maps.LatLng(-12.0879227, -77.0159834);

        function initialize() {
            var styles = [
              {
                stylers: [
                  { hue: "#607D8B" },
                  { saturation: -20 }
                ]
              },{
                featureType: "road",
                elementType: "geometry",
                stylers: [
                  { lightness: 100 },
                  { visibility: "simplified" }
                ]
              },{
                featureType: "road",
                elementType: "labels",
                stylers: [
                  { visibility: "on" }
                ]
              }
            ];

            var mapOptions = {
                zoom: 10,
                center: center,
                styles: styles,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

            // Adds a Places search box. Searching for a place will center the map on that
            // location.

            getCustomerMarkers();
        }

        function createMarker(lat, lng, data) {

            var coordinate = new google.maps.LatLng(lat, lng);

            var marker = new google.maps.Marker({
                position: coordinate,
                map: map,
                animation: google.maps.Animation.DROP,
                title: data
            });

            var infowindow = new google.maps.InfoWindow({
                content: data
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });

            return marker;
        }

        // Sets the map on all markers in the array.
        function setAllMap(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        function getCustomerMarkers()
        {

            jsonData = {{ $json }};

            $(jsonData).each(function(index, obj){
                vid = obj.id;
                lat = obj.lat;
                lng = obj.lng;
                plate = obj.plate;

                data = '<h3>'+obj.name+'</h3><br><p>'+obj.address+'</p>';

                marker = createMarker(lat, lng, data);
                markers.push(marker);
            });
            setAllMap(map);
        }

        $(window).load(initialize);
    </script>



@stop