@extends('layouts.dashboard-base')

@section('content')
	<div class="row">
		<div class="col-lg-6">
			{{$templator->getForm()}}
		</div>
		<div class="col-lg-6">
			<legend>Señale la ubicación del cliente en el mapa</legend>
            <div id="map-canvas"></div>
		</div>
	</div>
@stop		

@section('js')
	{{$templator->getScripts()}}

    <script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=drawing,places,geometry"></script>

    <script>
        var map;
        var markers = [];
        var allMarkers = [];
        var center = new google.maps.LatLng(-12.0879227, -77.0159834);

        function initialize() {
            var styles = [
              {
                stylers: [
                  { hue: "#607D8B" },
                  { saturation: -20 }
                ]
              },{
                featureType: "road",
                elementType: "geometry",
                stylers: [
                  { lightness: 100 },
                  { visibility: "simplified" }
                ]
              },{
                featureType: "road",
                elementType: "labels",
                stylers: [
                  { visibility: "on" }
                ]
              }
            ];

            var mapOptions = {
                zoom: 10,
                center: center,
                styles: styles,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

      			// create a new marker
      			var marker = validateLoc(map);

      			//add listener to set the marker to the position on the map
      			google.maps.event.addListener(map, 'click', function(event) {
      				marker.setPosition(event.latLng);
      				marker.setMap(map);
      				marker.setAnimation(google.maps.Animation.DROP);

    			    $('#lat').val(event.latLng.lat());
    			    $('#lng').val(event.latLng.lng());
      			});
      			
        }

        $(window).load(initialize);

        function validateLoc(map)
        {
          var lat=$.trim($("#lat").val());
        	var lng=$.trim($("#lng").val());

    			if(lat.length>0 && lng.length>0)
    			{
            var coordinate = new google.maps.LatLng(lat, lng);

            var marker = new google.maps.Marker({
                position: coordinate,
                map: map,
                animation: google.maps.Animation.DROP
            });

    			}else{
            var marker = new google.maps.Marker({});
          }
          console.log(marker);
          return marker;
        }
    </script>
@stop