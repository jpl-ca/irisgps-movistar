@extends('layouts.dashboard-base')

@section('content')
    @include('site.tracking-routes.display-all')
@stop

@section('js')
    @include('javascript.datatable-init')
@stop