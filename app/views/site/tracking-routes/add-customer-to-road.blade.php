<p>Elija un cliente para agregarlo a la lista de tareas</p>
<form class="form-horizontal" action="{{ route('addVisitToCustomer', $tracking_route_id)  }}" method="POST" role="form">
    {{ Templator::rawSelectBasic($id = 'customer_id', $label = 'Cliente' , $name = 'customer_id', $customers , $haveEmptyOption = false, $selected = null, $disabled=false)  }}
    {{ Templator::rawText($id = 'description', $label = 'Descripción', $name = 'description', $placeholder = 'descripción', $required = true, $autocomplete=false, $helpblock = null, $value = null) }}
	<div class="form-group">
		<label class="col-md-4 control-label" for="submit"></label>
		<div class="col-md-4">
	    	<button type="submit" name="submit_button" class="btn btn-success">Agregar</button>
		</div>
	</div>
</form>