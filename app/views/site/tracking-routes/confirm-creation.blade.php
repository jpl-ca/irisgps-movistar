@extends('layouts.dashboard-base')

@section('content')
	<div class="row">
		<div class="col-lg-12">		
			{{$templator->getForm()}}
		</div>
	</div>
    {{$templator->getModals()}}
@stop

@section('js')
    {{$templator->getScripts()}}
    @include('javascript.datatable-init')
    <script>
        $(function () {

            $('#date').datetimepicker({
                locale: 'es',
                format: 'DD/MM/YYYY',
                /*minDate: "2015-06-12",*/
                sideBySide: true
            });
        });
    </script>
@stop