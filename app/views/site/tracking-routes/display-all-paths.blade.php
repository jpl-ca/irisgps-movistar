
<div class="row">
    <div class="col-lg-12">
        <table class="table table-striped table-bordered table-hover" id="all-elements-datatable">
            <thead>
            <tr>
                <th>Fecha</th>
                <th>Vehículo</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $vehicle_path)
                {{--*/
                    $plate = Vehicle::findOrFail($vehicle_path['vehicle_id'])->plate;
                    $date = Carbon::createFromFormat('d-m-Y', $vehicle_path['date'])->format('d/m/y');
                /*--}}
                <tr class="">
                    <td>{{ $date }}</td>
                    <td>{{$plate}}</td>
                    <td class="center">
                        <a class="iris-link" href="{{ route('getVehiclePathByDate', array($plate,$vehicle_path['date'])) }}" title="ver el recorrido del {{ $date }}">
                            <i class="fa fa-road fa-fw"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>