@extends('layouts.dashboard-base')

@section('content')
	<div class="row">
		<div class="col-lg-6">
      <input type="hidden" id='polygon' name='polygon' value={{ $geofence->points }}>
			{{$templator->getForm()}}
		</div>
		<div class="col-lg-6">
      <legend>Geo-cerca</legend>
      <div id="map-canvas" style="height:600px;"></div>
		</div>
	</div>
@stop		

@section('js')
	{{$templator->getScripts()}}

    <script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=drawing,places,geometry"></script>

    <script type="text/javascript">
      var map;
      var drawingManager;
      var shapes = [];
      var markers = [];
      var allMarkers = [];
      var center = new google.maps.LatLng(-12.0879227, -77.0159834);

      function initialize() {
        var styles = [
          {
            stylers: [
              { hue: "#607D8B" },
              { saturation: -20 }
            ]
          },{
            featureType: "road",
            elementType: "geometry",
            stylers: [
              { lightness: 100 },
              { visibility: "simplified" }
            ]
          },{
            featureType: "road",
            elementType: "labels",
            stylers: [
              { visibility: "on" }
            ]
          }
        ];

        var mapOptions = {
            zoom: 10,
            center: center,
            styles: styles,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        existsPreviusPolygon();

      }

      function existsPreviusPolygon()
      {
        var poly = $('#polygon').val();

        console.log(poly);

        if(poly != '')
        {
          drawPolygon(JSON.parse( poly ));
        }
      }

      function drawPolygon(points)
      {
        var polygono = new google.maps.Polygon({
          paths: points,
          strokeColor: "#00527A",
          strokeOpacity: 1,
          strokeWeight: 2,
          fillColor: "#00527A",
          fillOpacity: 0.5
        });

        polygono.setMap(map);

        bounds = new google.maps.LatLngBounds();

        for (i = 0; i < points.length; i++) {
          
          ptoo = new google.maps.LatLng((points[i]).lat, (points[i]).lng);
          bounds.extend(ptoo);
        }
        map.fitBounds(bounds);

        pCenterLat = bounds.getCenter().lat();
        pCenterLng = bounds.getCenter().lng();
        pCenter = bounds.getCenter();

        map.setCenter(pCenter);

      }

      $(window).load(initialize);

    </script>
@stop