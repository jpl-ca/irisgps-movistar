@extends('layouts.dashboard-base-fwc')

@section('content')
    <div class="row">
        <div class="hidden">
            <div id="modal_button" style="padding: 15px;">
                <a href="{{ route('getCreateGeoFence') }}" style="background: none">
                    <i class="fa fa-plus fa-fw icon-material-share btn btn-primary" style="font-size: 30px; padding-left: 14px; padding-top: 14px; height: 60px;">
                    <div class="share-ripple ripple-wrapper"></div>
                   </i>
                </a>  
            </div>
        </div>        

        <div class="full-width-div" id="map-canvas"></div>

    </div>
@stop

@section('js')
    <script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=drawing,places,geometry"></script>

    <script>
        var map;
        var markers = [];
        var allMarkers = [];
        var center = new google.maps.LatLng(-12.0879227, -77.0159834);

        function initialize() {
            var styles = [
              {
                stylers: [
                  { hue: "#607D8B" },
                  { saturation: -20 }
                ]
              },{
                featureType: "road",
                elementType: "geometry",
                stylers: [
                  { lightness: 100 },
                  { visibility: "simplified" }
                ]
              },{
                featureType: "road",
                elementType: "labels",
                stylers: [
                  { visibility: "on" }
                ]
              }
            ];

            var mapOptions = {
                zoom: 10,
                center: center,
                styles: styles,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('modal_button'));

            // Adds a Places search box. Searching for a place will center the map on that
            // location.

            existsPreviusPolygon();
        }



        function existsPreviusPolygon()
        {
            @forelse($data as $geofence)

                var poly = {{ $geofence->points }};

                if(poly != '')
                {
                    info = "<h5 style='text-decoration:underline;'>{{ $geofence->name }}</h5><p><a class='iris-link' href='{{ route('getEditGeoFence', $geofence->id) }}' title='editar'><i class='fa fa-pencil fa-fw'></i></a><a class='iris-link' href='{{ route('getAssignVehicles', $geofence->id) }}' title='asignar vehículos'><i class='fa fa-car fa-fw'></i></a><a class='iris-link confirm-action' href='{{ route('delete', array(get_class($geofence),$geofence->id)) }}' title='eliminar'><i class='fa fa-trash fa-fw'></i></a></p>";
                    drawPolygon( poly, info );
                }

            @empty
            @endforelse
        }

        function drawPolygon(points, info)
        {
            var polygono = new google.maps.Polygon({
              paths: points,
              strokeColor: "#00527A",
              strokeOpacity: 1,
              strokeWeight: 2,
              fillColor: "#00527A",
              fillOpacity: 0.5
            });


            var infowindow = new google.maps.InfoWindow({
                content: info
            });

            google.maps.event.addListener(polygono, 'click', function(event) {
                infowindow.setPosition(event.latLng);
                infowindow.open(map, polygono);
            });

            polygono.setMap(map);
        }

        $(window).load(initialize);
    </script>
@stop