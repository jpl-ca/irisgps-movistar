@extends('layouts.dashboard-base-fwc')

@section('content')
    <div class="row">
        <div class="hidden">
            <div id="modal_button" style="padding: 15px;">
              <div style="background-color:white; padding:10px;">
                {{$templator->getForm()}}                
              </div>
            </div>
        </div>
        <div class="full-width-div" id="map-canvas"></div>

    </div>
@stop 

@section('js')
	{{$templator->getScripts()}}

    <script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=drawing,places,geometry"></script>

    <script type="text/javascript">
      var map;
      var drawingManager;
      var shapes = [];
      var markers = [];
      var allMarkers = [];
      var center = new google.maps.LatLng(-12.0879227, -77.0159834);

      function initialize() {
        var styles = [
          {
            stylers: [
              { hue: "#607D8B" },
              { saturation: -20 }
            ]
          },{
            featureType: "road",
            elementType: "geometry",
            stylers: [
              { lightness: 100 },
              { visibility: "simplified" }
            ]
          },{
            featureType: "road",
            elementType: "labels",
            stylers: [
              { visibility: "on" }
            ]
          }
        ];

        var mapOptions = {
            zoom: 10,
            center: center,
            styles: styles,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        
        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('modal_button'));

        drawingManager = new google.maps.drawing.DrawingManager({
          drawingMode: null,
          drawingControl: true,
          drawingControlOptions: {
              position: google.maps.ControlPosition.TOP_CENTER,
              drawingModes: [google.maps.drawing.OverlayType.POLYGON]
          },
          polygonOptions: {
              editable: true,
              draggable: true,
              strokeColor: "#00527A",
              strokeOpacity: 1,
              strokeWeight: 2,
              fillColor: "#00527A",
              fillOpacity: 0.5
          }
        });
        
        drawingManager.setMap(map);

        // Add a listener for creating new shape event.
        google.maps.event.addListener(drawingManager, "overlaycomplete", function (event) {
          var newShape = event.overlay;
          newShape.type = event.type;
          shapes.push(newShape);
          if (drawingManager.getDrawingMode()) {
              drawingManager.setDrawingMode(null);
          }
        });

        // add a listener for the drawing mode change event, delete any existing polygons
        google.maps.event.addListener(drawingManager, "drawingmode_changed", function () {
          if (drawingManager.getDrawingMode() != null) {
              for (var i = 0; i < shapes.length; i++) {
                  shapes[i].setMap(null);
              }
              shapes = [];
              $('#polygon').val(null);
          }
        });

        // Add a listener for the "drag" event.
        google.maps.event.addListener(drawingManager, "overlaycomplete", function (event) {
          overlayDragListener(event.overlay);
          polygonPoints = event.overlay.getPath().getArray();
          $('#polygon').val(JSON.stringify(polygonPoints));
        });

        existsPreviusPolygon();

      }

      function overlayDragListener(overlay) {
        google.maps.event.addListener(overlay.getPath(), 'set_at', function (event) {
          polygonPoints = overlay.getPath().getArray();          
          $('#polygon').val(JSON.stringify(polygonPoints));
        });
        google.maps.event.addListener(overlay.getPath(), 'insert_at', function (event) {
          polygonPoints = overlay.getPath().getArray();          
          $('#polygon').val(JSON.stringify(polygonPoints));
        });
      }

      function existsPreviusPolygon()
      {
        var poly = $('#polygon').val();

        if(poly != '')
        {
          drawPolygon(JSON.parse( poly ));
        }
      }

      function drawPolygon(points)
      {
        var polygono = new google.maps.Polygon({
          paths: points,
          editable: true,
          draggable: true,
          strokeColor: "#00527A",
          strokeOpacity: 1,
          strokeWeight: 2,
          fillColor: "#00527A",
          fillOpacity: 0.5
        });

        shapes.push(polygono);
        
        google.maps.event.addListener(polygono.getPath(), 'insert_at', function(index, obj) {
          polygonPoints = polygono.getPath().getArray();          
          $('#polygon').val(JSON.stringify(polygonPoints));
        });

        google.maps.event.addListener(polygono.getPath(), 'set_at', function(index, obj) {
          polygonPoints = polygono.getPath().getArray();          
          $('#polygon').val(JSON.stringify(polygonPoints));
        });

        polygono.setMap(map);

        bounds = new google.maps.LatLngBounds();

        for (i = 0; i < points.length; i++) {
          
          ptoo = new google.maps.LatLng((points[i]).lat, (points[i]).lng);
          bounds.extend(ptoo);
        }
        map.fitBounds(bounds);

        pCenterLat = bounds.getCenter().lat();
        pCenterLng = bounds.getCenter().lng();
        pCenter = bounds.getCenter();

        map.setCenter(pCenter);

      }

      function validateGeoFence()
      {
        polygon = $("#polygon").val();

  			if(polygon.length > 0)
  			{
          console.log('es valido');

  			}else{
          console.log('No es válido');
        }
      }

      $(window).load(initialize);

    </script>
@stop