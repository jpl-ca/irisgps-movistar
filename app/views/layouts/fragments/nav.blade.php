		<!-- Navigation -->
        <nav class="navbar navbar-iris navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('getHome') }}"><img src="{{asset('assets/images/irisgps-logo-largo_inv.png')}}"  class="img text-center" alt="" style="height:40px"></a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-left">                
                <li >
                    <p class="nav-page-title">
                        @yield('nav-title')
                    </p>
                </li>
            </ul>

            <ul class="nav navbar-top-links navbar-right">
                
                <li {{ (Request::is('/')) ? 'class="activex"' : ''}} >
                    <a class="menu-link dropdown-toggle open" href="{{route('getHome')}}">
                        <i class="fa fa-home fa-fw"></i>
                    </a>
                </li>
                
                <li class="dropdown {{ (Request::is('vehiculos*')) ? 'activex' : ''}}">
                    <a class="menu-link dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-car fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><p>Vehículos</p>
                        <li><a href="{{ route('getVehiclesPositions') }}"><i class="fa fa-caret-right fa-fw"></i> Ver Localización</a>
                        </li>
                        <li class="divider"></li>
                        <li><p>Gestionar Vehículos</p>
                        <li><a href="{{ route('getSearchVehiclePath') }}"><i class="fa fa-caret-right fa-fw"></i> Buscar Recorridos</a>
                        </li>
                        <li><a href="{{ route('getAllVehicles') }}"><i class="fa fa-caret-right fa-fw"></i> Ver Todos</a>
                        </li>
                        <li><a href="{{ route('getCreateVehicle') }}"><i class="fa fa-caret-right fa-fw"></i> Agregar Nuevo</a>
                        </li>
                    </ul>
                </li>
                
                <li class="dropdown {{ (Request::is('empleados*')) ? 'activex' : ''}}">
                    <a class="menu-link dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-male fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><p>Gestionar Empleados</p>
                        <li><a href="{{ route('getAllEmployees') }}"><i class="fa fa-caret-right fa-fw"></i> Ver Todos</a>
                        </li>
                        <li><a href="{{ route('getCreateEmployee') }}"><i class="fa fa-caret-right fa-fw"></i> Agregar Nuevo</a>
                        </li>
                    </ul>
                </li>
                
                <li class="dropdown {{ (Request::is('dispositivos*')) ? 'activex' : ''}}">
                    <a class="menu-link dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-mobile fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><p>Gestionar Dispositivos</p>
                        <li><a href="{{ route('getAllDevices') }}"><i class="fa fa-caret-right fa-fw"></i> Ver Todos</a>
                        </li>
                        <li><a href="{{ route('getCreateDevice') }}"><i class="fa fa-caret-right fa-fw"></i> Agregar Nuevo</a>
                        </li>
                    </ul>
                </li>
                
                <li class="dropdown {{ (Request::is('rutas*')) ? 'activex' : ''}}">
                    <a class="menu-link dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-road fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><p>Gestionar Rutas</p>
                        <li><a href="{{ route('getSearchTrackingRoute') }}"><i class="fa fa-caret-right fa-fw"></i> Buscar Rutas</a>
                        </li>
                        <li><a href="{{ route('getAllTrackingRoutes') }}"><i class="fa fa-caret-right fa-fw"></i> Ver Todos</a>
                        </li>
                        <li><a href="{{ route('getCreateTrackingRoute') }}"><i class="fa fa-caret-right fa-fw"></i> Agregar Nuevo</a>
                        </li>
                    </ul>
                </li>
                
                <li class="dropdown {{ (Request::is('geo-cercas*')) ? 'activex' : ''}}">
                    <a class="menu-link dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bookmark fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><p>Gestionar Geo-Cercas</p>
                        <li><a href="{{ route('getAllGeoFences') }}"><i class="fa fa-caret-right fa-fw"></i> Ver Todos</a>
                        </li>
                        <li><a href="{{ route('getCreateGeoFence') }}"><i class="fa fa-caret-right fa-fw"></i> Agregar Nuevo</a>
                        </li>
                    </ul>
                </li>
                
                <li class="dropdown {{ (Request::is('clientes*')) ? 'activex' : ''}}">
                    <a class="menu-link dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-users fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><p>Gestionar Clientes</p>
                        <li><a href="{{ route('getAllCustomers') }}"><i class="fa fa-caret-right fa-fw"></i> Ver Todos</a>
                        </li>
                        <li><a href="{{ route('getCreateCustomer') }}"><i class="fa fa-caret-right fa-fw"></i> Agregar Nuevo</a>
                        </li>
                        <li><a href="{{ route('getImportDataCustomer') }}"><i class="fa fa-caret-right fa-fw"></i> Carga masiva</a>
                        </li>
                    </ul>
                </li>
                
                <li class="dropdown {{ (Request::is('usuarios*')) ? 'activex' : ''}}">
                    <a class="menu-link dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user-secret fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><p>Gestionar Usuarios</p>
                        <li><a href="{{ route('getAllUsers') }}"><i class="fa fa-caret-right fa-fw"></i> Ver Todos</a>
                        </li>
                        <li><a href="{{ route('getCreateUser') }}"><i class="fa fa-caret-right fa-fw"></i> Agregar Nuevo</a>
                        </li>
                    </ul>
                </li>
                
                <li class="dropdown {{ (Request::is('reportes*')) ? 'activex' : ''}}">
                    <a class="menu-link dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bar-chart fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><p>Reportes</p>
                        <li><a href="{{ route('getTasksPerWeek') }}"><i class="fa fa-caret-right fa-fw"></i> Tareas por Semana</a>
                        </li>
                        <li><a href="{{ route('getIncidentsPerWeek') }}"><i class="fa fa-caret-right fa-fw"></i> Incidentes por Semana</a>
                        </li>
                        <li><a href="{{ route('getVehicleSpeeds') }}"><i class="fa fa-caret-right fa-fw"></i> Excesos de Velocidad</a>
                        </li>
                        <li><a href="{{ route('getGeofencesAccess') }}"><i class="fa fa-caret-right fa-fw"></i> Salidas de Geo-Cercas</a>
                        </li>
                        <li><a href="{{ route('getGasConsumption') }}"><i class="fa fa-caret-right fa-fw"></i> Consumo de Combustible</a>
                        </li>
                    </ul>
                </li>
                
                <li class="dropdown {{ (Request::is('manual-de-usuario*')) ? 'activex' : ''}}">
                    <a class="menu-link dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-book fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><p>Manual de Usuarios</p></li>
                        @foreach ((new UserManualController)->pages as $key => $value)
                        <li>
                            <a href="{{ route("$key") }}"><i class="fa fa-caret-right fa-fw"></i> {{$value['params']['legend']}}</a>
                        </li>
                        @endforeach
                    </ul>
                </li>
                
                <li class="dropdown">
                    <a class="menu-link dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><p>{{Auth::user()->getFullname()}}</p>
                        <li><a href="{{ route('getEditProfile') }}"><i class="fa fa-user fa-fw"></i> Perfil</a>
                        </li>
                        @if( Auth::user()->isAdmin() )
                        <li><a href="{{ route('getConfigurations') }}"><i class="fa fa-gear fa-fw"></i> Configuración</a>
                        </li>
                        @endif
                        <li class="divider"></li>
                        <li><a href="{{route('getLogout')}}"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesión</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
        {{-- 
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        @if(Auth::user()->isUser())
                            <li {{ (Request::is('perfil')) ? 'class="active"' : ''}} >
                                <a href="{{route('getEditProfile')}}" class="{{ (Request::is('perfil')) ? 'active' : ''}}"><i class="fa fa-user fa-fw"></i> Perfil</a>
                            </li>
                        @else
                        <li {{ (Request::is('/')) ? 'class="active"' : ''}} >
                            <a href="{{route('getHome')}}" {{ (Request::is('/')) ? 'class="active"' : ''}} ><i class="fa fa-home fa-fw"></i> Home</a>
                        </li>
                        <li {{ (Request::is('vehiculos*')) ? 'class="active"' : ''}} >
                            <a href="#" {{ (Request::is('vehiculos*')) ? 'class="active"' : ''}} ><i class="fa fa-car fa-fw"></i> Vehículos<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('getVehiclesPositions') }}" {{ (Request::is('vehiculos/posiciones')) ? 'class="active"' : ''}} >Ver Localización</a>
                                </li>
                                <li {{ (Request::is('vehiculos/todo') || Request::is('vehiculos/buscar-recorrido') || Request::is('vehiculos/agregar')) ? 'class="active"' : ''}} >
                                    <a href="#" {{ (Request::is('vehiculos/todo') || Request::is('vehiculos/buscar-recorrido') || Request::is('vehiculos/agregar')) ? 'class="active"' : ''}} >Gestionar Vehículos <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{ route('getSearchVehiclePath') }}" {{ (Request::is('vehiculos/buscar-recorrido')) ? 'class="active"' : ''}} >Buscar Recorridos</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('getAllVehicles') }}" {{ (Request::is('vehiculos/todo')) ? 'class="active"' : ''}} >Ver Todos</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('getCreateVehicle') }}" {{ (Request::is('vehiculos/agregar')) ? 'class="active"' : ''}} >Agregar Nuevo</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('empleados*')) ? 'class="active"' : ''}} >
                            <a href="#" {{ (Request::is('empleados*')) ? 'class="active"' : ''}} ><i class="fa fa-user fa-fw"></i> Gestionar Empleados<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('getAllEmployees') }}" {{ (Request::is('empleados/todo')) ? 'class="active"' : ''}} >Ver Todos</a>
                                </li>
                                <li>
                                    <a href="{{ route('getCreateEmployee') }}" {{ (Request::is('empleados/agregar')) ? 'class="active"' : ''}} >Agregar Nuevo</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('dispositivos*')) ? 'class="active"' : ''}} >
                            <a href="#" {{ (Request::is('dispositivos*')) ? 'class="active"' : ''}} ><i class="fa fa-mobile fa-fw"></i> Gestionar Dispositivos<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('getAllDevices') }}" {{ (Request::is('dispositivos/todo')) ? 'class="active"' : ''}} >Ver Todos</a>
                                </li>
                                <li>
                                    <a href="{{ route('getCreateDevice') }}" {{ (Request::is('dispositivos/agregar')) ? 'class="active"' : ''}} >Agregar Nuevo</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('rutas*')) ? 'class="active"' : ''}} >
                            <a href="#" {{ (Request::is('rutas*')) ? 'class="active"' : ''}} ><i class="fa fa-road fa-fw"></i> Gestionar Rutas<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('getAllTrackingRoutes') }}" {{ (Request::is('rutas/todo')) ? 'class="active"' : ''}} >Ver Todos</a>
                                </li>
                                <li>
                                    <a href="{{route('getSearchTrackingRoute')}}" {{ (Request::is('rutas/buscar')) ? 'class="active"' : ''}} >Buscar Rutas</a>
                                </li>
                                <li>
                                    <a href="{{ route('getCreateTrackingRoute') }}" {{ (Request::is('rutas/agregar')) ? 'class="active"' : ''}} >Agregar Nuevo</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('geo-cercas*')) ? 'class="active"' : ''}} >
                            <a href="#" {{ (Request::is('geo-cercas*')) ? 'class="active"' : ''}} ><i class="fa fa-square-o fa-fw"></i> Gestionar Geo-Cercas<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('getAllGeoFences') }}" {{ (Request::is('geo-cercas/todo')) ? 'class="active"' : ''}} >Ver Todos</a>
                                </li>
                                <li>
                                    <a href="{{ route('getCreateGeoFence') }}" {{ (Request::is('geo-cercas/agregar')) ? 'class="active"' : ''}} >Agregar Nuevo</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('clientes*')) ? 'class="active"' : ''}} >
                            <a href="#" {{ (Request::is('clientes*')) ? 'class="active"' : ''}}><i class="fa fa-male fa-fw"></i> Gestionar Clientes<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('getAllCustomers') }}" {{ (Request::is('clientes/todo')) ? 'class="active"' : ''}}>Ver Todos</a>
                                </li>
                                <li>
                                    <a href="{{ route('getCreateCustomer') }}" {{ (Request::is('clientes/agregar')) ? 'class="active"' : ''}}>Agregar Nuevo</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('usuarios*')) ? 'class="active"' : ''}} >
                            <a href="#" {{ (Request::is('usuarios*')) ? 'class="active"' : ''}} ><i class="fa fa-road fa-fw"></i> Gestionar usuarios<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('getAllUsers') }}" {{ (Request::is('usuarios/todo')) ? 'class="active"' : ''}} >Ver Todos</a>
                                </li>
                                <li>
                                    <a href="{{ route('getCreateUser') }}" {{ (Request::is('usuarios/agregar')) ? 'class="active"' : ''}} >Agregar Nuevo</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('reportes*')) ? 'class="active"' : ''}} >
                            <a href="#" {{ (Request::is('reportes*')) ? 'class="active"' : ''}}><i class="fa fa-bar-chart fa-fw"></i> Reportes<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('getTasksPerWeek') }}" {{ (Request::is('reportes/tareas-por-semana')) ? 'class="active"' : ''}}>Tareas por Semana</a>
                                </li>
                                <li>
                                    <a href="{{ route('getIncidentsPerWeek') }}" {{ (Request::is('reportes/incidentes-por-semana')) ? 'class="active"' : ''}}>Incidentes por Semana</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('manual-de-usuario*')) ? 'class="active"' : ''}} >
                            <a href="#" {{ (Request::is('manual-de-usuario*')) ? 'class="active"' : ''}}><i class="fa fa-book fa-fw"></i> Manual de usuario<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                @foreach ((new UserManualController)->pages as $key => $value)
                                <li>
                                    <a href="{{ route("$key") }}" {{ (Request::is($value['uri'])) ? 'class="active"' : ''}}>{{$value['params']['legend']}}</a>
                                </li>
                                @endforeach
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        @endif
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
             --}}
        </nav>