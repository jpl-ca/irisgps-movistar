@extends('layouts.base')

@section('body')

    <div id="wrapper">

        @include('layouts.fragments.nav')        

        <!-- Page Content -->
        <div id="page-wrapper2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <!--ALERT ZONE -->
                        @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{Session::get('success')}}
                        </div>
                        @endif
                        @if(Session::has('info'))
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{Session::get('info')}}
                        </div>
                        @endif
                        @if(Session::has('warning'))
                        <div class="alert alert-warning alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{Session::get('warning')}}
                        </div>
                        @endif
                        @if(Session::has('error'))
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{Session::get('error')}}
                        </div>
                        @endif
                        @if(Session::has('validation_errors'))
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Se han encontrado los siguientes errores:
                            <ul>
                                @foreach (Session::get('validation_errors') as $validation_error)
                                    @foreach ($validation_error as $error_message)
                                        <li>{{$error_message}}</li>
                                    @endforeach
                                @endforeach
                            </ul>
                        </div>
                        @endif                      
                        <!--END ALERT ZONE -->
                        <h1 class="page-header">{{ isset($pageTitle) ? $pageTitle : 'Título de la página' }}
                            @if(isset($actionButton))
                                <a href="{{ route($actionButton['route']) }}" class="btn btn-{{ $actionButton['class'] }} pull-right">{{ $actionButton['title'] }}</a>
                            @endif
                        </h1>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12">
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
@stop