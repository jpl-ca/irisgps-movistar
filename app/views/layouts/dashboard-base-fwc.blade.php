@extends('layouts.base')

@section('nav-title')
    {{ $pageTitle }}
@stop

@section('body')

    <div id="wrapper">
        @include('layouts.fragments.nav')

        @yield('content')

    </div>
    <!-- /#wrapper -->
@stop