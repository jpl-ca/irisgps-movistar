
<!DOCTYPE html>
<html lang="es">

	<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


	<style>
	table, th, td {
	    border: 1px solid black;
	    border-collapse: collapse;
	}
	th, td {
	    padding: 5px;
	    text-align: left;
	}
	</style>
		
	</head>
	<body>
	<h1>FCKNG-AWESOME-GEOPOSITION-SPEED-DATA... yeah you can't read this title without breathing</h1>
		<table style="width:100%">
			<tr>
				<th>LAT</th>
				<th>LNG</th>
				<th>SPEED DEVICE</th>
				<th>SPEED CUSTOM</th>
				<th>TIME</th>
			</tr>
			@if(empty($stored))
				<p>no data</p>
			@else
				@foreach($stored as $loc)
					<tr>
						<td>{{ $loc->lat }}</td>
						<td>{{ $loc->lng }}</td>
						<td>{{ $loc->speed_device }}</td>
						<td>{{ $loc->speed_custom }}</td>
						<td>{{ $loc->created_at }}</td>
					</tr>
				@endforeach
			@endif
		</table>
		
	</body>
</html>