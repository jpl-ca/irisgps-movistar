<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<h4>El sistema ha detectado las siguientes acciones</h4>
	
	@if(count($vehicles_in) > 0)
		<h5>Entradas a Geo-cercas</h5>
		<table>
			<tr>
				<th>Placa</th>
				<th>Geo-cerca</th>
				<th>Fecha</th>
			</tr>
			@foreach($vehicles_in as $vehicle)
				<tr>
					<td>{{ $vehicle['plate'] }}</td>
					<td>{{ $vehicle['geofence'] }}</td>
					<td>{{ $vehicle['date'] }}</td>
				</tr>	
			@endforeach
		</table>
	@endif
	
	@if(count($vehicles_out) > 0)
		<h5>Salidas a Geo-cercas</h5>
		<table>
			<tr>
				<th>Placa</th>
				<th>Geo-cerca</th>
				<th>Fecha</th>
			</tr>
			@foreach($vehicles_out as $vehicle)
				<tr>
					<td>{{ $vehicle['plate'] }}</td>
					<td>{{ $vehicle['geofence'] }}</td>
					<td>{{ $vehicle['date'] }}</td>
				</tr>	
			@endforeach
		</table>
	@endif
	
</body>