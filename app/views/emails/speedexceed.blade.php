<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<h4>El sistema ha detectado los siguientes excesos de velocidad</h4>
	
	@if(count($speed_alerts) > 0)
		<h5>Excesos de velocidad</h5>
		<table>
			<tr>
				<th>Placa</th>
				<th>Distancia</th>
				<th>Duración</th>
				<th>Velocidad</th>
				<th>Fecha</th>
			</tr>
			@foreach($speed_alerts as $sa)
				<tr>
					<td>{{ $sa['plate'] }}</td>
					<td>{{ round($sa['speed_exceeds'][0]['distance'],4) }} km</td>
					<td>{{ gmdate("H:i:s", ceil($sa['speed_exceeds'][0]['time'])*60 ) }}</td>
					<td>{{ $sa['speed_exceeds'][0]['speed'] }} km/h</td>
					<td>{{ $sa['speed_exceeds'][0]['until'] }}</td>
				</tr>	
			@endforeach
		</table>
	@endif
	
</body>