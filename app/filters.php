<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
    if($request->path() !== "api/store-locations")
    {
        
        if(!is_null($request->header('Device-Type')))
        {
            $responsor = new Responsor;

            if($request->header('Device-Type') == 'ws')
            {
                if (!is_null(Request::header('IMEI')) && !is_null(Request::header('gcm')))
                {
                    $responsor->isNoAuth();

                    $device = Device::findByMobile(Request::header('IMEI'));

                    if(is_null($device))
                    {
                        $responsor->message = 'el dispositivo no está registrado';
                        return $responsor->response();
                    }

                    if($device->isActivated())
                    {
                        if(!$device->isAllowed())
                        {
                            $responsor->message = 'el dispositivo no está permitido';
                            return $responsor->response();
                        }

                        Device::registerGCM(Request::header('IMEI'));

                        //->ejecuta la acción correspondiente

                    }else{
                        $responsor->message = 'el dispositivo no está activado';
                        return $responsor->response();
                    }
                }
                else
                {
                    $responsor->isInvalid();
                    $responsor->message = 'el IMEI del teléfono o id gcm no han sido encontrado en la solicitud';
                    return $responsor->response();
                }
            }
            //->ES MO

            // if($request->header('Device-Type') == 'mo')
            // {
            //     if (!is_null(Request::header('IMEI')) && !is_null(Request::header('gcm')))
            //     {
            //         $responsor->isNoAuth();

            //         $device = Device::findByMobile(Request::header('IMEI'));

            //         if(is_null($device))
            //         {
            //             $responsor->message = 'el dispositivo no está registrado';
            //             return $responsor->response();
            //         }

            //         if($device->isActivated())
            //         {
            //             if(!$device->isAllowed())
            //             {
            //                 $responsor->message = 'el dispositivo no está permitido';
            //                 return $responsor->response();
            //             }

            //             Device::registerGCM(Request::header('IMEI'));

            //             //->ejecuta la acción correspondiente

            //         }else{
            //             $responsor->message = 'el dispositivo no está activado';
            //             return $responsor->response();
            //         }
            //     }
            //     else
            //     {
            //         $responsor->isInvalid();
            //         $responsor->message = 'el IMEI del teléfono o id gcm no han sido encontrado en la solicitud';
            //         return $responsor->response();
            //     }
            // }
        }

    }
    //->ES WEB
});


App::after(function($request, $response)
{
    $token = null;

    $device = Device::findByMobile(Request::header('IMEI'));

    if(JMUserAgent::isWSRequest() && !is_null($device))
    {
        if(!is_null($request->header('token')) && !($response->getData()->empty_token))
        {
            $token = DeviceService::getDeviceByIMEI()->generateToken();
        }
    }

    $response->headers->set('token', $token);
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth.basic', function()
{
    return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('auth', function()
{
    if (Auth::guest())
    {
        return Redirect::route('getLogin');
    }
});

Route::filter('admin', function()
{
    if (Auth::user()->isUser())
    {
        return Redirect::route('getEditProfile');
    }
});

Route::filter('guest', function()
{
    if (Auth::check()) return Redirect::route('getHome');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
    if (Session::token() != Input::get('_token'))
    {
        throw new Illuminate\Session\TokenMismatchException;
    }
});

include 'filters/app_filters.php';