<?php

class TrackingRoutesController extends \BaseController {

	public function getAllTrackingRoutes()
	{
		$response = TrackingRouteService::allTrackingRoutes();
		$data = $response['data'];

		$actionButton = array(
			'route'=>'getCreateTrackingRoute',
			'title'=>'Agregar Nuevo',
			'class'=>'info'
		);

		return View::make('site.tracking-routes.all')->with(array('pageTitle'=>'Rutas', 'actionButton'=>$actionButton, 'data' => $data));
	}

	public function getAllVehiclePaths()
	{
		$response = TrackingRouteService::allTrackingRoutes();
		$data = $response['data'];

		$actionButton = array(
			'route'=>'getCreateTrackingRoute',
			'title'=>'Agregar Nuevo',
			'class'=>'info'
		);

		return View::make('site.tracking-routes.all')->with(array('pageTitle'=>'Rutas', 'actionButton'=>$actionButton, 'data' => $data));
	}

	public function getCreateTrackingRoute()
	{

		$actionButton = array(
			'route'=>'getAllVehicles',
			'title'=>'Regresar',
			'class'=>'default'
		);

		$templator = new Templator;

		$templator->createForm($actionRoute = 'postCreateTrackingRoute', $routeParams = null, $method = 'POST', $legend = 'Crear nueva ruta', $submitName = 'Agregar', $resetName = 'Reset');
		$templator->addText($id = 'date', $label = 'Fecha', $name = 'date', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
		$templator->addSelectBasic($id = 'vehicle_id', $label = 'Vehículo', $name = 'vehicle_id', $elements = Vehicle::all()->lists('plate', 'id'), $haveEmptyOption = false);
		$templator->addDualListBox($id = 'customers', $label = 'Clientes', $name = 'customers', $elements = Customer::all()->lists('name', 'id'), $rows = 10, $fromName = 'Todos los clientes', $toName='Elegidos para visitar', $size = 7);
		$templator->addLinkButton('postCreateVehicle', $params = null, $class = 'info', $name = 'Agregar Cliente <i class="fa fa-plus"></i>');
		$templator->addDualListBox($id = 'passengers', $label = 'Pasajeros', $name = 'passengers', $elements = Employee::all()->lists('first_name', 'id'), $rows = 10, $fromName = 'Empleados', $toName='Elegidos para esta ruta', $size = 7);
		$templator->addLinkButton('postCreateVehicle', $params = null, $class = 'info', $name = 'Agregar Empleado <i class="fa fa-plus"></i>');

		return View::make('site.tracking-routes.create')->with(array('pageTitle'=>'Rutas', 'actionButton'=>$actionButton, 'templator' => $templator));
	}	

	public function postCreateTrackingRoute()
	{
		$date = Input::get('date');
		$vehicle_id = Input::get('vehicle_id');
		$customers = Input::get('customers');
		$passengers = Input::get('passengers');

		$customers = array_unique(is_null($customers) ? array() : $customers);
		$passengers = array_unique(is_null($passengers) ? array() : $passengers);

		$date_input = array(
			'date' => $date,
		);

		$validator = Validator::make(
		    $date_input,
		    TrackingRoute::$date_rules
		);

		if ($validator->fails())
		{
			$messages = $validator->errors()->toArray();
			//return Response::invalid(false, false, ": los datos tienen errores", $messages);
			return Redirect::back()->withValidationErrors($messages)->withInput();
		}

		$date = Carbon::createFromFormat('d/m/Y', $date)->startOfDay()->toDateTimeString();

		$input = array(
			'date' => $date,
			'vehicle_id' => $vehicle_id,
			'customers' => $customers,
			'passengers' => $passengers
		);

		$validator = Validator::make(
		    $input,
		    TrackingRoute::$rules
		);

		if ($validator->fails())
		{
			$messages = $validator->errors()->toArray();
			//return Response::invalid(false, false, ": los datos tienen errores", $messages);
			return Redirect::back()->withValidationErrors($messages)->withInput();
		}

		$tracking_route = new TrackingRoute;
		$tracking_route->date = $date;
		$tracking_route->vehicle_id = $vehicle_id;
		$tracking_route->save();

		foreach ($customers as $customer_id) {
			$route_task = new RouteTask;
			$route_task->customer_id = $customer_id;
			$route_task->task_state_id	= 1;
			$route_task->tracking_route_id = $tracking_route->id;
			$route_task->description = 'Sin descripción de tarea';
			$route_task->save();

			$task_state_history = new TaskStateHistory;
			$task_state_history->route_task_id = $route_task->id;
			$task_state_history->task_state_id = $route_task->task_state_id;
			$task_state_history->description = 'Programado por el Sistema';
			$task_state_history->save();
		}

		$employee_counter = 0;

		foreach ($passengers as $employee_id) {
			$passenger = new Passenger;
			$passenger->employee_id = $employee_id;
			$passenger->tracking_route_id = $tracking_route->id;
			$passenger->passenger_type_id = ($employee_counter == 0) ? 1 : 2;
			$passenger->save();
			$employee_counter++;
		}

		return Redirect::route('getConfirmCreation', array($tracking_route->id))->withSuccess('Ruta creada correctamente.');
	}

	public static function getConfirmCreation($tracking_route_id)
	{
		$tracking_route = TrackingRoute::find($tracking_route_id);

        $tasks = $tracking_route->tasks()->get();

        $delete_passenger_url = URL::to('rutas/borrar-pasajero');

        $delete_customer_url = URL::to('eliminar/RouteTask');

		$elements = $tracking_route->employees()
				->select(DB::raw("concat(first_name, ' ', last_name, '  <a style=\'color:red\'class=\'confirm-action\' href=\'$delete_passenger_url', '/', passengers.id, '\'><i class=\'fa fa-trash fa-fw\'></i></a>') as fullname, passengers.id"))
				->lists("fullname", 'id');

		$employees = Employee::select(DB::raw('concat(first_name, " ", last_name) as fullname, id'))
				->lists('fullname', 'id');

		$driver_id = $tracking_route->getDriverId();

		$templator = new Templator;

		$templator->createForm($actionRoute = 'postConfirmCreation', $routeParams = [$tracking_route->id], $method = 'POST', $legend = 'Confirmar información de ruta', $submitName = 'Guardar Cambios', $resetName = null);

		$templator->addElement([
            'type' => 'text',
            'id' => 'date',
            'label' => 'Fecha',
            'name' => 'date',
            'placeholder' => 'dd/mm/YYYY',
            'required' => true,
            'autocomplete' => false,
            'value' => $tracking_route->getDate('d/m/Y')
        ]);

        $templator->addElement([
            'type' => 'select-basic',
            'id' => 'vehicle_id',
            'label' => 'Vehículo',
            'name' => 'vehicle_id',
            'elements' => Vehicle::all()->lists('plate', 'id'),
            'required' => true,
            'selected' => $tracking_route->vehicle_id
        ]);

		$templator->addRadioMultiple($id = 'driver', $name = 'driver', $label = 'Conductor', $elements, $checked = $driver_id);

        $modal_body = View::make('site.passengers.simple-create')->with(array('tracking_route_id' => $tracking_route->id, 'employees'=> $employees))->render();

        $templator->addElement([
            'type' => 'modal-button',
            'class' => 'info',
            'name' => "<i class='fa fa-plus fa-fw'></i> Agregar pasajero",
            'modal_content' => [
                    'modal_title' => "Agregar Pasajero",
                    'modal_body' => $modal_body
                ]
        ]);
        $templator->addSubLegend('Tareas a realizar por cliente');

        foreach($tasks as $task) {
        	$delete_task_link = "<a style='color:red' class='confirm-action' href='$delete_customer_url/".$task->id."'><i class='fa fa-trash fa-fw'></i></a>";
            $templator->addElement([
                'type'=>'text',
                'label'=> $delete_task_link.' para '.$task->customer->name.' se realizará lo siguiente:',
                'id'=>'task_description_'.$task->id,
                'name'=>'task_description_'.$task->id,
                'placeholder' => 'descripción',
                'required'=>true,
                'value'=>$task->description
            ]);
        }

        $additional_c_class = '';
        /*
        if(count($tasks) == 7) {
        	$additional_c_class = 'disabled';
        }
        */

        $customers_list = Customer::all()->lists('name', 'id');

        $modal_body = View::make('site.tracking-routes.add-customer-to-road')->with(array('tracking_route_id' => $tracking_route->id, 'customers'=> $customers_list))->render();

        $templator->addElement([
            'type' => 'modal-button',
            'class' => "info $additional_c_class",
            'name' => "<i class='fa fa-plus fa-fw'></i> Agregar visita a cliente",
            'modal_content' => [
                    'modal_title' => 'Agregar visita a cliente',
                    'modal_body' => $modal_body
                ]
        ]);
        $templator->addSubLegend('');

		return View::make('site.tracking-routes.confirm-creation')->with(array('pageTitle'=>'Rutas', 'actionButton'=>null, 'templator' => $templator));
	}

	public static function postConfirmCreation($tracking_route_id)
	{
		$tracking_route = TrackingRoute::findOrFail($tracking_route_id);

		//return dd($tracking_route->getConfirmRule());

		$date = Input::get('date');
		$vehicle_id = Input::get('vehicle_id');

		$date_input = array(
			'date' => $date,
		);

		$validator = Validator::make(
		    $date_input,
		    TrackingRoute::$date_rules
		);

		if ($validator->fails())
		{
			$messages = $validator->errors()->toArray();
			//return Response::invalid(false, false, ": los datos tienen errores", $messages);
			return Redirect::back()->withValidationErrors($messages)->withInput();
		}

		$date = Carbon::createFromFormat('d/m/Y', $date)->startOfDay()->toDateTimeString();

		$input = array(
			'date' => $date,
			'vehicle_id' => $vehicle_id
		);

		$validator2 = Validator::make(
		    $input,
		    $tracking_route->getConfirmRule()
		);

		if ($validator2->fails())
		{
			$messages = $validator2->errors()->toArray();
			//return Response::invalid(false, false, ": los datos tienen errores", $messages);
			return Redirect::back()->withValidationErrors($messages)->withInput();
		}

		$tracking_route->date = $date;
		$tracking_route->vehicle_id = $vehicle_id;
		$tracking_route->save();




        $passengers =$tracking_route->passengers()->get()->lists('id');
        $driver = Input::get('driver');
        $driver_arr = [$driver];

        $toUpdate = array_diff($passengers,$driver_arr);

        Passenger::whereIn('id', $toUpdate)->update(array('passenger_type_id' => 2));
        Passenger::where('id', $driver)->update(array('passenger_type_id' => 1));

        $tasks = $tracking_route->tasks;

        foreach($tasks as $task) {
            $t = RouteTask::findOrFail($task->id);
            $t->description = Input::get('task_description_'.$task->id);
            $t->save();
        }
        return Redirect::back()->withSuccess('Todos los datos se actualizaron correctamente');
	}

	public static function getCurrentRouteInfo($plate = null)
	{
		if(!$plate)
		{
			$token = Request::header('token');

			$plate = DeviceService::getPairedVehiclePlateByToken($token);
		}

		return TrackingRouteService::currentRouteInfo($plate);
	}

	public static function getRouteInfoByPlateAndDate($plate, $date = null)
	{
		if(is_null($date))
		{
			$response = TrackingRouteService::currentRouteInfo($plate);
		}else
		{
			$response = TrackingRouteService::getRouteInfoByPlateAndDate($plate, $date);
		}

		return $response;
	}

	public static function postCreateRouteComment()
	{
		$tracking_route_id = Input::get('tracking_route_id');
		$comment = Input::get('comment');

		$response = TrackingRouteService::registerComment($tracking_route_id, $comment);

		if(!JMUserAgent::isAndroidRequest()){
			if($response['status'] == 'error')
			{
				if($response['code'] == 'irs402')
				{
					return Redirect::back()->withValidationErrors($response['data'])->withInput();
				}else{
					return Redirect::back()->withValidationError('Hubo un error al procesar la acción.')->withInput();					
				}
			}else{
				/*
				if(!is_null($route))
				{
					return Redirect::route($route)->withSuccess('Comentario creado satisfactoriamente');
				}*/
				return Redirect::back()->withSuccess('Comentario creado satisfactoriamente');
			}
		}

		return $response;
	}	

	public static function postChangeRouteTaskState()
	{
		$route_task_id = Input::get('route_task_id');
		$description = Input::get('description');
		$task_state_id = Input::get('task_state_id');

		$response = TrackingRouteService::changeRouteTaskState($route_task_id, $task_state_id, $description);

		return $response;
	}

    public static function getTrackingRouteById($tracking_route_id)
    {
    	$snapped = Input::has('snapped');
    	$both = Input::has('both');
        $response = TrackingRouteService::getTrackingRouteById($tracking_route_id);
        $data = $response['data'];
        return View::make('site.vehicles.route')->with(array('pageTitle'=>'Ruta del Vehículo', 'data' => $data, 'snapped' => $snapped, 'both' => $both));
    }

    public function getSearchTrackingRoute()
    {
        $data = null;

        if(Input::has('vehicle_id'))
        {
            $vehicle_id = Input::get('vehicle_id');
            $from = Carbon::createFromFormat('d/m/Y', Input::get('from'))->startOfDay()->toDateTimeString();
            $until = Carbon::createFromFormat('d/m/Y', Input::get('until'))->endOfDay()->toDateTimeString();

            $data = TrackingRoute::where('date', '>=', $from)
                ->where('date', '<=', $until);

            if($vehicle_id != 0)
            {
                $data = $data->where('vehicle_id', $vehicle_id);
            }
            $data = $data->orderBy('date','DESC')->get();
        }

        $templator = new Templator;
        $templator->isFormInline();
        $templator->createForm($actionRoute = 'getSearchTrackingRoute', $routeParams = null, $method = 'GET', $legend = 'Buscar', $submitName = 'Buscar', $resetName = null);
        $templator->addText($id = 'from', $label = 'Desde', $name = 'from', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
        $templator->addText($id = 'until', $label = 'Hasta', $name = 'until', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
        $templator->addSelectBasic($id = 'vehicle_id', $label = 'Vehículo', $name = 'vehicle_id', $elements = array('0'=>'Cualquiera')+Vehicle::all()->lists('plate', 'id'), $haveEmptyOption = false);
        //$templator->addLinkButton('postCreateVehicle', $params = null, $class = 'info', $name = 'Agregar Cliente <i class="fa fa-plus"></i>');

        return View::make('site.tracking-routes.search')->with(array('pageTitle'=>'Rutas', 'actionButton'=> null /*$actionButton*/, 'templator' => $templator, 'data' => $data));
    }

    public function getSearchVehiclePath()
    {
        $data = null;

        if(Input::has('vehicle_id'))
        {

        	$from = Carbon::createFromFormat('d/m/Y', Input::get('from'))->startOfDay()->toDateTimeString();
        	$until = Carbon::createFromFormat('d/m/Y', Input::get('until'))->endOfDay()->toDateTimeString();

        	$vehicle_id = Input::get('vehicle_id');

        	if($vehicle_id == 0) {       		

		        $data = LocationHistory::select(
		        		DB::raw("distinct(DATE_FORMAT(created_at,'%d-%m-%Y')) as date, vehicle_id")
		        	)
					->where('created_at', '>=', $from)
					->where('created_at', '<=', $until)
					->orderBy('date', 'DESC')
					->get()->toArray();

        	}else {

		        $data = LocationHistory::select(
		        		DB::raw("distinct(DATE_FORMAT(created_at,'%d-%m-%Y')) as date, vehicle_id")
		        	)
					->where('vehicle_id', $vehicle_id)				
					->where('created_at', '>=', $from)
					->where('created_at', '<=', $until)
					->orderBy('date', 'DESC')
					->get()->toArray();

        	}
        }

        $templator = new Templator;
        $templator->isFormInline();
        $templator->createForm($actionRoute = 'getSearchVehiclePath', $routeParams = null, $method = 'GET', $legend = 'Buscar Recorridos', $submitName = 'Buscar', $resetName = null);
        $templator->addText($id = 'from', $label = 'Desde', $name = 'from', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
        $templator->addText($id = 'until', $label = 'Hasta', $name = 'until', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
        $templator->addSelectBasic($id = 'vehicle_id', $label = 'Vehículo', $name = 'vehicle_id', $elements = array('0'=>'Cualquiera')+Vehicle::all()->lists('plate', 'id'), $haveEmptyOption = false);
        //$templator->addLinkButton('postCreateVehicle', $params = null, $class = 'info', $name = 'Agregar Cliente <i class="fa fa-plus"></i>');

        return View::make('site.tracking-routes.search-path')->with(array('pageTitle'=>'Vehículos', 'actionButton'=> null /*$actionButton*/, 'templator' => $templator, 'data' => $data));
    }

	public function addPassenger($tracking_route_id)
	{
        $employee_id = Input::get('employee_id');

        $preview = Passenger::where('tracking_route_id', $tracking_route_id)
            ->where('employee_id', $employee_id)->first();

        if(!is_null($preview))
        {
            return Redirect::back()->withError('El empleado ya fue registrado como pasajero');
        }
		$passenger = new Passenger();

        $passenger->tracking_route_id = $tracking_route_id;
        $passenger->passenger_type_id = 2;
        $passenger->employee_id = $employee_id;

        $passenger->save();

        return Redirect::back()->withSuccess('Pasajero agregado correctamente');
	}

	public function addVisitToCustomer($tracking_route_id)
	{
        $customer_id = Input::get('customer_id');
        $description = Input::get('description');

        $preview = RouteTask::where('tracking_route_id', $tracking_route_id)
            ->where('customer_id', $customer_id)->first();

        if(!is_null($preview))
        {
            return Redirect::back()->withError('El cliente ya fue registrado.');
        }

		$route_task = new RouteTask();

        $route_task->tracking_route_id = $tracking_route_id;
        $route_task->description = $description;
        $route_task->customer_id = $customer_id;
        $route_task->task_state_id = 1;

        $route_task->save();

        return Redirect::back()->withSuccess('Cliente agregado correctamente');
	}

	public function deletePassenger($passenger_id) {

		$passenger = Passenger::find($passenger_id);

		$passengers = TrackingRoute::find($passenger->tracking_route_id)
			->passengers()
				->where('id', '<>', $passenger->id)
				->get()->lists('id');

        if($passenger->passenger_type_id == 1)
        {
        	if(count($passengers) >= 1) {

        		$new_driver = $passengers[0];
	        	$driver_arr = [$new_driver];
		        Passenger::where('id', $new_driver)->update(array('passenger_type_id' => 1));

		        if(count($passengers) >= 2) {

		        	$toUpdate = array_diff($passengers,$driver_arr);

		        	Passenger::whereIn('id', $toUpdate)->update(array('passenger_type_id' => 2));
		    	}
        		
        	}
        }

        $passenger->delete();

        return Redirect::back()->withSuccess('Pasajero eliminado correctamente');
	}

	/*

	public function create()
	{
		return View::make('trackingroutes.create');
	}

	public function store()
	{
		$validator = Validator::make($data = Input::all(), Trackingroute::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Trackingroute::create($data);

		return Redirect::route('trackingroutes.index');
	}

	public function show($id)
	{
		$trackingroute = Trackingroute::findOrFail($id);

		return View::make('trackingroutes.show', compact('trackingroute'));
	}

	public function edit($id)
	{
		$trackingroute = Trackingroute::find($id);

		return View::make('trackingroutes.edit', compact('trackingroute'));
	}

	public function update($id)
	{
		$trackingroute = Trackingroute::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Trackingroute::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$trackingroute->update($data);

		return Redirect::route('trackingroutes.index');
	}

	public function destroy($id)
	{
		Trackingroute::destroy($id);

		return Redirect::route('trackingroutes.index');
	}
	*/

}
