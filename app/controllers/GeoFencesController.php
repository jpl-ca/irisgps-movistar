<?php

class GeoFencesController extends \BaseController {

	public function getAllGeoFences()
	{
		$data = Geofence::all();

		$actionButton = array(
			'route'=>'getCreateGeoFence',
			'title'=>'Agregar Nuevo',
			'class'=>'info'
		);

		return View::make('site.geofences.all')->with(array('pageTitle'=>'Geo-Cercas', 'actionButton'=>$actionButton, 'data' => $data));
	}

	public function getCreateGeoFence()
	{
		$actionButton = array(
			'route'=>'getAllGeoFences',
			'title'=>'Regresar',
			'class'=>'default'
		);

		$templator = new Templator;

		$templator->setSize(7);
		$templator->createForm($actionRoute = 'postCreateGeoFence', $routeParams = null, $method = 'POST', $legend = 'Registrar nueva geo-cerca', $submitName = 'Agregar', $resetName = null);
		$templator->addText($id = 'name', $label = 'Nombre*', $name = 'name', $placeholder = null, $required = true, $autocomplete=false, $helpblock = null);
		$templator->addHidden($id = 'polygon', $name = 'polygon', $value = null);

		return View::make('site.geofences.create')->with(array('pageTitle'=>'Geo-Cercas <small>crear nuevo</small>', 'actionButton'=>$actionButton, 'templator' => $templator));
	}

	public function postCreateGeoFence()
	{
		$input = Input::only('name', 'polygon');

		if( empty( trim( $input['polygon'] ) ) )
		{
			return Redirect::back()->withError('Debe de seleccionar un área.')->withInput();				
		}

		$validator = Validator::make(
		    $input, Geofence::$rules
		);

		if ($validator->fails())
		{
			$messages = $validator->errors()->toArray();
			return Redirect::back()->withValidationErrors($messages)->withInput();
		}

		$polygonName = $input['name'];
		$polygonPoints = $input['polygon'];

		$polygonPoints = str_replace('K', 'lng', str_replace('G', 'lat', $polygonPoints));
		$polygonPoints = str_replace('L', 'lng', str_replace('H', 'lat', $polygonPoints));

		$geofence = new Geofence;

		$geofence->name = $polygonName;
		$geofence->points = $polygonPoints;
		$geofence->save();


		return Redirect::route('getAllGeoFences')->withSuccess('Geo-Cerca registrada satisfactoriamente');
	}

	public function getEditGeoFence($id)
	{
		$geofence = Geofence::findOrFail($id);

		$actionButton = array(
			'route'=>'getAllGeoFences',
			'title'=>'Regresar',
			'class'=>'default'
		);

		$templator = new Templator;

		$templator->setSize(7);
		$templator->createForm($actionRoute = 'postEditGeoFence', $routeParams = $id, $method = 'POST', $legend = 'Registrar nueva geo-cerca', $submitName = 'Guardar', $resetName = null);
		$templator->addText($id = 'name', $label = 'Nombre*', $name = 'name', $placeholder = null, $required = true, $autocomplete=false, $helpblock = null, $value = $geofence->name);
		$templator->addHidden($id = 'polygon', $name = 'polygon', $value = $geofence->points);

		return View::make('site.geofences.edit')->with(array('pageTitle'=>'Geo-Cercas', 'actionButton'=>$actionButton, 'templator' => $templator, 'geofence' => $geofence));
	}

	public function postEditGeoFence($id)
	{
		$input = Input::only('name', 'polygon');

		if( empty( trim( $input['polygon'] ) ) )
		{
			return Redirect::back()->withError('Debe de seleccionar un área.')->withInput();				
		}

		$validator = Validator::make(
		    $input, Geofence::$rules
		);

		if ($validator->fails())
		{
			$messages = $validator->errors()->toArray();
			return Redirect::back()->withValidationErrors($messages)->withInput();
		}

		$polygonName = $input['name'];
		$polygonPoints = $input['polygon'];

		$polygonPoints = str_replace('K', 'lng', str_replace('G', 'lat', $polygonPoints));
		$polygonPoints = str_replace('L', 'lng', str_replace('H', 'lat', $polygonPoints));

		$geofence = Geofence::find($id);

		$geofence->name = $polygonName;
		$geofence->points = $polygonPoints;

		$geofence->save();

		return Redirect::route('getAllGeoFences')->withSuccess('Geo-Cerca editada satisfactoriamente');
	}

	public function getAssignVehicles($id)
	{
		$geofence = Geofence::findOrFail($id);

		$vehicles = Vehicle::all();

		$assigned_vehicles = $geofence->vehiclesIds();

		$actionButton = array(
			'route'=>'getAllGeoFences',
			'title'=>'Regresar',
			'class'=>'default'
		);

		$templator = new Templator;

		$templator->setSize(7);
		$templator->createForm($actionRoute = 'postAssignVehicles', $routeParams = $id, $method = 'POST', $legend = "Asignar vehículos a '$geofence->name'", $submitName = 'Guardar', $resetName = null);
		$templator->addLabel('Seleccione los vehículos que quiera asignar a la geo-cerca:');
		foreach ($vehicles as $vehicle) {
			$templator->addCheckboxSingle($id = null, $name = "vehicles[]", $value = $vehicle->id, $label = $vehicle->plate, $checked = in_array($vehicle->id, $assigned_vehicles));
		}

		return View::make('site.geofences.asign-vehicles')->with(array('pageTitle'=>'Geo-Cercas', 'actionButton'=>$actionButton, 'templator' => $templator, 'geofence' => $geofence));
	}

	public function postAssignVehicles($id)
	{
		$vehicles = Input::has('vehicles') ? Input::get('vehicles') : array();

		$geofence = Geofence::findOrFail($id);

		$assigned_vehicles = $geofence->vehiclesIds();

		$gone_vehicles = array_diff($assigned_vehicles, $vehicles);

		$new_vehicles = array_diff($vehicles, $assigned_vehicles);

		if(count($gone_vehicles) > 0)
		{
			GeofenceVehicle::whereIn('vehicle_id', $gone_vehicles)->delete();
		}

		if(count($new_vehicles) > 0)
		{
			foreach ($new_vehicles as $new_vehicle_id) {
				GeofenceVehicle::create([
					'geofence_id' => $id,
					'vehicle_id' => $new_vehicle_id
				]);
			}
		}

		return Redirect::route('getAllGeoFences')->withSuccess('Vehículos asignados satisfactoriamente');
	}

}