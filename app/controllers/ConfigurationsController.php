<?php

class ConfigurationsController extends BaseController {

	public function getConfigurations()
	{
		$configurations = Configuration::all();
		$templator = new Templator;

		$templator->setSize(7);
		$templator->createForm($actionRoute = 'postConfigurations', $routeParams = null, $method = 'POST', $legend = 'Edite la configuraciones principales', $submitName = 'Guardar', $resetName = null);
		foreach ($configurations as $config) {
			$templator->addText($id = 'name', $label = trans('configurations.'.$config->key), $name = $config->key, $placeholder = null, $required = true, $autocomplete=false, $helpblock = null, $value = $config->value);
		}

		return View::make('site.configurations.index')->with(array('pageTitle'=>'Configuraciones', 'actionButton'=> null, 'templator' => $templator));
	}

	public function postConfigurations()
	{
		$configurations = Configuration::all();

		$geofences_notification_targets = Input::get('geofences_notification_targets');		


		$speed_limits_green = Input::get('speed_limits_green');
		$speed_limits_yellow = Input::get('speed_limits_yellow');
		$speed_limits_orange = Input::get('speed_limits_orange');

		$geofences_validation_frecuency = Input::get('geofences_validation_frecuency');

		$geofences_notification_targets = array_map('trim', explode(',', $geofences_notification_targets));

		$speed_limits_green = Arrayer::trimExplode(',', $speed_limits_green);
		$speed_limits_yellow = Arrayer::trimExplode(',', $speed_limits_yellow);

		foreach($geofences_notification_targets as $target)
		{
			$validator = Validator::make(
			    ['target' => $target],
			    ['target' => 'required|email']
			);

			if ($validator->fails())
			{
				return Redirect::back()->withError("El campo '".trans('configurations.geofences_notification_targets')."' debe de contener solo direcciones de correo electrónico separadas por comas.")->withInput();
			}
		}

		if(count($speed_limits_green) != 2 || count($speed_limits_yellow) != 2){
			return Redirect::back()->withError("Lo límites de velocidad verde y amarillo debe de contener 2 valores separados por comas.")->withInput();
		}

		foreach($speed_limits_green as $target)
		{
			$validator = Validator::make(
			    ['target' => $target],
			    ['target' => 'required|integer']
			);

			if ($validator->fails())
			{
				return Redirect::back()->withError("El campo '".trans('configurations.speed_limits_green')."' debe de contener 2 valores enteros separados por comas.")->withInput();
			}
		}

		if($speed_limits_green[0] >= $speed_limits_green[1])
		{
			return Redirect::back()->withError("Los valores del campo '".trans('configurations.speed_limits_green')."' deben de representar un rango coherente.")->withInput();
		}

		foreach($speed_limits_yellow as $target)
		{
			$validator = Validator::make(
			    ['target' => $target],
			    ['target' => 'required|integer']
			);

			if ($validator->fails())
			{
				return Redirect::back()->withError("El campo '".trans('configurations.speed_limits_yellow')."' debe de contener 2 valores enteros separados por comas.")->withInput();
			}
		}

		if($speed_limits_yellow[0] >= $speed_limits_yellow[1])
		{
			return Redirect::back()->withError("Los valores del campo '".trans('configurations.speed_limits_yellow')."' deben de representar un rango coherente.")->withInput();
		}

		if($speed_limits_green[1] > $speed_limits_yellow[0] || $speed_limits_yellow[0] < $speed_limits_green[1])
		{
			return Redirect::back()->withError("El valor del rango de '".trans('configurations.speed_limits_green')."' deben ser menor al de '".trans('configurations.speed_limits_green')."'")->withInput();
		}

		$validator = Validator::make(
		    ['target' => $speed_limits_orange],
		    ['target' => 'required|integer']
		);

		if ($validator->fails())
		{
			return Redirect::back()->withError("El campo '".trans('configurations.speed_limits_orange')."' debe de contener un valor entero.")->withInput();
		}

		$validator = Validator::make(
		    ['target' => $geofences_validation_frecuency],
		    ['target' => 'required|integer']
		);

		if ($validator->fails())
		{
			return Redirect::back()->withError("El campo '".trans('configurations.geofences_validation_frecuency')."' debe de contener un valor entero.")->withInput();
		}

		foreach ($configurations as $configuration) {
			$value = $configuration->value;
			$key = $configuration->key;

			$input = isset($$key) ? ( is_array($$key) ? implode(', ', $$key) : $$key) : Input::get($key);

			if($value != $input)
			{				
				$configuration->value = $input;
				$configuration->save();
			}
		}

		return Redirect::route('getConfigurations')->withSuccess('Configuraciones guardadas satisfactoriamente');
	}

}
