<?php

class ReportsController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getTasksPerWeek()
	{
		$week_number = Input::get('week_number', Timesor::getCurrentWeekNumber());
		$current_year = Timesor::getCurrentYear();

		$date_from = Timesor::getFirstDayOfWeekNumber($current_year, $week_number, $format = 'd/m/Y');
		$date_until = Timesor::getLastDayOfWeekNumber($current_year, $week_number, $format = 'd/m/Y');		

		$from = Timesor::createFromDMY($date_from)->startOfDay()->toDateTimeString();
		$until = Timesor::createFromDMY($date_until)->endOfDay()->toDateTimeString();

		$visits = RouteTask::leftJoin('tracking_routes', 'tracking_routes.id', '=', 'route_tasks.tracking_route_id')
			->where('tracking_routes.date', '>=', $from)
			->where('tracking_routes.date', '<=', $until)
			->distinct('tracking_routes.date')
			->select('tracking_routes.date')
			->get()->toArray();

		$dates = array();

		$current_date = Timesor::createFromDMY($date_from)->subDay()->startOfDay();

		for ($i=0; $i < 7; $i++) { 
			$current_date = $current_date->addDay();
			$date = ['system' => $current_date->format('Y-m-d H:i:s'), 'readable' => $current_date->format('d/m/Y')];
			array_push($dates, $date);
		}

		$weeks = Timesor::getWeeksArray();

		$templator = new Templator;
		$templator->isFormInline();
		$templator->setSize(5);
		$templator->createForm($actionRoute = 'getTasksPerWeek', $routeParams = null, $method = 'GET', $legend = 'Tareas por Semana', $submitName = 'Ver', $resetName = null);
		$templator->addSelectBasic($id = 'week_number', $label = 'Semana', $name = 'week_number', $elements = $weeks, $haveEmptyOption = false, $selected = $week_number);

		return View::make('site.reports.visits-per-user')
			->with(
				array(
					'pageTitle'=>'Reportes',
					'visits' => $visits,
					'templator' => $templator,
					'dates' => $dates,
					'date_from' => Timesor::formatFromSystem('d/m/Y', $dates[0]['system']),
					'date_until' => Timesor::formatFromSystem('d/m/Y', $dates[6]['system'])
					)
				);
	}

	public function getIncidentsPerWeek()
	{
		$week_number = Input::get('week_number', Timesor::getCurrentWeekNumber());
		$current_year = Timesor::getCurrentYear();

		$date_from = Timesor::getFirstDayOfWeekNumber($current_year, $week_number, $format = 'd/m/Y');
		$date_until = Timesor::getLastDayOfWeekNumber($current_year, $week_number, $format = 'd/m/Y');

		$from = Timesor::createFromDMY($date_from)->startOfDay()->toDateTimeString();
		$until = Timesor::createFromDMY($date_until)->endOfDay()->toDateTimeString();

		$incidents = LocationHistory::whereNotNull('incident_type_id')
			->where('created_at', '>=', $from)
			->where('created_at', '<=', $until)
			->get()->toArray();

		$dates = array();

		$current_date = Timesor::createFromDMY($date_from)->subDay()->startOfDay();

		for ($i=0; $i < 7; $i++) { 
			$current_date = $current_date->addDay();
			$date = ['system' => $current_date->format('Y-m-d H:i:s'), 'readable' => $current_date->format('d/m/Y')];
			array_push($dates, $date);
		}

		$weeks = Timesor::getWeeksArray();

		$templator = new Templator;
		$templator->isFormInline();
		$templator->setSize(5);
		$templator->createForm($actionRoute = 'getIncidentsPerWeek', $routeParams = null, $method = 'GET', $legend = 'Incidentes por Semana', $submitName = 'Ver', $resetName = null);
		$templator->addSelectBasic($id = 'week_number', $label = 'Semana', $name = 'week_number', $elements = $weeks, $haveEmptyOption = false, $selected = $week_number);

		return View::make('site.reports.incidents-per-week')
			->with(
				array(
					'pageTitle'=>'Reportes',
					'incidents' => $incidents,
					'dates' => $dates,
					'templator' => $templator,
					'date_from' => Timesor::formatFromSystem('d/m/Y', $dates[0]['system']),
					'date_until' => Timesor::formatFromSystem('d/m/Y', $dates[6]['system'])
					)
				);
	}

	public function getVehicleSpeeds()
    {
        $data = null;

        $vehicle = null;

        $from_date = null;
        $until_date = null;

        if(Input::has('vehicle_id'))
        {

        	$from_date = Input::get('from');
        	$until_date = Input::get('until');

        	$from = Carbon::createFromFormat('d/m/Y', Input::get('from'))->startOfDay()->toDateTimeString();
        	$until = Carbon::createFromFormat('d/m/Y', Input::get('until'))->endOfDay()->toDateTimeString();

        	$vehicle_id = Input::get('vehicle_id');

	        $source = LocationHistory::where('vehicle_id', $vehicle_id)				
				->where('created_at', '>=', $from)
				->where('created_at', '<=', $until)
				->orderBy('created_at', 'DESC')
				->get()->toArray();

			if(count($source) > 2)
			{

				$vehicle = Vehicle::find($vehicle_id);

				$data = GeoPos::speedExceedX($source,$vehicle->speed_limit, 0.001, 'km');

				$dataAsArray = $data->speedAsArray();

				$data = count($dataAsArray) >= 1 ? $data : null ;

			}
        }

        $templator = new Templator;
        $templator->isFormInline();
        $templator->createForm($actionRoute = 'getVehicleSpeeds', $routeParams = null, $method = 'GET', $legend = 'Excesos de Velocidad', $submitName = 'Buscar', $resetName = null);
        $templator->addText($id = 'from', $label = 'Desde', $name = 'from', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
        $templator->addText($id = 'until', $label = 'Hasta', $name = 'until', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
        $templator->addSelectBasic($id = 'vehicle_id', $label = 'Vehículo', $name = 'vehicle_id', Vehicle::all()->lists('plate', 'id'), $haveEmptyOption = false);
        //$templator->addLinkButton('postCreateVehicle', $params = null, $class = 'info', $name = 'Agregar Cliente <i class="fa fa-plus"></i>');

        return View::make('site.reports.vehicle-speeds')->with(array(
        	'pageTitle'=>'Reportes',
        	'actionButton'=> null /*$actionButton*/,
        	'templator' => $templator, 
        	'data' => $data,
        	'vehicle' => $vehicle,
        	'from_date' => $from_date,
        	'until_date' => $until_date,
        ));
    }

    public function getExportVehicleSpeeds()
    {
        $data = null;

        $vehicle = null;

        $from_date = null;
        $until_date = null;

        if(Input::has('vehicle_id'))
        {

        	$from_date = Input::get('from');
        	$until_date = Input::get('until');

        	$from = Carbon::createFromFormat('d/m/Y', Input::get('from'))->startOfDay()->toDateTimeString();
        	$until = Carbon::createFromFormat('d/m/Y', Input::get('until'))->endOfDay()->toDateTimeString();

        	$vehicle_id = Input::get('vehicle_id');

	        $source = LocationHistory::where('vehicle_id', $vehicle_id)				
				->where('created_at', '>=', $from)
				->where('created_at', '<=', $until)
				->orderBy('created_at', 'ASC')
				->get()->toArray();

			if(count($source) > 2)
			{

				$vehicle = Vehicle::find($vehicle_id);

				$data = GeoPos::speedExceedX($source,$vehicle->speed_limit, 0.001, 'km');

				$data = count($data) > 1 ? $data : null ;

				$now = Carbon::now()->format('Ymdhis');

		    	return Excel::create("excesos-velocidad-$now", function($excel) use($data, $vehicle, $from_date, $until_date){

				    $excel->sheet('excesos de velocidad', function($sheet) use($data, $vehicle, $from_date, $until_date){

				        $sheet->loadView('site.reports.export.vehicle-speeds')->with(array(
				        	'data' => $data,
				        	'vehicle' => $vehicle,
				        	'from_date' => $from_date,
				        	'until_date' => $until_date,
				        ));
				    });

				    $excel->export('xls');
				});
			}

			return Redirect::back()->withError('No hay datos que exportar');
        }
    }

	public function getGeofencesAccess()
    {
        $data = null;

        $geofence = null;

        $from_date = null;
        $until_date = null;

        if(Input::has('geofence_id'))
        {

        	$from_date = Input::get('from');
        	$until_date = Input::get('until');

        	$from = Carbon::createFromFormat('d/m/Y', Input::get('from'))->startOfDay()->toDateTimeString();
        	$until = Carbon::createFromFormat('d/m/Y', Input::get('until'))->endOfDay()->toDateTimeString();

        	$geofence_id = Input::get('geofence_id');

        	$geofence = Geofence::find($geofence_id);

	        $source = GeofenceRecord::where('geofence_id', $geofence_id)				
				->where('created_at', '>=', $from)
				->where('created_at', '<=', $until)
				->orderBy('created_at', 'ASC')
				->get();

			if(!$source->isEmpty())
			{

				$data = $source;

			}
        }

        $templator = new Templator;
        $templator->isFormInline();
        $templator->createForm($actionRoute = 'getGeofencesAccess', $routeParams = null, $method = 'GET', $legend = 'Reporte de Geo-Cercas', $submitName = 'Buscar', $resetName = null);
        $templator->addText($id = 'from', $label = 'Desde', $name = 'from', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
        $templator->addText($id = 'until', $label = 'Hasta', $name = 'until', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
        $templator->addSelectBasic($id = 'geofence_id', $label = 'Geo-Cerca', $name = 'geofence_id', Geofence::all()->lists('name', 'id'), $haveEmptyOption = false);
        //$templator->addLinkButton('postCreateVehicle', $params = null, $class = 'info', $name = 'Agregar Cliente <i class="fa fa-plus"></i>');

        return View::make('site.reports.geofences')->with(array(
        	'pageTitle'=>'Reportes',
        	'actionButton'=> null /*$actionButton*/,
        	'templator' => $templator, 
        	'data' => $data,
        	'geofence' => $geofence,
        	'from_date' => $from_date,
        	'until_date' => $until_date,
        ));
    }

    public function getExportGeofencesAccess()
    {
        
        $data = null;

        $geofence = null;

        $from_date = null;
        $until_date = null;

        if(Input::has('geofence_id'))
        {

        	$from_date = Input::get('from');
        	$until_date = Input::get('until');

        	$from = Carbon::createFromFormat('d/m/Y', Input::get('from'))->startOfDay()->toDateTimeString();
        	$until = Carbon::createFromFormat('d/m/Y', Input::get('until'))->endOfDay()->toDateTimeString();

        	$geofence_id = Input::get('geofence_id');

        	$geofence = Geofence::find($geofence_id);

	        $source = GeofenceRecord::where('geofence_id', $geofence_id)				
				->where('created_at', '>=', $from)
				->where('created_at', '<=', $until)
				->orderBy('created_at', 'ASC')
				->get();

			if(!$source->isEmpty())
			{

				$data = $source;

				$now = Carbon::now()->format('Ymdhis');

		    	return Excel::create("salidas-geocercas-$now", function($excel) use($data, $geofence, $from_date, $until_date){

				    $excel->sheet('salidas de geocerca', function($sheet) use($data, $geofence, $from_date, $until_date){

				        $sheet->loadView('site.reports.export.geofences')->with(array(
				        	'data' => $data,
				        	'geofence' => $geofence,
				        	'from_date' => $from_date,
				        	'until_date' => $until_date,
				        ));
				    });

				    $excel->export('xls');
				});

			}

			return Redirect::back()->withError('No hay datos que exportar');
        }

    }

	public function getGasConsumption()
    {
        $data = null;

        $vehicle = 0;

        $from_date = null;
        $until_date = null;

        if(Input::has('vehicle_id'))
        {
        	$data = array();

        	$from_date = Input::get('from');
        	$until_date = Input::get('until');

        	$from = Carbon::createFromFormat('d/m/Y', Input::get('from'))->startOfDay();
        	$until = Carbon::createFromFormat('d/m/Y', Input::get('until'))->endOfDay();

        	$vehicle_id = Input::get('vehicle_id');

        	if($vehicle_id == 0)
        	{

        		$vehicles = Vehicle::all();

        		foreach ($vehicles as $veh) {

        			$fromx = $from->copy();
        			
	        		while( $fromx->isPast($until) || $fromx->isSameDay($until) )
	        		{
	        			$_from = $fromx->copy()->startOfDay();
	        			$_until = $_from->copy()->endOfDay();


				        $source = LocationHistory::where('vehicle_id', $veh->id)				
							->where('created_at', '>=', $_from)
							->where('created_at', '<=', $_until)
							->orderBy('created_at', 'ASC')
							->get()->toArray();

						if(count($source) > 2)
						{
							$distance = GeoPos::distance($source);

							array_push($data, ['date' => $_from->copy()->format('Y-m-d'), 'plate' => $veh->plate, 'distance' => $distance, 'gas_consumption' => $distance / $veh->fuel_performance]);

						}

						$fromx->addDays(1);
	        		}

        		}

        	}else{
        		$vehicle = Vehicle::find($vehicle_id);
        			
        		while( $from->isPast($until) || $from->isSameDay($until) )
        		{
        			$_from = $from->copy()->startOfDay();
        			$_until = $_from->copy()->endOfDay();


			        $source = LocationHistory::where('vehicle_id', $vehicle->id)				
						->where('created_at', '>=', $_from)
						->where('created_at', '<=', $_until)
						->orderBy('created_at', 'ASC')
						->get()->toArray();

					if(count($source) > 2)
					{
						$distance = GeoPos::distance($source);

						array_push($data, ['date' => $_from->copy()->format('Y-m-d'), 'plate' => $vehicle->plate, 'distance' => $distance, 'gas_consumption' => $distance / $vehicle->fuel_performance]);

					}

					$from->addDays(1);
        		}
        	}
        }

        $templator = new Templator;
        $templator->isFormInline();
        $templator->createForm($actionRoute = 'getGasConsumption', $routeParams = null, $method = 'GET', $legend = 'Buscar Recorridos', $submitName = 'Buscar', $resetName = null);
        $templator->addText($id = 'from', $label = 'Desde', $name = 'from', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
        $templator->addText($id = 'until', $label = 'Hasta', $name = 'until', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
        $templator->addSelectBasic($id = 'vehicle_id', $label = 'Vehículo', $name = 'vehicle_id', $elements = array('0'=>'Cualquiera')+Vehicle::all()->lists('plate', 'id'), $haveEmptyOption = false);
        //$templator->addLinkButton('postCreateVehicle', $params = null, $class = 'info', $name = 'Agregar Cliente <i class="fa fa-plus"></i>');

        return View::make('site.reports.gas-consumption')->with(array(
        	'pageTitle'=>'Reportes',
        	'actionButton'=> null /*$actionButton*/,
        	'templator' => $templator, 
        	'data' => $data,
        	'vehicle' => $vehicle,
        	'from_date' => $from_date,
        	'until_date' => $until_date,
        ));
    }

	public function getGasConsumptionExport()
    {
        $data = null;

        $vehicle = 0;

        $from_date = null;
        $until_date = null;

        if(Input::has('vehicle_id'))
        {

        	$data = array();

        	$from_date = Input::get('from');
        	$until_date = Input::get('until');

        	$from = Carbon::createFromFormat('d/m/Y', Input::get('from'))->startOfDay();
        	$until = Carbon::createFromFormat('d/m/Y', Input::get('until'))->endOfDay();

        	$vehicle_id = Input::get('vehicle_id');

        	if($vehicle_id == 0)
        	{

        		$vehicles = Vehicle::all();

        		foreach ($vehicles as $veh) {

        			$fromx = $from->copy();
        			
	        		while( $fromx->isPast($until) || $fromx->isSameDay($until) )
	        		{
	        			$_from = $fromx->copy()->startOfDay();
	        			$_until = $_from->copy()->endOfDay();


				        $source = LocationHistory::where('vehicle_id', $veh->id)				
							->where('created_at', '>=', $_from)
							->where('created_at', '<=', $_until)
							->orderBy('created_at', 'ASC')
							->get()->toArray();

						if(count($source) > 2)
						{
							$distance = GeoPos::distance($source);

							array_push($data, ['date' => $_from->copy()->format('Y-m-d'), 'plate' => $veh->plate, 'distance' => $distance, 'gas_consumption' => $distance / $veh->fuel_performance]);

						}

						$fromx->addDays(1);
	        		}

        		}

        	}else{
        		$vehicle = Vehicle::find($vehicle_id);
        			
        		while( $from->isPast($until) || $from->isSameDay($until) )
        		{
        			$_from = $from->copy()->startOfDay();
        			$_until = $_from->copy()->endOfDay();


			        $source = LocationHistory::where('vehicle_id', $vehicle->id)				
						->where('created_at', '>=', $_from)
						->where('created_at', '<=', $_until)
						->orderBy('created_at', 'ASC')
						->get()->toArray();

					if(count($source) > 2)
					{
						$distance = GeoPos::distance($source);

						array_push($data, ['date' => $_from->copy()->format('Y-m-d'), 'plate' => $vehicle->plate, 'distance' => $distance, 'gas_consumption' => $distance * $vehicle->fuel_performance]);

					}

					$from->addDays(1);
        		}
        	}


			if(!empty($data))
			{

				$now = Carbon::now()->format('Ymdhis');

		    	return Excel::create("consumo-de-combustible-$now", function($excel) use($data){

				    $excel->sheet('consumo de combustible', function($sheet) use($data){

				        $sheet->loadView('site.reports.export.gas-consumption')->with(array( 
				        	'data' => $data
				        ));
				    });

				    $excel->export('xls');
				});

			}

        }

		return Redirect::back()->withError('No hay datos que exportar');
    }

}
