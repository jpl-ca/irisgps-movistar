<?php

class UserManualController extends BaseController {

	private $pageTitle = 'Manual de Usuario';

	public $pages = [
		/*'getIndex' => [
			'uri' => 'manual-de-usuario',
			'params' => [
				'legend' => 'Inicio',
				'detail_view' => 'index',
				'image' => 'logo'
			]
		],*/
		'getInicioDeSesion' => [
			'uri' => 'manual-de-usuario/inicio-de-sesion',
			'params' => [
				'legend' => 'Inicio de sesión',
				'detail_view' => array(
					'inicio-de-sesion',
					'recuperar-password'
				),
				'image' => array(
					1,
					43
				)
			]
		],
		'getMenuPrincipal' => [
			'uri' => 'manual-de-usuario/menu-principal',
			'params' => [
				'legend' => 'Menu principal',
				'detail_view' => 'menu-principal',
				'image' => '2'
			]
		],
		'getPerfilDeUsuario' => [
			'uri' => 'manual-de-usuario/perfil-de-usuario',
			'params' => [
				'legend' => 'Perfil de usuario',
				'detail_view' => 'perfil-de-usuario',
				'image' => '3'
			]
		],
		'getVerLocalización' => [
			'uri' => 'manual-de-usuario/ver-localizacion',
			'params' => [
				'legend' => 'Ver Localización',
				'detail_view' => 'ver-localizacion',
				'image' => '4'
			]
		],
		'getRecorridoDelVehiculo' => [
			'uri' => 'manual-de-usuario/recorrido-del-vehiculo',
			'params' => [
				'legend' => 'Recorrido del vehículo',
				'detail_view' => 'recorrido-del-vehiculo',
				'image' => '5'
			]
		],
		'getRutalDelVehículo' => [
			'uri' => 'manual-de-usuario/rutal-del-vehiculo',
			'params' => [
				'legend' => 'Ruta del vehículo',
				'detail_view' => array(
					'ruta-del-vehiculo-detalle-recorrido',
					'ruta-del-vehiculo-detalle-vehiculo',
					'ruta-del-vehiculo-pasajeros',
					'ruta-del-vehiculo-tareas',
					'ruta-del-vehiculo-comentarios',
					'ruta-del-vehiculo-incidencias'
					), 
				'image' => array(
					6,
					7,
					8,
					9,
					10,
					11)
				]
		],
		'getBuscarRecorridos' => [
			'uri' => 'manual-de-usuario/buscar-recorridos',
			'params' => [
				'legend' => 'Buscar recorridos',
				'detail_view' => 'buscar-recorridos',
				'image' => '12'
			]
		],
		'getVehiculos' => [
			'uri' => 'manual-de-usuario/vehiculos',
			'params' => [
				'legend' => 'Vehículos',
				'detail_view' => array(
					'vehiculos-todo',
					'vehiculos-nuevo',
					'vehiculos-eliminar',
					'vehiculos-editar',
					),
				'image' => array(
					13,
					14,
					15,
					16
					)
			]
		],
		'getEmpleados' => [
			'uri' => 'manual-de-usuario/empleados',
			'params' => [
				'legend' => 'Empleados',
				'detail_view' => array(
					'empleados-todo',
					'empleados-nuevo',
					'empleados-editar',
					'empleados-eliminar',
					),
				'image' => array(
					17,
					18,
					19,
					20
					)
			]
		],
		'getDispositivos' => [
			'uri' => 'manual-de-usuario/dispositivos',
			'params' => [
				'legend' => 'Dispositivos',
				'detail_view' => array(
					'dispositivos-todo',
					'dispositivos-nuevo',
					'dispositivos-editar',
					'dispositivos-eliminar',
					),
				'image' => array(
					21,
					22,
					23,
					24
					)
			]
		],
		'GetRutas' => [
			'uri' => 'manual-de-usuario/rutas',
			'params' => [
				'legend' => 'Rutas',
				'detail_view' => array(
					'rutas-todo',
					'rutas-nuevo',
					'rutas-editar',
					'rutas-agregar-pasajero',
					'rutas-agregar-cliente',
					'rutas-eliminar',
					),
				'image' => array(
					25,
					26,
					27,
					28,
					29,
					30
					)
			]
		],		
		'getBuscarRutas' => [
			'uri' => 'manual-de-usuario/buscar-rutas',
			'params' => [
				'legend' => 'Buscar rutas',
				'detail_view' => array(
					'buscar-rutas',
					'buscar-rutas-resultado',
					),
				'image' => array(
					31,
					32
					)
			]
		],
		'getClientes' => [
			'uri' => 'manual-de-usuario/clientes',
			'params' => [
				'legend' => 'Clientes',
				'detail_view' => array(
					'clientes-todo',
					'clientes-nuevo',
					'clientes-editar',
					'clientes-eliminar',
					),
				'image' => array(
					33,
					34,
					35,
					36
					)
			]
		],		
		'GetUsuarios' => [
			'uri' => 'manual-de-usuario/usuarios',
			'params' => [
				'legend' => 'Usuarios',
				'detail_view' => array(
					'usuarios-todo',
					'usuarios-nuevo',
					'usuarios-editar',
					'usuarios-eliminar',
					),
				'image' => array(
					37,
					38,
					39,
					40
					)
			]
		],

		'getReportesTareasPorSemana' => [
			'uri' => 'manual-de-usuario/reportes/tareas-por-semana',
			'params' => [
				'legend' => 'Tareas por semana',
				'detail_view' => 'reportes-tareas-por-semana',
				'image' => '41'
			]
		],
		'getReportesIncidentesPorSemana' => [
			'uri' => 'manual-de-usuario/reportes/incidentes-por-semana',
			'params' => [
				'legend' => 'Incidentes por semana',
				'detail_view' => 'reportes-incidentes-por-semana',
				'image' => '42'
			]
		],
	];

	private function generateView($index) {
		return View::make('site.user-manual.manual-base')
			->with(array(
				'pageTitle'		=> $this->pageTitle,
				'legend'		=> $this->pages[$index]['params']['legend'],
				'detail_view'	=> $this->pages[$index]['params']['detail_view'],
				'image'			=> $this->pages[$index]['params']['image']
			));
	}

	function __call($method, $arguments) {
		return array_key_exists($method,$this->pages) ? $this->generateView($method) : false;
	}
}
