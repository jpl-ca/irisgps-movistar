<?php

class VehiclesController extends \BaseController {

	public function getVehiclesPositions()
	{
		$response = VehicleService::vehiclesLastPositions();

		$geofences = Geofence::all();

		if(!JMUserAgent::isAndroidRequest()){
			$data = $response['data'];
			return View::make('site.vehicles.positions')->with(array(
				'pageTitle'=>'Posicionamiento',
				'data' => $data,
				'geofences' => $geofences
			));
		}

		return $response;
	}

	public function getVehicleLastTrackingRoute($plate)
	{
		$response = VehicleService::vehicleLastPath($plate);
		$data = $response['data'];
		return View::make('site.vehicles.route')->with(array('pageTitle'=>'Ruta del Vehículo', 'data' => $data));
	}

	public function getVehiclePathByDate($plate, $date = null)
	{
    	$snapped = Input::has('snapped');
    	$both = Input::has('both');
    	$countpath = Input::has('countpath');
    	$showpath = Input::has('showpath');
    	
		$vehicle = Vehicle::where('plate', $plate)->first();
		$path = $vehicle->getLocationHistoryByDate($date);
		$pathArray = $path->toArray();

        if($countpath)
        {
        	return dd(count($pathArray));
        }
        
        if($showpath)
        {
        	return dd($path->toArray());
        }

        if(count($pathArray) > 2)
        {
        	//$speeds = GeoPos::speed($path->toArray(), 0.01, 'km');
        	$speeds = GeoPos::speed2($path->toArray(), 0.001, 'km');
        	$distance = GeoPos::distance($path->toArray());
        }else{
        	$speeds = new FakeSpeeds;
        	$distance = 0;
        }

		return View::make('site.vehicles.path')->with(array(
			'pageTitle'=>'Recorrido del Vehículo',
			'data' => array(
				'vehicle' => $vehicle,
				'path' => $path,
				'distance' => $distance, 
				'speeds' => $speeds),
			'snapped' => $snapped, 'both' => $both, 'date' => $date
			));
	}

	public function getVehiclePathByDateAPI($plate, $date = null)
	{
    	$snapped = Input::has('snapped');
    	$both = Input::has('both');
    	$countpath = Input::has('countpath');
    	$showpath = Input::has('showpath');
    	
		$vehicle = Vehicle::where('plate', $plate)->first();
		$path = $vehicle->getLocationHistoryByDate($date);

        if($countpath)
        {
        	return dd(count($path->toArray()));
        }
        
        if($showpath)
        {
        	return dd($path->toArray());
        }

        if(count($path) > 2)
        {
        	$speeds = GeoPos::speed($path->toArray(), 0.01, 'km');
        }else{
        	$speeds = new FakeSpeeds;
        }

        $distance = GeoPos::distance($path->toArray());

		$json = array(
			'vehicle' => $vehicle,
			'path' => $path,
			'distance' => $distance, 
			'speeds' => $speeds,
			'snapped' => $snapped, 'both' => $both, 'date' => $date
		);

		return $json;
	}

	public function getCreateVehicle()
	{

		$actionButton = array(
			'route'=>'getAllVehicles',
			'title'=>'Regresar',
			'class'=>'default'
		);

		$templator = new Templator;

		$templator->createForm($actionRoute = 'postCreateVehicle', $routeParams = ['getAllVehicles', true], $method = 'POST', $legend = 'Crear nuevo vehículo', $submitName = 'Agregar', $resetName = null);
		$templator->addText($id = 'plate', $label = 'Placa', $name = 'plate', $placeholder = '?#?-###', $required = true, $autocomplete=false, $helpblock = "La placa debe de tener el formato '?#?-###' por ejemplo A1B-234");
		$templator->addSelectBasic($id = 'brand', $label = 'Marca', $name = 'brand', $elements = ['Nissan' => 'Nissan', 'Toyota' => 'Toyota'], $haveEmptyOption = false);
		$templator->addSelectBasic($id = 'model', $label = 'Modelo', $name = 'model', $elements = ['Almera' => 'Almera'], $haveEmptyOption = false);
		$templator->addSelectBasic($id = 'color', $label = 'Color', $name = 'color', $elements = ['Rojo' => 'Rojo'], $haveEmptyOption = false);
		$templator->addText($id = 'speed_limit', $label = 'Límite de velocidad', $name = 'speed_limit', $placeholder = 'xxx', $required = true, $autocomplete=false, $helpblock = "El límite de velocidad debe de ser expresado en km/hr.");
		$templator->addText($id = 'fuel_performance', $label = 'Rendimiento de combustible', $name = 'fuel_performance', $placeholder = 'x.xxx', $required = true, $autocomplete=false, $helpblock = "El rendimiento debe de ser expresado en km/gal.");

		$pusher = new Pusher();		
		$devices = Device::wherenotNull('gcm')->select('gcm')->lists('gcm');
		$pusher->push_type = 'NEW_VEHICLE';
		$pusher->addRecipent($devices);
		$pusher->send();

		return View::make('site.vehicles.create')->with(array('pageTitle'=>'Vehículos', 'actionButton'=>$actionButton, 'templator' => $templator));
	}

	public function postCreateVehicle($route = null, $withInput = false)
	{
		$input = Input::only('plate', 'brand', 'model', 'color', 'fuel_performance', 'speed_limit');

		$response = VehicleService::createVehicle($input);

		if(!JMUserAgent::isAndroidRequest()){
			if($response['status'] == 'error')
			{
				if($response['code'] == 'irs402')
				{
					return Redirect::back()->withValidationErrors($response['data'])->withInput();
				}else{
					return Redirect::back()->withValidationError('Hubo un error al procesar la acción.')->withInput();					
				}
			}else{
				if(!is_null($route))
				{
					return Redirect::route($route)->withSuccess('Vehículo creado satisfactoriamente');
				}
				return Redirect::back()->withSuccess('Vehículo creado satisfactoriamente');
			}
		}

		return $response;
	}


	public function getEditVehicle($vehicle_id)
	{
        $vehicle = Vehicle::findOrFail($vehicle_id);

		$actionButton = array(
			'route'=>'getAllVehicles',
			'title'=>'Regresar',
			'class'=>'default'
		);

		$templator = new Templator;

		$templator->createForm($actionRoute = 'postEditVehicle', $routeParams = [$vehicle->id], $method = 'POST', $legend = 'Editar vehículo', $submitName = 'Guardar', $resetName = null);
		$templator->addText($id = 'plate', $label = 'Placa', $name = 'plate', $placeholder = '?#?-###', $required = true, $autocomplete=false, $helpblock = "La placa debe de tener el formato '?#?-###' por ejemplo A1B-234", $value = $vehicle->plate);
		$templator->addElement(['type' => 'text', 'label' => 'Marca', 'name' => 'brand', 'id' => 'brand', 'value' => $vehicle->brand]);
		$templator->addElement(['type' => 'text', 'label' => 'Modelo', 'name' => 'model', 'id' => 'model', 'value' => $vehicle->model]);
		$templator->addElement(['type' => 'text', 'label' => 'Color', 'name' => 'color', 'id' => 'color', 'value' => $vehicle->color]);
		$templator->addText($id = 'speed_limit', $label = 'Límite de velocidad', $name = 'speed_limit', $placeholder = 'xxx', $required = true, $autocomplete=false, $helpblock = "El límite de velocidad debe de ser expresado en km/hr.", $value = $vehicle->speed_limit);
		$templator->addText($id = 'fuel_performance', $label = 'Rendimiento de combustible', $name = 'fuel_performance', $placeholder = 'x.xxx', $required = true, $autocomplete=false, $helpblock = "El rendimiento debe de ser expresado en km/gal.", $value = $vehicle->fuel_performance);

		return View::make('site.CRUD.basic-form')->with(array('pageTitle'=>'Vehículos', 'actionButton'=>$actionButton, 'templator' => $templator));
	}

	public function postEditVehicle($vehicle_id)
	{
        $vehicle = Vehicle::findOrFail($vehicle_id);

		$input = Input::only('plate', 'brand', 'model', 'color', 'fuel_performance', 'speed_limit');

        $validator = Validator::make(
            $input,
            $vehicle->getUpdateRules()
        );

        if ($validator->fails())
        {
            $messages = $validator->errors()->toArray();
            return Redirect::back()->withValidationErrors($messages);
        }

        $vehicle->plate = $input['plate'];
        $vehicle->brand = $input['brand'];
        $vehicle->model = $input['model'];
        $vehicle->color = $input['color'];
        $vehicle->fuel_performance = $input['fuel_performance'];
        $vehicle->speed_limit = $input['speed_limit'];

        $vehicle->save();

		return Redirect::back()->withSuccess('Vehículo editado satisfactoriamente');
	}

	public function getAllVehicles()
	{
		$response = VehicleService::allVehicles();
		$data = $response['data'];

		$actionButton = array(
			'route'=>'getCreateVehicle',
			'title'=>'Agregar Nuevo',
			'class'=>'info'
		);

		return View::make('site.vehicles.all')->with(array('pageTitle'=>'Vehículos', 'actionButton'=>$actionButton, 'data' => $data));
	}

	/*
	public function index()
	{
		//
	}


	public function create()
	{
		//
	}


	public function store()
	{
		//
	}


	public function show($id)
	{
		//
	}


	public function edit($id)
	{
		//
	}


	public function update($id)
	{
		//
	}


	public function destroy($id)
	{
		//
	}
	*/

}