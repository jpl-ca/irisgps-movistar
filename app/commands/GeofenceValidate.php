<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class GeofenceValidate extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'geofence:validate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Validate if vehicles are in or out geofences';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	protected function getOptions()
	{
	    return array(
	        array('fake', 'fake', InputOption::VALUE_NONE , 'An example argument.')
	    );
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		Log::info('=== se esta disparando la validación de geocercas');

		$isFake = $this->option('fake');

		if($isFake)
		{
			$data =array();

			array_push($data, ['plate' => 'ABC-123', 'geofence' => 'geocerca 1', 'date' => Carbon::now()->subMinutes(5)->format('Y-m-d H:i:s'), 'latlng' => "-12.0248082,-77.0824949"]);
			array_push($data, ['plate' => 'AQW-353', 'geofence' => 'geocerca 2', 'date' => Carbon::now()->subMinutes(4)->format('Y-m-d H:i:s'), 'latlng' => "-12.0213661,-77.061398"]);
			array_push($data, ['plate' => 'HTW-234', 'geofence' => 'geocerca 3', 'date' => Carbon::now()->subMinutes(3)->format('Y-m-d H:i:s'), 'latlng' => "-12.0252944,-77.0452264"]);
			array_push($data, ['plate' => 'MJS-994', 'geofence' => 'geocerca 1', 'date' => Carbon::now()->subMinutes(3)->format('Y-m-d H:i:s'), 'latlng' => "-12.0250925,-77.0363308"]);
			array_push($data, ['plate' => 'KSS-122', 'geofence' => 'geocerca 5', 'date' => Carbon::now()->subMinutes(2)->format('Y-m-d H:i:s'), 'latlng' => "-12.0349064,-77.0160061"]);

			$devices = Device::wherenotNull('gcm')->select('gcm')->lists('gcm');

			$pusher = new Pusher();

			$pusher->push_type = 'GEO';

			$pusher->addRecipent($devices);

			$pusher->addData([
				'data' => json_encode($data) 
			]);

			$pusher->send();

		}else{//real			

			$geofences = Geofence::all();

			$geofences_validation_frecuency = Configuration::whereKey('geofences_validation_frecuency')->first()->value;

			$geofences_notification_targets = array_map('trim', explode(',', Configuration::whereKey('geofences_notification_targets')->first()->value));


			$until = Carbon::now();
			$from = $until->copy()->subMinutes($geofences_validation_frecuency)->toDateTimeString();

			$until = $until->toDateTimeString();

			$vehicles = Vehicle::all();

			$data_to_evaluate = array();

			foreach ($vehicles as $vehicle) {
				if( !is_null( $lastposition = $vehicle->lastPositionInRange($from, $until) ) )
				{
					$data_to_evaluate[$vehicle->id] = [
						'vehicle_id' => $lastposition->vehicle_id,
						'lat' => $lastposition->lat,
						'lng' => $lastposition->lng,
						'date' => $lastposition->created_at,
						'plate' => $vehicle->plate
					];
					$this->info("vehiculos". $vehicle->id ." lat ". $lastposition->lat." lng ".$lastposition->lat);
				}
			}

			$vehicles_in = array();
			$vehicles_out = array();
			$vehicles_out_push = array();

			foreach ($geofences as $geofence) {

				$assigned_vehicles = $geofence->vehiclesIds();

				$this->info("vehiculos asignados a ". $geofence->name.": ".implode(', ', $assigned_vehicles));

				$this->info("vehiculos asignados a ". $geofence->name." a evaluar con posicion: ".implode(', ', array_keys($data_to_evaluate)));

				$assigned_vehicles_with_last_position = array_only($data_to_evaluate, array_intersect(array_keys($data_to_evaluate), $assigned_vehicles));

				$polygon = new PolygonObject($geofence->name, JSON::toArray($geofence->points));

				foreach ($assigned_vehicles_with_last_position as $assigned_vehicle) {

					$vehicle_id = $assigned_vehicle['vehicle_id'];
					$lat = $assigned_vehicle['lat'];
					$lng = $assigned_vehicle['lng'];
					$date = $assigned_vehicle['date'];
					$plate = $assigned_vehicle['plate'];

					$_latLng = $assigned_vehicle['lat'].','.$assigned_vehicle['lng'];

					$this->info("vehiculos". $vehicle->id ." latlng ". $_latLng);

					$is_inside = $polygon->contains($lat, $lng);

					$previous_record = GeofenceRecord::whereNull('entrance')
						->whereGeofenceId($geofence->id)
						->whereVehicleId($vehicle_id)
						->first();

					if(!$is_inside && is_null($previous_record)){ // esta fuera de la geocerca pero no tenia un registro previo
						// ha salido de la geocerca por primera vez
						// crear registro
						
						$newRecord =GeofenceRecord::create([
							'geofence_id' => $geofence->id,
							'vehicle_id' => $vehicle_id,
							'departure' => $date,
							'departure_latlng' => "$_latLng"
						]);

						array_push(
							$vehicles_out_push,
							[
								'plate' => $assigned_vehicle['plate'],
								'geofence' => $geofence->name,
								'date' => $newRecord->departure->format('Y-m-d H:i:s'),
								'latlng' => $_latLng
							]
						);

						array_push($vehicles_out, array_merge($assigned_vehicle, ['geofence' => $geofence->name]));
						
						
					}

					if($is_inside && !is_null($previous_record)){ // esta dentro de la geocerca pero ya habia registro previo
						// esta entrando
						// actualizar registro
												
						$previous_record->entrance = $date;
						$previous_record->entrance_latlng = $_latLng;
						$previous_record->save();

						array_push($vehicles_in, array_merge($assigned_vehicle, ['geofence' => $geofence->name]));
					}
				}
			}

			$vehicles_in = array_filter($vehicles_in);
			$vehicles_out = array_filter($vehicles_out);

			if(count($vehicles_in) > 0 || count($vehicles_out) > 0)
			{			

				Mail::send('emails.geofences', [
					'vehicles_in' => $vehicles_in,
					'vehicles_out' => $vehicles_out
				], function($message) use ($geofences_notification_targets)
				{
				    $message->from('geo-cercas@movistar.irisgps.com', 'Iris GPS');
				    foreach ($geofences_notification_targets as $target) {
				    	$message->to( $target );
				    }
				    $message->subject('Alertas de Geo-Cercas');
				});

				if(count($vehicles_out_push) > 0)
				{
					$devices = Device::wherenotNull('gcm')->select('gcm')->lists('gcm');

					$pusher = new Pusher();

					$pusher->push_type = 'GEO';

					$pusher->addRecipent($devices);

					$pusher->addData([
						'data' => $vehicles_out_push,
					]);

					$pusher->send();
				}

			}
		}
	}

}
