<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SpeedAlerts extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'speed:alerts';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Validate if vehicles over speed limit';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	protected function getOptions()
	{
	    return array(
	        array('fake', 'fake', InputOption::VALUE_NONE , 'An example argument.'),	        
	        array('day', 'day', InputOption::VALUE_NONE , 'An example argument.')
	    );
	}


	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		Log::info('>>> se esta disparando la validación de velocidad');

		$isFake = $this->option('fake');

		if($isFake)
		{
			$devices = Device::wherenotNull('gcm')->select('gcm')->lists('gcm');

			$pusher = new Pusher();

			$pusher->push_type = 'VEL_EXC';

			$pusher->addRecipent($devices);

			$pusher->addData([
				'data' => '[{"plate":"ASY-579","speed_exceeds":[{"distance":0.18853769118439,"time":0.1,"speed":37.6653,"from":"2015-09-02 17:27:05","until":"2015-09-02 17:26:59","pointA":"-12.091899800000000,-77.031858400000000","pointB":"-12.091530000000000,-77.030168100000000"}]},{"plate":"BDE-774","speed_exceeds":[{"distance":0.62931237007719,"time":0.4,"speed":66.4563,"from":"2015-09-09 12:18:51","until":"2015-09-09 12:18:27","pointA":"-12.082692200000000,-76.980033100000000","pointB":"-12.078458400000000,-76.976202100000000"}]}]' 
			]);

			$pusher->send();

		}else{ //real

			$vehicles = Vehicle::all();

			$nowDate = Carbon::now();


			$dayOption = $this->option('day');

			if($dayOption)
			{
				$from = $nowDate->copy()->subMinutes(1440)->toDateTimeString();
	  			$until = $nowDate->copy()->toDateTimeString();
			}else{
				$from = $nowDate->copy()->subMinutes(5)->toDateTimeString();
	  			$until = $nowDate->copy()->toDateTimeString();
			}

	    	$speed_alerts = array();


			foreach ($vehicles as $vehicle) {			

		        $source = LocationHistory::where('vehicle_id', $vehicle->id)				
					->where('created_at', '>=', $from)
					->where('created_at', '<=', $until)
					->orderBy('created_at', 'DESC')
					->get()->toArray();

				if(count($source) > 2)
				{

					$data = GeoPos::speedExceedW($source,$vehicle->speed_limit, 0.001, 'km');

					if(!empty($data))
					{
						$to_json = ['plate' => $vehicle->plate, 'speed_exceeds' => $data];

						array_push($speed_alerts, $to_json);

					}

				}
			}

			if(count($speed_alerts) > 0)
			{

				$devices = Device::wherenotNull('gcm')->select('gcm')->lists('gcm');

				$pusher = new Pusher();

				$pusher->push_type = 'VEL_EXC';

				$pusher->addRecipent($devices);

				$pusher->addData([
					'data' => json_encode($speed_alerts) 
				]);

				$pusher->send();

				$geofences_notification_targets = array_map('trim', explode(',', Configuration::whereKey('geofences_notification_targets')->first()->value));

				Mail::send('emails.speedexceed', [
					'speed_alerts' => $speed_alerts,
				], function($message) use ($geofences_notification_targets)
				{
				    $message->from('geo-cercas@movistar.irisgps.com', 'Iris GPS');
				    foreach ($geofences_notification_targets as $target) {
				    	$message->to( $target );
				    }
				    $message->subject('Alertas de Velocidad');
				});

			}

		}

		

	}

}
