<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UserTypesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		UserType::create(['name'=>'Administrador']);
		UserType::create(['name'=>'Usuario']);
	}

}