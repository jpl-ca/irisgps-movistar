<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class TrackingRoutesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 5) as $vehicleId)
		{
			foreach(range(1, 150) as $days)
			{
				TrackingRoute::create([
					'date' => Carbon::now()->addDays($days-1)->startOfDay(),
					'vehicle_id' => $vehicleId
				]);
			}
		}
	}

}