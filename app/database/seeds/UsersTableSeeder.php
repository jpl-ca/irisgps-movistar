<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		User::create([
			'email' => 'admin@yopmail.com',
			'password' => Hash::make('password'),
			'first_name' => 'Administrador',
			'last_name' => 'IrisGPS',
			'user_type_id' => 1,
			'company_id' => 2
		]);

		foreach(range(1, 10) as $index)
		{
			User::create([
				'email' => "usuario$index@yopmail.com",
				'password' => Hash::make('password'),
				'first_name' => "Usuario $index",
				'last_name' => 'IrisGPS',
				'user_type_id' => 2,
				'company_id' => 2
			]);
		}
	}

}