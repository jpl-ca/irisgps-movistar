<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGeofenceRecordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('geofence_records', function(Blueprint $table)
		{
			$table->string('entrance_latlng')->nullable()->after('departure');
			$table->string('departure_latlng')->nullable()->after('entrance_latlng');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('geofence_records', function($table)
		{
		    $table->dropColumn('entrance_latlng');
		    $table->dropColumn('departure_latlng');
		});
	}

}
