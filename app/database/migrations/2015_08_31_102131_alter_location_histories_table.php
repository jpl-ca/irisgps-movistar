<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLocationHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('location_histories', function(Blueprint $table)
		{
			$table->decimal('speed_device',8,4)->default(0)->after('lng');
			$table->decimal('speed_custom',8,4)->default(0)->after('speed_device');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('location_histories', function($table)
		{
		    $table->dropColumn('speed_device');
		    $table->dropColumn('speed_custom');
		});
	}

}
