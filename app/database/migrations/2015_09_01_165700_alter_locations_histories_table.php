<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLocationsHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('location_histories', function(Blueprint $table)
		{
			$table->boolean('movement')->default(false)->after('speed_custom');
			$table->boolean('contact_open')->default(false)->after('movement');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('location_histories', function($table)
		{
		    $table->dropColumn('movement');
		    $table->dropColumn('contact_open');
		});
	}

}
