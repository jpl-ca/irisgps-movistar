<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVehiclesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vehicles', function(Blueprint $table)
		{
			$table->decimal('fuel_performance',7,4)->default(1)->after('color');
			$table->integer('speed_limit')->default(100)->after('fuel_performance');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vehicles', function($table)
		{
		    $table->dropColumn('fuel_performance');
		    $table->dropColumn('speed_limit');
		});
	}

}
