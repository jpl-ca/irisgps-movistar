<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigurationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('configurations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('key')->unique();
			$table->string('value', 3000);
			$table->timestamps();
		});

		Configuration::create([
			'key' => 'geofences_notification_targets',
			'value' => 'prueba@yopmail.com',			
		]);

		Configuration::create([
			'key' => 'geofences_validation_frecuency',
			'value' => '5',			
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('configurations');
	}

}
