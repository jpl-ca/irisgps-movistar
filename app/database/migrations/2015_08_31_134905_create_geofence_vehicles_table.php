<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeofenceVehiclesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('geofence_vehicles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('geofence_id')->unsigned();
			$table->integer('vehicle_id')->unsigned();
			$table->timestamps();

			$table->foreign('geofence_id')->references('id')->on('geofences')->onDelete('cascade');
			$table->foreign('vehicle_id')->references('id')->on('vehicles')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('geofence_vehicles');
	}

}
