<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertConfigurationsTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Configuration::create([
			'key' => 'speed_limits_green',
			'value' => '0,50',
		]);

		Configuration::create([
			'key' => 'speed_limits_yellow',
			'value' => '50,90',
		]);

		Configuration::create([
			'key' => 'speed_limits_orange',
			'value' => '90',
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Configuration::whereIn('key', ['speed_limits_green','speed_limits_yellow','speed_limits_orange'])->delete();
	}

}
